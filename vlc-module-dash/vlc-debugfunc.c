/**
 * @file    vlc-debugfunc.c
 * @brief   debug functions
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-12-15
 */

#include <limits.h> // UINT_MAX
#include <errno.h>
#include <assert.h>
#include <ctype.h> // isprint
#include <unistd.h> // write()

#include "vlc-debugfunc.h"

#if USE_VLCLIB
int
bhd_cb_writer_vlcstream (void *fd, opaque_t * fragment, size_t size)
{
    static char buf [200] = "";
    stream_t *strm = (stream_t *)fd;
    char *pcur = (char *)fragment;
    size_t pos_end = strlen (buf);
    size_t i;
    for (i = 0; i < size; i ++) {
        if (*pcur == '\n') {
            msg_Dbg (strm, "%s", (char *)buf);
            buf[0] = 0;
            pos_end = 0;
        } else {
            // append to the end of buf
            buf[pos_end] = *pcur;
            pos_end ++;
            buf[pos_end] = 0;
        }
        if (pos_end >= sizeof (buf)) {
            msg_Dbg (strm, "%s", buf);
            buf[0] = 0;
            pos_end = 0;
        }
        pcur ++;
    }
    return 0;
}

#else
int
bhd_cb_writer_fd (void *fd, opaque_t * fragment, size_t size)
{
    return write (fd, fragment, size);
}
#endif

static int
bhd_cb_writer_null (void *fd, opaque_t * fragment, size_t size)
{
    return size;
}

size_t
bulk_hex_dump (void *fd, opaque_t * fragment, size_t size, bhd_cb_writer_t writer, int use_c_style)
{
    bhd_cb_writer_t pwriter = bhd_cb_writer_null;
    size_t size_ret = 0;
    int ret = 0;
    size_t line_num = 0;
    size_t i = 0;
    uint8_t buffer[20];
    /* printf("the buffer data(size=%d=0x%08x):\n", size, size); */
    if (NULL != writer)
    {
        pwriter = writer;
    }
    i = line_num << 4;
    while (i < size)
    {
        if (! use_c_style)
        {
            ret = sprintf ((char *)buffer, "%08xh: ", i);
            assert (ret < sizeof (buffer));
            if (ret > 0)
            {
                ret = pwriter (fd, buffer, ret);
                if (ret > 0)
                    size_ret += ret;
            }
        }
        while ((i < ((line_num + 1) << 4)))
        {
            if (i == (line_num << 4) + 8)
            {
                if (use_c_style) {
                    ret = pwriter (fd, (uint8_t *)" ", strlen (" "));
                } else {
                    ret = pwriter (fd, (uint8_t *)"- ", strlen ("- "));
                }
                if (ret > 0)
                    size_ret += ret;
            }

            if (i < size)
            {
                if (use_c_style) {
                    ret = sprintf ((char *)buffer, "0x%02X, ", fragment[i]);
                    assert (ret < sizeof (buffer));
                } else {
                    ret = sprintf ((char *)buffer, "%02X ", fragment[i]);
                    assert (ret < sizeof (buffer));
                }
                if (ret > 0)
                {
                    ret = pwriter (fd, buffer, ret);
                    if (ret > 0)
                        size_ret += ret;
                }
            }
            else
            {
                ret = pwriter (fd, (uint8_t *)"   ", strlen ("   "));
                if (ret > 0)
                    size_ret += ret;
            }

            i++;
        }
        if (! use_c_style) {
            ret = pwriter (fd, (uint8_t *)"; ", strlen ("; "));
            if (ret > 0)
                size_ret += ret;
            i = line_num << 4;
            while ((i < ((line_num + 1) << 4)) && (i < size))
            {
                char ch;
                if (i == (line_num << 4) + 8)
                {
                    ret = pwriter (fd, (uint8_t *)" ", strlen (" "));
                    if (ret > 0)
                        size_ret += ret;
                }
                ch = fragment[i];
                if (!isprint (ch))
                    ch = '.';
                ret = sprintf ((char *)buffer, "%c", ch);
                assert (ret < sizeof (buffer));
                ret = pwriter (fd, buffer, ret);
                if (ret > 0)
                    size_ret += ret;
                i++;
            }
        }
        ret = pwriter (fd, (uint8_t *)"\n", strlen ("\n"));
        if (ret > 0)
            size_ret += ret;
        line_num++;
        i = line_num << 4;
    }
    return size_ret;
}
