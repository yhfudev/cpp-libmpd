/**
 * @file    vlc-dash.cc
 * @brief   VLC DASH module
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-09-05
 */
/*****************************************************************************
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <limits.h> // UINT_MAX
#include <errno.h>

#include "vlc-dash.h"

#include "mympd.h"
#include "mympdalg.h"
#include "myxmlparser.h"

/* the number of the requested chunks */
#define NUM_REQUEST_CHUNK 4

/* the number of downloader threads */
#define NUM_DOWNLOADER 1

/*****************************************************************************
 * Local prototypes
 *****************************************************************************/
struct stream_sys_t {
    vdash_streambuf_t * p_dashbuf;
    mpd_t     *p_mpd;
    bool       isLive;
};
#define demux_sys_t stream_sys_t;

#if USE_DASH_DEMUX
SeekPrepare
static int  DemuxInit( demux_t * );
static void DemuxEnd( demux_t * );
static int  DemuxPacket( demux_t * );
static int  Demux (demux_t *p_stream);
static int  Control (demux_t *p_stream, int i_query, va_list args);
#else
static int  Read    (stream_t *p_stream, void *p_ptr, unsigned int i_len);
static int  Peek    (stream_t *p_stream, const uint8_t **pp_peek, unsigned int i_peek);
static int  Control (stream_t *p_stream, int i_query, va_list args);
#endif // USE_DASH_DEMUX

#define SZ_PEEKMPD 512
static bool
find_needle_mpd ( stream_t *s, const char *needle)
{
    const char * encoding = NULL;
    bool ret = false;

    const uint8_t * peek = NULL;
    char * peeked = NULL;

    int i_size = stream_Peek( s->p_source, &peek, SZ_PEEKMPD );
    if( i_size < 1 ) {
        msg_Dbg( s, "Error in stream_Peek()\n" );
        return false;
    }

    peeked = (char *)malloc (SZ_PEEKMPD + 2);
    if (NULL == peeked) {
        return false;
    }
    msg_Dbg( s, "memcpy() ...\n" );
    memcpy( peeked, peek, i_size );
    peeked[i_size] = peeked[i_size + 1] = '\0';

    //msg_Dbg( s, "strstr() ...\n" );
    if( strstr( (const char *)peeked, needle ) != NULL ) {
        ret = true;
    } else {
        /* maybe it's utf-16 encoding, should we also test other encodings? */
        if( !memcmp( peeked, "\xFF\xFE", 2 ) )
            encoding = "UTF-16LE";
        else if( !memcmp( peeked, "\xFE\xFF", 2 ) )
            encoding = "UTF-16BE";
        else
        {
            free( peeked );
            return false;
        }
        peeked = FromCharset( encoding, peeked, SZ_PEEKMPD );

        if( strstr( peeked, needle ) != NULL ) {
            msg_Dbg( s, "detected DASH MPD file!\n" );
            ret = true;
        } else {
            msg_Info( s, "Not a DASH MPD file, skiping ...\n" );
        }
    }

    free (peeked);
    return ret;
}

static bool
is_dash_mpd ( stream_t *s )
{
    const char *needles[] = {
        "urn:mpeg:DASH:schema:MPD:2011",
        "urn:mpeg:mpegB:schema:DASH:MPD:DIS2011",
        "urn:3GPP:ns:PSS:AdaptiveHTTPStreamingMPD:2009",
        "urn:mpeg:dash:profile:full:2011",
        "urn:mpeg:dash:profile:isoff-live:2011",
        "urn:mpeg:dash:profile:isoff-main:2011",
        "urn:mpeg:mpegB:profile:dash:isoff-basic-on-demand:cm",
        "urn:mpeg:dash:profile:isoff-ondemand:2011",
        "urn:mpeg:dash:profile:isoff-on-demand:2011",
        "urn:mpeg:dash:profiles:isoff-main:2011",
        "urn:mpeg:dash:profile:mp2t-main:2011",
        "urn:mpeg:dash:profile:mp2t-simple:2011",
        "urn:webm:dash:profile:webm-on-demand:2012",
        };
    for (int i = 0; i < NUM_TYPE(needles); i ++) {
        if (find_needle_mpd (s, needles[i])) {
            return true;
        }
    }
    return false;
}

static mpd_t *
parse_manifest( stream_t *s )
{
    //stream_sys_t *p_sys = (stream_sys_t *)s->p_sys;

    stream_t *st = s->p_source;
    msg_Info ( s, "DASH Streaming (%s://%s)", s->psz_access, s->psz_path );
    msg_Dbg ( s, "Manifest parsing");

    xml_reader_lib_t xmlrd (st);
    if (! xmlrd.init()) {
        msg_Err( s, "Failed to open XML parser" );
        return NULL;
    }

    //msg_Dbg(s, "opening mpd file (%s)", st->psz_path);

    std::string name1("");
    name1 += s->psz_access;
    name1 += "://";
    name1 += s->psz_path;
    /*
    char *uri = NULL;
    if( ! unlikely( asprintf( &uri, "%s://%s", s->psz_access, s->psz_path ) < 0 ) ) {
        name1 += uri;
    }*/

    mpd_t *mpd = mpd_parse_current (&xmlrd, name1);
    if (NULL == mpd) {
        msg_Err( s, "Failed to parse MPD" );
        return NULL;
    }
    if (! mpd->verify()) {
        msg_Dbg( s, "Error in verify() MPD\n" );
        //delete mpd; return NULL;
    } else {
        msg_Dbg( s, "MPD verify() OK\n" );
    }
#if DEBUG
    msg_Dbg( s, "MPD re-serilize:" );
    std::cout << mpd->toString() << std::endl;
#endif

    return mpd;
}

/*****************************************************************************
 * Open:
 *****************************************************************************/
VLC_EXTERN int
libmpd_vlc_open (vlc_object_t *p_obj)
{
#if USE_DASH_DEMUX
    demux_t * p_demux = (demux_t *)p_obj;
    stream_t *p_stream = (stream_t *) p_demux->s;
#else
    stream_t *p_stream = (stream_t *) p_obj;
#endif

    DBG_SET_STREAM(p_stream);

    msg_Dbg( p_stream, "checking dash header... %d\n", 0);
    printf ("checking dash header ...\n");
    if( ! is_dash_mpd ( p_stream ) ) {
        msg_Err( p_stream, "Not dash header. %d\n", 0);
        printf ("Not dash header\n");
        return VLC_EGENERIC;
    }

// debug message
{
    std::string msg1;
#if USE_VLCLIB
    msg1 += "for VLC,";
#endif
#if LIBDASH_USE_208
    msg1 += "2.0.8, ";
#else
    msg1 += "2.1 and later, ";
#endif
#if LIBDASH_USE_DASH_DOWNLOAD
    msg1 += "use downloader module of the old libdash";
#endif
    msg_Dbg( p_stream, "DASH module: %s\n", msg1.c_str());
}

    msg_Dbg( p_stream, "parse manifest... %d\n", 0);
    mpd_t *mpd = parse_manifest ( p_stream );
    if( NULL == mpd ) {
        msg_Err ( p_stream, "Could not parse mpd file." );
        return VLC_EGENERIC;
    }

    stream_sys_t * p_sys = (stream_sys_t *) malloc (sizeof(stream_sys_t));
    if (unlikely (p_sys == NULL)) {
        return VLC_ENOMEM;
    }
    memset (p_sys, 0, sizeof (*p_sys));
    p_sys->p_mpd = mpd;

    msg_Dbg( p_stream, "create DASH stream buffer... %d\n", 0);
    // adaptor algorithm
#if LIBDASH_USE_DASH_DOWNLOAD
    p_sys->p_dashbuf = new vdash_streambuf_libdash_t (p_sys->p_mpd, p_stream);
#else // LIBDASH_USE_DASH_DOWNLOAD
    p_sys->p_dashbuf = new vdash_streambuf_libmpd_t (p_sys->p_mpd, p_stream);
#endif // LIBDASH_USE_DASH_DOWNLOAD
    if (unlikely (p_sys->p_dashbuf == NULL)) {
        libmpd_vlc_close (p_obj);
        msg_Err ( p_stream, "Could not create DASH stream buffer." );
        return VLC_ENOMEM;
    }
    if (! p_sys->p_dashbuf->start0()) {
        libmpd_vlc_close (p_obj);
        msg_Err ( p_stream, "Error in starting DASH stream buffer." );
        return VLC_ENOMEM;
    }

    p_sys->isLive           = mpd->is_live();

#if USE_DASH_DEMUX
    p_demux->p_sys          = p_sys;
    p_demux->pf_demux       = Demux;
    p_demux->pf_control     = Control;
    if( DemuxInit( p_demux ) ) {
        libmpd_vlc_close (p_obj);
        msg_Err ( p_stream, "Could not parse mpd file." );
        return VLC_EGENERIC;
    }
#else
    p_stream->p_sys         = p_sys;
    p_stream->pf_read       = Read;
    p_stream->pf_peek       = Peek;
    p_stream->pf_control    = Control;
#endif
    msg_Dbg( p_stream, "open DASH successfully\n");
    return VLC_SUCCESS;
}

/*****************************************************************************
 * Close:
 *****************************************************************************/
VLC_EXTERN void
libmpd_vlc_close (vlc_object_t *p_obj)
{
#if USE_DASH_DEMUX
    demux_t  *p_stream = (demux_t *)p_obj;
    DemuxEnd ( p_stream );
#else
    stream_t     * p_stream = (stream_t*) p_obj;
#endif
    stream_sys_t * p_sys    = (stream_sys_t *) p_stream->p_sys;
    DBG_SET_STREAM(p_stream);

    msg_Dbg( p_stream, "close DASH module\n");
    if (p_sys->p_dashbuf) {
        delete (p_sys->p_dashbuf);
        p_sys->p_dashbuf = NULL;
    }
    if (p_sys->p_mpd) {
        delete (p_sys->p_mpd);
        p_sys->p_mpd = NULL;
    }
    free (p_sys);
}

/*****************************************************************************
 * Callbacks:
 *****************************************************************************/

#if 1// DEBUG
#define CASE(a) case a: return #a
static char *
vlc_ctrl_query_code (int query_code)
{
    switch (query_code) {
#if USE_DASH_DEMUX
        CASE(DEMUX_CAN_SEEK);
        CASE(DEMUX_CAN_CONTROL_PACE);
        CASE(DEMUX_GET_TIME);
        CASE(DEMUX_SET_TIME);
        CASE(DEMUX_GET_POSITION);
        CASE(DEMUX_SET_POSITION);
        CASE(DEMUX_GET_LENGTH);

#else // USE_DASH_DEMUX
        CASE(STREAM_CAN_SEEK);
        CASE(STREAM_CAN_FASTSEEK);
#if ! LIBDASH_USE_208
        CASE(STREAM_CAN_PAUSE);
        CASE(STREAM_CAN_CONTROL_PACE);
        CASE(STREAM_SET_PAUSE_STATE);
#endif
        CASE(STREAM_GET_POSITION);
        CASE(STREAM_SET_POSITION);
        CASE(STREAM_GET_SIZE);

#endif // USE_DASH_DEMUX
    }
    return "(Unknown)";
}
#undef CASE
#endif

#if USE_DASH_DEMUX
typedef struct
{
    int i_cat;

    es_out_id_t     *p_es;
    es_format_t     *p_fmt; /* format backup for video changes */

    //mtime_t i_time;
    //block_t         *p_frame; /* use to gather complete frame */
} dash_track_t;

static int
DemuxInit ( demux_t * p_demux )
{
    stream_sys_t *p_sys = (stream_sys_t *)p_demux->p_sys;

}

static void
DemuxEnd( demux_t *p_demux )
{
    stream_sys_t *p_sys = (stream_sys_t *)p_demux->p_sys;

}

static int
Control (demux_t *p_stream, int i_query, va_list args)
{
    stream_sys_t *p_sys = (stream_sys_t *)p_stream->p_sys;
    if (NULL == p_sys) {
        msg_Dbg( p_stream, "p_sys NULL\n");
        return VLC_EGENERIC;
    }
    vdash_streambuf_t * p_dashbuf = p_sys->p_dashbuf;
    if (NULL == p_dashbuf) {
        msg_Dbg( p_stream, "p_dashbuf NULL\n");
        return VLC_EGENERIC;
    }
    DBG_SET_STREAM(p_stream);
#if 1 // DEBUG
    msg_Dbg( p_stream, "control DASH query code: %d(%s)\n", i_query, vlc_ctrl_query_code(i_query));
#endif
    switch (i_query)
    {
        case DEMUX_GET_LENGTH: /* the time */
        {
            uint64_t * res = (va_arg (args, uint64_t *));
            if(p_sys->isLive) {
                *res = 0;
            } else {
                *res = p_dashbuf->get_size_of_media();
            }
            msg_Dbg( p_stream, "Control get size=%d\n", (int)(*res));
            break;
        }

        case DEMUX_CAN_SEEK:
            /*TODO Support Seek */
#if 1 //LIBDASH_USE_DASH_DOWNLOAD
            msg_Dbg( p_stream, "Control get can seek = false\n");
            *(va_arg (args, bool *)) = false;
#else
            msg_Dbg( p_stream, "Control get can seek = true\n");
            *(va_arg (args, bool *)) = true;
#endif
            break;

#if ! LIBDASH_USE_208
        case DEMUX_CAN_CONTROL_PACE:
            msg_Dbg( p_stream, "Control get can control pace = true\n");
            *(va_arg (args, bool *)) = true;
            break;
#endif

        case DEMUX_GET_TIME:
        {
            int64_t *pi64;
            if (NULL == p_sys->p_dashbuf) return VLC_EGENERIC;
            if (p_sys->p_dashbuf->get_position_time() < 0) return VLC_EGENERIC;
            pi64 = (int64_t*)va_arg(args, int64_t *);
            *pi64 = (int64_t) p_sys->p_dashbuf->get_position_time();
            return VLC_SUCCESS;
        }

        case DEMUX_SET_TIME:
            SeekPrepare(p_stream);
            {
                va_list acpy;
                va_copy(acpy, args);
                i64 = (int64_t)va_arg(acpy, int64_t);
                va_end(acpy);

                if( !SeekIndex( p_stream, i64, -1 ) )
                    return VLC_SUCCESS;
            }
            return VLC_EGENERIC;

        case DEMUX_GET_POSITION: /* % of the position */
            if (p_sys->i_time < 0) return VLC_EGENERIC;
            if (p_sys->i_length > 0)
            {
                pf = (double*)va_arg(args, double *);
                *pf = p_sys->i_time / (double)p_sys->i_length;
                return VLC_SUCCESS;
            }
            return VLC_EGENERIC;

        case DEMUX_SET_POSITION: /* % of the position */
            SeekPrepare(p_stream);
            if (p_sys->i_length > 0)
            {
                va_list acpy;
                va_copy(acpy, args);
                f = (double)va_arg(acpy, double);
                va_end(acpy);

                if (!SeekIndex(p_stream, -1, f))
                    return VLC_SUCCESS;
            }
            return VLC_EGENERIC;

        default:
            return VLC_EGENERIC;
    }
    return VLC_SUCCESS;
}

typedef struct
{
    int i_cat;

    es_out_id_t     *p_es;
    es_format_t     *p_fmt; /* format backup for video changes */

    //mtime_t i_time;
    //block_t         *p_frame; /* use to gather complete frame */
} dash_track_t;


static int
DemuxInit ( demux_t * p_demux )
{
}

static void
DemuxEnd( demux_t *p_demux )
{
}

#else // USE_DASH_DEMUX

static int
Read (stream_t *p_stream, void *p_ptr, unsigned int i_len)
{
    stream_sys_t * p_sys = (stream_sys_t *) p_stream->p_sys;
    vdash_streambuf_t * p_dashbuf = p_sys->p_dashbuf;
    uint8_t * p_buffer = (uint8_t*)p_ptr;
    int i_ret = 0;
    int i_read = 0;
    DBG_SET_STREAM(p_stream);

    msg_Dbg( p_stream, "read DASH size: %d\n", (int)i_len);
    while( i_len > 0 )
    {
        i_read = p_dashbuf->read( p_buffer, i_len );
        if( i_read < 0 )
            break;
        p_buffer += i_read;
        i_ret += i_read;
        i_len -= i_read;
    }
    p_buffer -= i_ret;

    if (i_read < 0)
    {
        switch (errno)
        {
            case EINTR:
            case EAGAIN:
                break;
            default:
                msg_Dbg(p_stream, "DASH Read: failed to read");
                return 0;
        }
        return 0;
    }

    return i_ret;
}

static int
Peek (stream_t *p_stream, const uint8_t **pp_peek, unsigned int i_peek)
{
    stream_sys_t        *p_sys          = (stream_sys_t *) p_stream->p_sys;
    vdash_streambuf_t   *p_dashbuf  = p_sys->p_dashbuf;
    DBG_SET_STREAM(p_stream);

    return p_dashbuf->peek( pp_peek, i_peek );
}

static int
Control (stream_t *p_stream, int i_query, va_list args)
{
    stream_sys_t * p_sys = (stream_sys_t *)p_stream->p_sys;
    if (NULL == p_sys) {
        msg_Dbg( p_stream, "p_sys NULL\n");
        return VLC_EGENERIC;
    }
    vdash_streambuf_t * p_dashbuf = p_sys->p_dashbuf;
    if (NULL == p_dashbuf) {
        msg_Dbg( p_stream, "p_dashbuf NULL\n");
        return VLC_EGENERIC;
    }
    DBG_SET_STREAM(p_stream);
#if 1 // DEBUG
    msg_Dbg( p_stream, "control DASH query code: %d(%s)\n", i_query, vlc_ctrl_query_code(i_query));
#endif
    switch (i_query)
    {
        case STREAM_CAN_SEEK:
            /*TODO Support Seek */
#if 1 //LIBDASH_USE_DASH_DOWNLOAD
            msg_Dbg( p_stream, "Control get can seek = false\n");
            *(va_arg (args, bool *)) = false;
#else
            msg_Dbg( p_stream, "Control get can seek = true\n");
            *(va_arg (args, bool *)) = true;
#endif
            break;

        case STREAM_CAN_FASTSEEK:
            msg_Dbg( p_stream, "Control get can fast seek = false\n");
            *(va_arg (args, bool *)) = false;
            break;

#if ! LIBDASH_USE_208
        case STREAM_CAN_PAUSE:
            msg_Dbg( p_stream, "Control get can pause = true\n");
            *(va_arg (args, bool *)) = true;
            break;

        case STREAM_CAN_CONTROL_PACE:
            msg_Dbg( p_stream, "Control get can control pace = true\n");
            *(va_arg (args, bool *)) = true;
            break;

        case STREAM_SET_PAUSE_STATE:
        {
            bool paused = va_arg (args, unsigned);
            msg_Dbg( p_stream, "Control set paused\n");
            p_dashbuf->set_paused (paused);
        }
            break;
#endif

        case STREAM_GET_POSITION:
        {
            uint64_t * pos1 = (va_arg (args, uint64_t *));
            *pos1 = p_dashbuf->get_current_read_position0 ();
            msg_Dbg( p_stream, "Control get pos=%d\n", (int)(*pos1));
        }
            break;

        case STREAM_SET_POSITION:
        {
            uint64_t pos = (uint64_t)va_arg(args, uint64_t);
            msg_Dbg( p_stream, "Control set pos=%d\n", (int)(pos));
            if (p_dashbuf->seek (pos, SEEK_SET)) {
                break;
            }
            return VLC_EGENERIC;
        }

        case STREAM_GET_SIZE:
        {
            uint64_t * res = (va_arg (args, uint64_t *));
            if(p_sys->isLive) {
                *res = 0;
            } else {
                *res = p_dashbuf->get_size_of_media();
            }
            msg_Dbg( p_stream, "Control get size=%d\n", (int)(*res));
            break;
        }

        default:
            return VLC_EGENERIC;
    }
    return VLC_SUCCESS;
}

#endif // USE_DASH_DEMUX

/*****************************************************************************
 * stream buffer:
 *****************************************************************************/

#if LIBDASH_USE_DASH_DOWNLOAD

vdash_streambuf_libdash_t::~vdash_streambuf_libdash_t()
{
    this->stop ();

    if (NULL != this->conManager) {
        delete (this->conManager);
    }
#if LIBDASH_USE_208
    if (NULL != this->currentChunk) {
        delete (this->currentChunk);
    }
#else
    if (NULL != this->buffer) {
        delete (this->buffer);
    }
    if (NULL != this->downloader) {
        delete (this->downloader);
    }
#endif
    if (NULL != this->adaptationLogic) {
        delete (this->adaptationLogic);
    }
}

/**
 * @brief set the video offset for the open video stream
 *
 * @param pos : "video"/"audio"/"text"("application/ttml+xml")
 * @param type : one of value SEEK_SET, SEEK_CUR, and SEEK_END
 *
 * @return false on failure
 *
 * set the video offset for the open video stream
 */
bool
vdash_streambuf_libdash_t::seek (off_t offset, int whence)
{
    off_t real_pos;
#if LIBDASH_USE_208
    if (NULL == this->conManager) {
        return false;
    }
    return false; // TODO
#else
    if (NULL == this->buffer) {
        return false;
    }
#endif
    switch (whence) {
    case SEEK_CUR:
        if (offset < 0) {
            real_pos = - offset;
            //this->adaptationLogic->seek ();
#if LIBDASH_USE_208
            return false; // TODO
#else
            if (this->buffer->seekBackwards (real_pos)) {
                this->set_read_position (this->get_current_read_position0() + offset);
                return true;
            }
            return false;
#endif
        }
        real_pos = offset;
#if LIBDASH_USE_208
        // TODO
#else
        this->buffer->get (NULL, real_pos);
        this->set_read_position (this->get_current_read_position0() + offset);
#endif
        break;

    case SEEK_SET:
        this->set_read_position (offset);
        break;

    case SEEK_END:
        this->set_read_position (this->get_size_of_media() + offset);
        break;

    default:
        return false;
    }
    return false;
}

ssize_t
vdash_streambuf_libdash_t::peek( const uint8_t **pp_peek, size_t i_peek )
{
    ssize_t ret = -1;
#if LIBDASH_USE_208
    if ( NULL == this->currentChunk ) {
        try {
            this->currentChunk = this->adaptationLogic->getNextChunk();
        } catch (dash::exception::EOFException & e) {
            this->currentChunk = NULL;
            return 0;
        }
#if DEBUG
        this->id_current_segment ++;
#endif
    }
    ret = this->conManager->peek( this->currentChunk, pp_peek, i_peek );
    TRACE_PEEK(0, this->id_current_segment, this->get_current_read_position0(), i_peek, ret);
#else
    ret = this->buffer->peek(pp_peek, i_peek);
    TRACE_PEEK(0, 0, this->get_current_read_position0(), i_peek, ret);
#endif
    return ret;
}

ssize_t
vdash_streambuf_libdash_t::read (void * buf, size_t len)
{
    int ret = 0;
#if LIBDASH_USE_208
    if ( NULL == this->currentChunk ) {
        try {
            this->currentChunk = this->adaptationLogic->getNextChunk();
        } catch(dash::exception::EOFException & e) {
            this->currentChunk = NULL;
            return 0;
        }
#if DEBUG
        this->id_current_segment ++;
#endif
    }
    if ( NULL == this->currentChunk ) {
        return 0;
    }
    ret = this->conManager->read( this->currentChunk, buf, len );
    if ( ret == 0 ) {
        try {
            /*if (this->currentChunk) {
                delete this->currentChunk;
                this->currentChunk = NULL;
            }*/
            this->currentChunk = this->adaptationLogic->getNextChunk();
        } catch(dash::exception::EOFException & e) {
            this->currentChunk = NULL;
        }
        if (NULL == this->currentChunk) {
            return 0;
        }
#if DEBUG
        this->id_current_segment ++;
#endif
        ret = this->conManager->read( this->currentChunk, buf, len );
        TRACE_READ(0, this->id_current_segment, this->get_current_read_position0(), len, ret);
    }
#else
    ret = this->buffer->get(buf, len);
    TRACE_READ(0, this->id_current_segment, this->get_current_read_position0(), len, ret);
#endif
    if (ret > 0) {
        this->set_read_position (this->get_current_read_position0() + ret);
    }
    return ret;
}

bool
vdash_streambuf_libdash_t::stop (void)
{
#if LIBDASH_USE_208
#else
    //this->downloader->stop ();
#endif
    return true;
}

bool
vdash_streambuf_libdash_t::start0 (void)
{
    this->adaptationLogic = new vdash_adaptlogic_t (this->mpd);

    this->conManager = new dash::http::HTTPConnectionManager(
#if LIBDASH_USE_208
#else
                            this->adaptationLogic,
#endif
                            this->stream);
    this->conManager->attach(this->adaptationLogic);

#if LIBDASH_USE_208
    return true;
#else
    this->buffer     = new dash::buffer::BlockBuffer(this->stream);
    this->buffer->attach(this->adaptationLogic);

    this->downloader = new dash::DASHDownloader(this->conManager, this->buffer);
    return this->downloader->start();
#endif
}

#else // LIBDASH_USE_DASH_DOWNLOAD

void
vdash_streambuf_libmpd_t::clear_buffer (void)
{
    size_t szbuf = 0;
    //vlc_mutex_lock ( &(this->mt_buffer) );
    szbuf = this->buffer.size();
    //vlc_mutex_unlock ( &(this->mt_buffer) );

    for (int i = 0; i < szbuf; i ++) {
        std::map<int, vdash_downsegment_t *>::iterator it;
        for (it = this->buffer[i].begin(); it != this->buffer[i].end(); it ++) {
            delete (it->second);
            it->second = NULL; //this->buffer[i][it->first] = NULL;
            this->buffer[i].erase (it);
        }
        this->buffer_stat[i].received = 0;
    }
}

inline
vdash_streambuf_libmpd_t::~vdash_streambuf_libmpd_t ()
{
    this->stop ();
    // delay? or use condition?
    this->downer.unregister_notify (&this->tpstat);
    this->tpstat.unregister_notify (this);
    vlc_mutex_destroy ( &(this->mt_buffer) );
    vlc_cond_destroy ( &(this->ct_buffer) );
    this->clear_buffer ();
    delete (this->interlogic);
    this->interlogic = NULL;
    this->mpd = NULL;
    this->stream = NULL;
}

bool
vdash_streambuf_libmpd_t::stop (void)
{
    this->downer.stop ();
    this->tpstat.stop ();
    this->downer.join ();
    this->tpstat.join ();
}

bool
vdash_streambuf_libmpd_t::start0 (void)
{
    this->interlogic = new mpd_adaptationlogic_yhfu_t (this->mpd);

    // parse mpd
    std::map<std::string, std::string> categories;
    std::vector<std::string> minetypes = this->interlogic->get_mimetype_list();
    for (std::vector<std::string>::iterator it = minetypes.begin(); it != minetypes.end(); it ++) {
        if (std::string::npos != (*it).find ("video")) {
            categories["video"] = *it;
        } else if (std::string::npos != (*it).find ("audio")) {
            categories["audio"] = *it;
        }
    }
    this->buffer_stat.clear();
    this->map_to_category.clear();
    int i = 0;
    for (std::map<std::string, std::string>::iterator it2 = categories.begin(); it2 != categories.end(); it2 ++) {
        map_to_category[it2->second] = i;
        i ++;
    }
    vlc_mutex_lock ( &(this->mt_buffer) );
    // clear this->buffer
    this->clear_buffer ();
    this->buffer.resize(categories.size());
    this->buffer_stat.resize(categories.size());
    vlc_mutex_unlock ( &(this->mt_buffer) );

    this->estimate_media_byte_length (); // initilize the size of media
    TD ("feed_requests (start id=%d) ...", 0);
    ssize_t ret1 = this->feed_requests (0);
    if (ret1 >= 0) {
        this->id_requested_segment = ret1;
    }

    // start
    this->downer.set_num_worker (NUM_DOWNLOADER);
    this->downer.register_notify (&this->tpstat);
    this->tpstat.register_notify (this);
    this->downer.start ();
    this->tpstat.start ();

    return true;
}

void
vdash_streambuf_libmpd_t::set_paused (bool is_paused)
{
    this->downer.pause (is_paused);
    this->tpstat.pause (is_paused);
}

// add as many requests as possible to the download queue
// return: < 0 on error, > 0 the next request chunk id
ssize_t
vdash_streambuf_libmpd_t::feed_requests (size_t idx_request_start)
{
    size_t idx_request = idx_request_start;
    int i;
    std::string lang("en"); // TODO: get the language for current presentation
    vdash_downsegment_t * seg = NULL;
    std::map<int, vdash_downsegment_t *>::iterator it1;
    std::map<std::string, int>::iterator it2;
    std::map<std::string, int>::iterator itcat;
    mpd_chunk_t chkpin;

    assert (NULL != this->mpd);
    assert (NULL != this->interlogic);

    itcat = this->map_to_category.begin();
    size_t current_bitrate = this->interlogic->get_current_bitrate(itcat->first, lang);
    // TODO: make sure startime?=0
    double segduration0 = this->mpd->get_segment_duration(itcat->first, lang, 0, current_bitrate, idx_request_start>0?idx_request_start:1 );

    TD ("segduration=%d; total seg=%d", (int)segduration0, (int)this->total_segment);
    this->interlogic->notify_buf_level(
                    segduration0 * this->total_segment
                    , this->buffer_stat[0].received //segduration * this->buffer[itcat->second].size()
                    , this->buffer_stat[0].requested); //segduration * this->cnt_requested);

    chkpin.set_category1 (itcat->second);

    for (; this->interlogic->is_need_request(); ) {
        assert (this->buffer.size() > 0);
        assert (this->map_to_category.size() > 0);
        if (this->downer.requests_number () > NUM_REQUEST_CHUNK) {
            break;
        }
        i = 0;
        for (i = 0; (i < this->total_segment); i ++, idx_request ++) {
            chkpin.set_id (idx_request);
            // be careful the following call to downer, deadlock?
            if (this->downer.exist_request (&chkpin)) {
                TW ("request id=%d exist in downer, skip", idx_request);
                continue;
            }

            if ((it1 = this->buffer[itcat->second].find (idx_request)) != this->buffer[itcat->second].end()) {
                // compare the bit rate downloaded
                seg = it1->second;
                TW ("try to request a duplicated id=%d of bitrate=%d, with a existing bitrate=%d,", idx_request, current_bitrate, seg->get_bitrate());
                if (seg->get_bitrate() >= current_bitrate) {
                    TW ("request id=%d exist in buffer, and requested bitrate is lower than or equal to it, skip", idx_request);
                    continue;
                }
            }
            // try to download larger bitrate content.
            TD ("try to download id=%d, bitrate=%d", idx_request, current_bitrate);
            break;
        }
        if (i >= this->total_segment) {
            // not any more segments to be fetched
            break;
        }
        // seek to the position
        TD ("alg seek to id=%d", idx_request);
        this->interlogic->seek (idx_request);

        mpd_chunk_t * chunk = NULL;
        for (it2 = this->map_to_category.begin(); it2 != this->map_to_category.end(); it2 ++) {
            chunk = this->interlogic->get_next_chunk (it2->first, lang);
            if (NULL == chunk) {
                break;
            }
            assert (NULL != chunk);

            this->buffer_stat[it2->second].requested += this->interlogic->get_segment_duration(idx_request);
            chunk->set_id (idx_request);
            chunk->set_category1(it2->second);
            vdash_downrequest_t * req = new vdash_downrequest_mpd_t (chunk, this);
            assert (NULL != req);
            this->downer.request (req);
        }
        if (NULL == chunk) {
            break;
        }
        idx_request ++;
        //this->cnt_requested ++;

        // keep track of the # of segments in buffer, and # of requests in downloader
        // TODO: use real buffer maximum size instead of (segduration * this->total_segment)
        this->interlogic->notify_buf_level(segduration0 * this->total_segment
                                           , this->buffer_stat[0].received //segduration * this->buffer[itcat->second].size()
                                           , this->buffer_stat[0].requested); //segduration * this->cnt_requested);
    }
    TD ("feed_requests() done");
    return idx_request;
}

/*
 * the seek range:
 *   1) recalulate the total range of the video/audio size using current bitrate(throughput) when user call STREAM_GET_SIZE
 *   2) use the calculated max size to convert the position to segment id
 *   3) use the segment id to move to the begining of the new segment.
 *   4) note: use real data position when the new position is in current segment
 */

/* get the current read segment from the buffer from category
 */
vdash_downsegment_t *
vdash_streambuf_libmpd_t::get_buffer_segment (int category, int id)
{
    if (id > this->total_segment) {
        return NULL;
    }

    size_t szbuf = 0;
    //vlc_mutex_lock ( &(this->mt_buffer) );
    szbuf = this->buffer.size();
    //vlc_mutex_unlock ( &(this->mt_buffer) );

    if (category >= szbuf) {
        return NULL;
    }
    // find the segment by id this->id_current_segment
    if (this->buffer[category].end() == this->buffer[category].find (id) ) {
        return NULL;
    }
    vdash_downsegment_t * seg = NULL;
    //vlc_mutex_lock ( &(this->mt_buffer) );
    seg = this->buffer[category][id];
    //vlc_mutex_unlock ( &(this->mt_buffer) );

    return seg;
}

/* get the current read segment from the buffer by the read position
 */
vdash_downsegment_t *
vdash_streambuf_libmpd_t::get_current_read_segment (void)
{
    vdash_downsegment_t * seg = NULL;
    int id = this->id_current_segment;

    size_t szbuf = 0;
    //vlc_mutex_lock ( &(this->mt_buffer) );
    szbuf = this->buffer.size();
    //vlc_mutex_unlock ( &(this->mt_buffer) );

    TD ("try get current segment id=%d", id);
    int i;
    for (; (seg == NULL);) {

        for (i = 0; i < szbuf; i ++) {
            seg = this->get_buffer_segment (i, id);
            if (NULL == seg) {
                TE ("chunk not found! channel=%d, id=%d", i, id);
                break;

            } else {
                if (seg->get_relpos() < seg->get_size()) {
                    TD ("chunk found at channel=%d, id=%d, relpos=%d, size=%d", i, id, (int)seg->get_relpos(), (int)seg->get_size());
                    if (id != this->id_current_segment) {
                        assert (id > this->id_current_segment);
                        TD ("switch current seg id from %d to %d", this->id_current_segment, id);
                        this->id_current_segment = id;
                    }
                    break;
                }
            }
        }

        if (i < szbuf) {
            if (NULL == seg) {
                // not found
                TE ("chunk not downloaded! channel=%d, id=%d", i, id);
                return NULL;
            }
        } else {
            // we need to move to next id
            id ++;
            TD ("try next id: %d", id);
            seg = NULL;
        }
    }
    return seg;
}

// the size of all of the segment as the same id
off_t
vdash_streambuf_libmpd_t::get_chunk_id_size (int id)
{
    size_t szseg = 0;
    vdash_downsegment_t * seg = NULL;

    size_t szbuf = 0;
    //vlc_mutex_lock ( &(this->mt_buffer) );
    szbuf = this->buffer.size();
    //vlc_mutex_unlock ( &(this->mt_buffer) );

    for (int i = 0; i < szbuf; i ++) {
        seg = this->get_buffer_segment (i, id);
        if (NULL != seg) {
            szseg += seg->get_size();
        } else {
            // content not ready
            return 0;
        }
    }
    return szseg;
}

bool
vdash_streambuf_libmpd_t::estimate_media_byte_length ()
{
    assert (NULL != this->mpd);

    std::string lang("en");
    // recalculate the size of media according current used bitrate and current segment.

    // get the bitrate
    size_t total_bandwidth = 0;
    double duration = 0;
    double duration0;
    size_t bitrate;

    std::vector<std::string> minetypes = this->interlogic->get_mimetype_list();
    for (std::vector<std::string>::iterator it = minetypes.begin(); it != minetypes.end(); it ++) {
        bitrate = this->interlogic->get_current_bitrate (*it, lang);
        duration0 = this->mpd->get_segment_duration (*it, lang, 0, bitrate, 1);
        if (duration < duration0) {
            duration = duration0;
        }
        total_bandwidth += bitrate;
    }
    size_t alpha = 8; // weight of bandwidth
    size_t rought_size = duration * total_bandwidth / 8; // the duration time of the segment 0 is 0, so use 1 instead

    // get the size of the current chunk
    size_t szseg = this->get_current_chunk_size ();
    if (szseg > 0) {
        rought_size *= alpha;
        rought_size += (10 - alpha) * szseg;
        rought_size /= 10;
    }
    if (rought_size < this->init_segment_size) {
        rought_size = this->init_segment_size;
    }
    // save rought_size ...
    this->avg_segment_size = rought_size;
    TD ("this->mpd->get_pres_duration_value ()=%f; this->mpd->get_min_buf_time_value=%f", this->mpd->get_pres_duration_value (), this->mpd->get_min_buf_time_value());
    this->total_segment = ceil (this->mpd->get_pres_duration_value () / this->mpd->get_min_buf_time_value());
    this->total_segment = this->interlogic->get_total_number_segments ();

    return true;
}

/* the size of video+audio calculated by throughput(not exact size) for upper layer */
off_t
vdash_streambuf_libmpd_t::get_size_of_media ()
{
    // B <- get current used bitrate.
    // D <- get the duration of the media
    // size = B * D / 8
    TRACE_GETSIZE(0, this->id_current_segment, this->get_current_read_position0(), (this->avg_segment_size * this->total_segment + this->init_segment_size));
    return (this->avg_segment_size * this->total_segment + this->init_segment_size);
}

off_t
vdash_streambuf_libmpd_t::get_current_read_position0 ()
{
    off_t pos = 0;
    vlc_mutex_lock ( &(this->mt_buffer) );
    pos = _get_current_read_position ();
    vlc_mutex_unlock ( &(this->mt_buffer) );
    TD ("get_current_read_position: ret=%d\n", (int)pos);
    return pos;
}

/* the current read position, precisely in current segment */
off_t
vdash_streambuf_libmpd_t::_get_current_read_position ()
{
    off_t pos = 0;

    pos = get_chunk_start_position (this->id_current_segment);

    // B <- get current used bitrate.
    // D <- get the id of the current chunk
    // pos = B * (D-1) / 8 + related position
    vdash_downsegment_t * seg = NULL;

    size_t szbuf = 0;
    //vlc_mutex_lock ( &(this->mt_buffer) );
    szbuf = this->buffer.size();
    //vlc_mutex_unlock ( &(this->mt_buffer) );

    for (int i = 0; i < szbuf; i ++) {
        seg = this->get_buffer_segment (i, this->id_current_segment);
        if (NULL == seg) {
            TE ("segment at channel=%d, id=%d, not found!!!!", i, (int)this->id_current_segment);
            break;
        } else {
            if (seg->get_relpos() >= seg->get_size()) {
                TD ("pos(=%d) + seg size=%d; channel=%d, id=%d; relpos=%d", (int)pos, (int)seg->get_size(), i, (int)this->id_current_segment, (int)seg->get_relpos());
                pos += seg->get_size();
            } else {
                TD ("pos(=%d) + seg relpos=%d; channel=%d, id=%d;", (int)pos, (int)seg->get_relpos(), i, (int)this->id_current_segment);
                pos += seg->get_relpos ();
                TRACE_GETPOS (0, this->id_current_segment, pos, seg->get_size());
                break;
            }
        }
    }

    return pos;
}

void
vdash_streambuf_libmpd_t::update_init_size ()
{
    int id = 0;
    size_t szinit = 0;

    vdash_downsegment_t * seg = NULL;
    size_t szbuf = 0;
    //vlc_mutex_lock ( &(this->mt_buffer) );
    szbuf = this->buffer.size();
    //vlc_mutex_unlock ( &(this->mt_buffer) );

    for (int i = 0; i < szbuf; i ++) {
        seg = this->get_buffer_segment (i, id);
        if (NULL == seg) {
            TE ("segment at channel=%d, id=%d, not found!!!!", i, id);
        } else {
            szinit += seg->get_size();
        }
    }
    this->init_segment_size = szinit;
}

off_t
vdash_streambuf_libmpd_t::get_chunk_start_position (int id)
{
    off_t pos = 0;
    if (id > 0) {
        pos += this->init_segment_size;
    }
    if (id > 1) {
        pos += this->avg_segment_size * (id - 1);
        TD ("id_current_segment = %d; set pos=%d", (int)(this->id_current_segment), (int)pos);
    }
    return pos;
}

/* set the read position, precisely in current segment, roughly in other segments
 * (the position will be set to the begining of that segment)
 */
bool
vdash_streambuf_libmpd_t::set_read_position (off_t new_pos)
{
    int i;
    vdash_downsegment_t * seg = NULL;
    assert (NULL != this->interlogic);
    off_t pos = 0;
    bool is_inside_chunk = false;
    off_t current_chunk_size = this->get_current_chunk_size ();
    if (0 == current_chunk_size) {
        // fake size
        current_chunk_size = this->avg_segment_size;
    }

    TD ("Set position: %d\n", (int)new_pos);

    vlc_mutex_lock ( &(this->mt_buffer) );

    pos = get_chunk_start_position (this->id_current_segment);

    TD ("pos=%d, new_pos=%d, chunk sz=%d, id_current_segment=%d\n", (int)pos, (int)new_pos, (int)(current_chunk_size), (int)(this->id_current_segment));

    if ((pos <= new_pos) && (new_pos < (pos + current_chunk_size))) {
        TD ("the position is inside current chunk, pos=%d, new_pos=%d, chunk sz=%d\n", (int)pos, (int)new_pos, (int)(current_chunk_size));
        is_inside_chunk = true;
    }

    // if the position is euqal current pos, do nothing
    if (new_pos == this->_get_current_read_position ()) {
        vlc_mutex_unlock ( &(this->mt_buffer) );
        TD ("same position as current, skip");
        return true;
    }
    if (is_inside_chunk) {
        size_t szseg = pos;
        TD ("process inside chunk.");
        size_t szbuf = 0;
        //vlc_mutex_lock ( &(this->mt_buffer) );
        szbuf = this->buffer.size();
        //vlc_mutex_unlock ( &(this->mt_buffer) );

        for (i = 0; i < szbuf; i ++) {
            seg = this->get_buffer_segment (i, this->id_current_segment);
            if (NULL != seg) {
                szseg += seg->get_size();
                if (szseg > new_pos) {
                    TD ("set relpos to pos=%d", (int)(seg->get_size() - (szseg - new_pos)));
                    seg->set_relpos (seg->get_size() - (szseg - new_pos));
                    i ++;
                    break;
                } else {
                    TD ("set relpos to max=%d", (int)seg->get_size());
                    seg->set_relpos(seg->get_size());
                }
            }
        }
        for (; i < szbuf; i ++) {
            seg = this->get_buffer_segment (i, this->id_current_segment);
            seg->set_relpos(0);
        }

    } else {
        TD ("process outside chunk.");
        // calculate the block
        if (pos + this->get_current_chunk_size () == new_pos) {
            // next chunk
            TD ("just in the start of next chunk.");
            this->id_current_segment ++;

        } else {
            // try next chunk size
            bool is_next1 = false;
            off_t nextsz = this->get_chunk_id_size (this->id_current_segment + 1);
            if (nextsz > 0) {
                assert (new_pos > pos);
                if (new_pos <= pos + this->get_current_chunk_size () + nextsz) {
                    this->id_current_segment ++;
                    is_next1 = true;
                }
            }
            if (! is_next1) {
                // try other chunks
                TD ("new_pos = %d, avg_size=%d", (int)new_pos, (int)this->avg_segment_size);
                int idx = new_pos / this->avg_segment_size;
                if (this->avg_segment_size > this->init_segment_size) {
                    idx ++;
                }
                if (idx >= this->total_segment) {
                    vlc_mutex_unlock ( &(this->mt_buffer) );
                    TE ("idx(%d) > total-segment(%d)", (int)idx, (int)(this->total_segment) );
                    return false;
                }
                this->id_current_segment = idx;
            }
        }
        TD ("outside chunk at new id (%d).", (int)this->id_current_segment);

        // check if there's such idx in the buffer
        // if not, set the position for mpd_adaptationlogic_t, by seconds
        //if (0) {
            TD ("seek to new id(%d).", (int)(this->id_current_segment));
            this->interlogic->seek (this->id_current_segment);
        //}
        // set the position for buffer
        size_t szbuf = 0;
        for (i = 0; i < szbuf; i ++) {
            seg = this->get_buffer_segment (i, this->id_current_segment);
            if (NULL != seg) {
                seg->set_relpos (0);
            } else {
                // no found such segment
                // TODO: send requests?
                TD ("TODO: not found segment, channel(%d), new id(%d).", i, (int)(this->id_current_segment));
            }
        }
        // recalculate the blocks
        this->estimate_media_byte_length ();
    }
    // TODO: set next chunk read position to 0, in case of rewind back to previous chunk.
    TRACE_SETPOS(0, this->id_current_segment, 0, this->_get_current_read_position());
    vlc_mutex_unlock ( &(this->mt_buffer) );
    TD ("set position successfully.");
    return true;
}

bool
vdash_streambuf_libmpd_t::seek (off_t offset, int whence)
{
    // we use lock/unlock in set_read_position, so here's no need to use it.
    switch (whence) {
    case SEEK_SET:
        if (offset < 0) {
            return false;
        }
        return this->set_read_position (offset);

    case SEEK_CUR:
        return this->set_read_position (this->get_current_read_position0 () + offset);

    case SEEK_END:
        return this->set_read_position (this->get_size_of_media () + offset);
    }
    return false;
}

ssize_t
vdash_streambuf_libmpd_t::read (void * buf, size_t len)
{
    ssize_t szrd = 0;
    vdash_downsegment_t * seg = NULL;
    TD ("read: sz=%d\n", (int)len);

    vlc_mutex_lock ( &(this->mt_buffer) );

    seg = this->get_current_read_segment ();
    if (NULL == seg) {
        // wait
        // The locker should not be unlocked again when a EINVAL error occur.
        mtime_t start = mdate();
        mtime_t timeout_limit = start + (10 * UINT64_C(1000000));
        int res = vlc_cond_timedwait (&(this->ct_buffer), &(this->mt_buffer), timeout_limit);
        switch (res) {
        case ETIMEDOUT:
            TE ("timeout limit reached!");
            vlc_mutex_unlock(&(this->mt_buffer));
            return -1;
        case EINVAL: // lock is not locked so we can just return
            return -1;
        }
        seg = this->get_current_read_segment ();
    }
    if (NULL == seg) {
        vlc_mutex_unlock ( &(this->mt_buffer) );
        return -1;
    }
    szrd = seg->read ((void*)buf, len);
    TRACE_READ(seg->get_category1(), seg->get_id(), this->_get_current_read_position(), len, szrd);
    //HEXDUMP ((unsigned char *)buf, szrd);

    TD ("feed_requests (start id=%d) ...", (int)(this->id_requested_segment));
    ssize_t ret1 = this->feed_requests (this->id_requested_segment); /* we place here because the new request should only after the content be read by the player */
    if (ret1 >= 0) {
        this->id_requested_segment = ret1;
    }
    TD ("END of feed_requests");

    vlc_mutex_unlock ( &(this->mt_buffer) );

    return szrd;
}

ssize_t
vdash_streambuf_libmpd_t::peek ( const uint8_t **pp_peek, size_t i_peek )
{
    ssize_t ret = 0;
    vdash_downsegment_t * seg = NULL;
    TD ("peek: sz=%d\n", (int)i_peek);

    vlc_mutex_lock ( &(this->mt_buffer) );

    seg = this->get_current_read_segment ();
    if (NULL == seg) {
        // wait
        // The locker should not be unlocked again when a EINVAL error occur.
        mtime_t start = mdate();
        mtime_t timeout_limit = start + (10 * UINT64_C(1000000));
        int res = vlc_cond_timedwait (&(this->ct_buffer), &(this->mt_buffer), timeout_limit);
        switch (res) {
        case ETIMEDOUT:
            TE ("timeout limit reached!");
            vlc_mutex_unlock(&(this->mt_buffer));
            return -1;
        case EINVAL: // lock is not locked so we can just return
            return -1;
        }
        seg = this->get_current_read_segment ();
    }
    if (NULL == seg) {
        vlc_mutex_unlock(&(this->mt_buffer));
        return -1;
    }
    ret = seg->peek (pp_peek, i_peek);
    TRACE_PEEK (seg->get_id(), seg->get_id(), this->_get_current_read_position(), i_peek, ret);
    //HEXDUMP ((unsigned char *)*pp_peek, ret);

    vlc_mutex_unlock ( &(this->mt_buffer) );
    TD ("peek: ret=%d\n", (int)ret);
    return ret;
}

bool
vdash_streambuf_libmpd_t::append_segment (mpd_chunk_t * chunk, vdash_downsegment_t * seg)
{
    assert (NULL != seg);
    assert (NULL != chunk);

    vlc_mutex_lock ( &(this->mt_buffer) );

    // how to keep the sequence when two seg are blocked at the same mutex?
    // set the req-id and sort the buffer list?
    int id = chunk->get_id();
    int catid = chunk->get_category();
    assert (catid < this->map_to_category.size());
    assert (this->map_to_category.size() == this->buffer.size());
#if DEBUG
    seg->set_category1 (catid);
#endif
    seg->set_id (id);
    seg->set_bitrate (chunk->get_bitrate());

    if (this->buffer[catid].end() == this->buffer[catid].find (id)) {
        this->buffer[catid][id] = seg;
    } else {
        // remove old one? or compare the bitrate of the segment?
        vdash_downsegment_t * seg0 = this->buffer[catid][id];
        if (seg0->get_bitrate () > seg->get_bitrate ()) {
            delete seg;
        } else {
            delete seg0;
            this->buffer[catid][id] = seg;
        }
    }
    // update the # of segments in buffer and the # of requested segments
    //assert (this->cnt_requested > 0);
    //if (this->cnt_requested > 0) {
    //    this->cnt_requested --;
    //}
    std::string lang("en");
    std::string type("video");
    double segduration = this->mpd->get_segment_duration(type, lang, 0, chunk->get_bitrate(), 1);
    this->buffer_stat[catid].requested -= segduration;
    this->buffer_stat[catid].received += segduration;

    // update the 0(init) size
    if (0 == id) {
        this->update_init_size ();
    }
    vlc_cond_signal (&(this->ct_buffer));
    vlc_mutex_unlock ( &(this->mt_buffer) );
    return true;
}

void
vdash_streambuf_libmpd_t::bps_notify (size_t instant, size_t smooth)
{
    assert (NULL != this->interlogic);
    vlc_mutex_lock ( &(this->mt_buffer) );
    this->interlogic->notify_avg_throughput (instant);
    vlc_mutex_unlock ( &(this->mt_buffer) );
}

#endif // LIBDASH_USE_DASH_DOWNLOAD
