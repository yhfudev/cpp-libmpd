/**
 * @file    vlc-api.cc
 * @brief   api for VLC DASH module
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-09-05
 */
/*****************************************************************************
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "vlc-api.h"

/*****************************************************************************
 * Module descriptor
 *****************************************************************************/

static int
Open (vlc_object_t *p_obj)
{
    return libmpd_vlc_open (p_obj);
}

static void
Close (vlc_object_t *p_obj)
{
    libmpd_vlc_close (p_obj);
}

#define DASH_WIDTH_TEXT N_("Preferred Width")
#define DASH_WIDTH_LONGTEXT N_("Preferred Width")

#define DASH_HEIGHT_TEXT N_("Preferred Height")
#define DASH_HEIGHT_LONGTEXT N_("Preferred Height")

#define DASH_BUFFER_TEXT N_("Buffer Size (Seconds)")
#define DASH_BUFFER_LONGTEXT N_("Buffer size in seconds")

//vlc_module_begin ()
//    set_shortname( N_("MPDASH"))
//    set_description( N_("Dynamic Adaptive Streaming over HTTP") )
//    set_capability( "stream_filter", 19 )
//    set_category( CAT_INPUT )
//    set_subcategory( SUBCAT_INPUT_STREAM_FILTER )
//    add_integer( "dash-prefwidth",  480, DASH_WIDTH_TEXT,  DASH_WIDTH_LONGTEXT,  true )
//    add_integer( "dash-prefheight", 360, DASH_HEIGHT_TEXT, DASH_HEIGHT_LONGTEXT, true )
//    add_integer( "dash-buffersize", 30, DASH_BUFFER_TEXT, DASH_BUFFER_LONGTEXT, true )
//    set_callbacks( Open, Close )
//vlc_module_end ()

#if USE_DASH_DEMUX
vlc_module_begin ()
    set_category( CAT_INPUT )
    set_subcategory( SUBCAT_INPUT_DEMUX )
    set_description(N_("Dynamic Adaptive Streaming over HTTP (demux)"))
    set_capability( "access_demux", 200 )
    set_callbacks( Open, Close )
    add_shortcut("dashmpd")
vlc_module_end ()

#else // USE_DASH_DEMUX

vlc_module_begin()
    set_category(CAT_INPUT)
    set_subcategory(SUBCAT_INPUT_STREAM_FILTER)
    set_description(N_("Dynamic Adaptive Streaming over HTTP"))
    set_capability("stream_filter", 20)
    set_callbacks(Open, Close)
vlc_module_end()

#endif // USE_DASH_DEMUX
