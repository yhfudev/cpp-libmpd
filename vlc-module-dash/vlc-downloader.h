/**
 * @file    vlc-downloader.h
 * @brief   VLC DASH Downloader module
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-09-05
 */
/*****************************************************************************
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
/**
 * TODO:
 *   *) how to manage the buffer, with the forward/rewind supports.
 *      save the bitrate with the received segments,
 *      keep the maximum bitrate record for current connection
 *      drop/replace the segment with a higher bitrate
 *      save the segment to temp file
 */
#ifndef VLC_DOWNLOADER_H
#define VLC_DOWNLOADER_H

#if LIBDASH_USE_DASH_DOWNLOAD
#else

#include <assert.h>

#include <queue>
#include <deque>

#include <vlc_common.h>
#include <vlc_threads.h>

#include "mympdalg.h"

/*
 * we need a thread and a timer, to handle the download and stat works.
 */

class vdash_runnable_t {
public:
    vdash_runnable_t ();
    ~vdash_runnable_t ();
    virtual bool start (void);
    virtual void stop (void);
    virtual void pause (bool is_paused = true);

    virtual void join (void);
    virtual void * work (void) = 0;

    void set_num_worker (size_t num_threads) { this->num_worker = num_threads; }

protected:
    bool is_stop ();

private:
    bool flg_stop;
    bool flg_paused;
    size_t num_worker;
    //vlc_thread_t worker;
    std::vector<vlc_thread_t> workers;
    vlc_mutex_t mt_flag;
    vlc_cond_t ct_flag;
};

inline
vdash_runnable_t::vdash_runnable_t ()
: flg_stop(false), flg_paused(false), num_worker(1)
{
    vlc_mutex_init( &(this->mt_flag) );
    vlc_cond_init( &(this->ct_flag) );
}

inline
vdash_runnable_t::~vdash_runnable_t ()
{
    this->stop ();
    this->join ();
    vlc_mutex_destroy ( &(this->mt_flag) );
    vlc_cond_destroy ( &(this->ct_flag) );
}

inline void
vdash_runnable_t::pause (bool is_paused)
{
    this->flg_paused = is_paused;
    vlc_cond_signal (&(this->ct_flag));
}

inline bool
vdash_runnable_t::is_stop ()
{
    for (; this->flg_paused; ) {
        vlc_mutex_lock ( &(this->mt_flag) );
        vlc_cond_wait (&(this->ct_flag), &(this->mt_flag) );
        vlc_mutex_unlock ( &(this->mt_flag) );
    }
#if 1
    return this->flg_stop;
#else
    bool ret = false;
    vlc_mutex_lock ( &(this->mt_flag) );
    ret = this->flg_stop;
    vlc_mutex_unlock ( &(this->mt_flag) );
    return ret;
#endif
}

/**
 * @brief helper to gather records for throughput stat
 */
class vdash_fix_array_t {
public:
    vdash_fix_array_t (size_t max0) : utcount(0), max(max0), sum(0)  {}

    size_t get_size () { return records.size(); }
    size_t get_untouched_count () { return this->utcount; }
    void insert_value (size_t val) {
        for (; this->records.size () >= this->max; ) {
            assert (this->sum >= this->records.front ());
            this->sum -= this->records.front ();
            this->records.pop ();
        }
        this->sum += val;
        this->records.push (val);
        this->utcount ++;
    }
    void set_max (size_t max0) {
        for (; this->records.size () > max0; ) {
            assert (this->sum >= this->records.front ());
            this->sum -= this->records.front ();
            this->records.pop ();
        }
        this->max = max0;
    }
    size_t get_sum () { utcount = 0; return this->sum; }

private:
    size_t utcount; /*<= how many times of calling insert_value() since last get_sum() */
    size_t max; /*<= max number of records in the queue */
    size_t sum; /*<= the sum of records in the queue */
    std::queue<size_t> records; /*<= the records */
};

// on memory or file
//vdash_downsegment_t
class vdash_downsegment_t {
public:
    vdash_downsegment_t () : posrd(0), bitrate(0), size(0), id(0) {}
    virtual bool seek (off_t related_off) = 0;
    virtual ssize_t read (void * buffer, size_t size) = 0;
    virtual ssize_t peek (const uint8_t **pp_peek, size_t size) = 0;

    virtual bool append_data (void *buffer, size_t size) = 0;

    off_t get_size (void) { return this->size; } /*<= get the size of this cache peice */
    off_t get_relpos (void) { return this->posrd; } /*<= get the related read position to the begin */
    void set_size (off_t size0) { this->size = size0; } /*<= set the size of this cache */
    void set_relpos (off_t pos0) { this->posrd = pos0; }

    size_t get_bitrate (void) { return this->bitrate; } /*<= get the bitrate of this segment */
    void set_bitrate (size_t bitrate0) { this->bitrate = bitrate0; } /*<= set the bitrate of this segment */

    //off_t get_start () { return this->start; }; /*<= get the absolute position of the begin */
    //void set_start (off_t start_offset) { this->start = start_offset; } /*<= set the absolute position of the begin of this cache */
    void set_id (int id0) { this->id = id0; }
    int get_id (void) { return this->id; }

#if DEBUG
    void set_category1 (size_t c0) { this->category1 = c0; }
    int get_category1 (void) { return this->category1; }
private:
    size_t category1;
#else
    int get_category1 (void) {return this->get_id();} // fake
#endif

protected:
    off_t posrd;  /*<= current read position, related */

private:
    size_t bitrate;
    off_t size; /*<= the size of the data in this segment */
    //off_t start; /*<= the absolute position of the begin */
    int id; /*<= the id of segment of the media stream */
};

// segment in memory
class vdash_downsegment_mem_t : public vdash_downsegment_t {
public:
    vdash_downsegment_mem_t () : buffer(NULL), sz_buf(0) {}
    virtual bool seek (off_t related_off);
    virtual ssize_t read (void * buffer, size_t size);
    virtual ssize_t peek (const uint8_t **pp_peek, size_t size);
    virtual bool append_data (void *buffer, size_t size);

private:
    ssize_t peek_val (void * buffer, size_t size);
    unsigned char * buffer;
    size_t sz_buf;
};

//vdash_downsegment_mem_and_disk_t

#if 0
// the callback interface for buffer
class vdash_api_buffer_t {
public:
    virtual void buffer_notify (double sec_max, double sec_cur) = 0;
};
#endif

// the callback interface for buffer in the streambuf
class vdash_api_buffer_t {
public:
    virtual bool append_segment (mpd_chunk_t * chunk, vdash_downsegment_t *seg) = 0;
};

// the callback interface for downloader
class vdash_api_received_t {
public:
    virtual void received_notify (size_t instant) = 0;
};

// the callback interface for throughput stat
class vdash_api_bps_t {
public:
    virtual void bps_notify (size_t instant, size_t smooth) = 0;
};

/**
 * @brief helper to stat throughput
 *  this class is implemented with a thread, which calculate the bps with a 'timer'
 */
class vdash_throughput_stat_t : public vdash_runnable_t, public vdash_api_received_t {
public:
    vdash_throughput_stat_t (size_t max0);
    ~vdash_throughput_stat_t ();
    virtual void * work (void);
    //virtual void stop (void);
    virtual void received_notify (size_t instant);

    bool register_notify (vdash_api_bps_t * v);
    bool unregister_notify (vdash_api_bps_t * v);

private:
    void notify (size_t instant, size_t smooth);

    vdash_fix_array_t rec; /*<= the records */

    vlc_mutex_t mt_bps; /*<= lock cur_bytes */
    vlc_cond_t ct_bps; /*<= if the callback is too fast, then break the sleep early. */
    size_t cur_bytes; /*<= the acculmulated received byte size */
    size_t times_cb;
    mtime_t start_time;

    std::vector<vdash_api_bps_t *> notify_targets; /*<= the targets to be notifed by the changed bps  */
};

inline
vdash_throughput_stat_t::vdash_throughput_stat_t (size_t max0)
: rec(max0), cur_bytes(0), times_cb(0), start_time(0)
{
    vlc_mutex_init ( &(this->mt_bps) );
    vlc_cond_init ( &(this->ct_bps) );
}

inline
vdash_throughput_stat_t::~vdash_throughput_stat_t ()
{
    vlc_mutex_destroy ( &(this->mt_bps) );
    vlc_cond_destroy ( &(this->ct_bps) );
}

class vdash_downrequest_t {
public:
    virtual ~vdash_downrequest_t() {}
    virtual bool on_finish (vdash_downsegment_t * seg) = 0;
    virtual mpd_chunk_t * get_request (void) = 0;
    //bool operator == (vdash_downrequest_t & other);
};

//inline bool
//vdash_downrequest_t::operator == (vdash_downrequest_t & other)
//{
//    mpd_chunk_t * thischk = this->get_request();
//    mpd_chunk_t * otherchk = other.get_request();
//    return (*thischk == *otherchk);
//}

class vdash_downrequest_mpd_t : public vdash_downrequest_t {
public:
    vdash_downrequest_mpd_t (mpd_chunk_t * chunk0, vdash_api_buffer_t * buffer0) : chunk(chunk0), buffer(buffer0) {}
    ~vdash_downrequest_mpd_t();
    virtual bool on_finish (vdash_downsegment_t * seg);
    virtual mpd_chunk_t * get_request (void) { assert (NULL != this->chunk); return this->chunk; }

private:
    mpd_chunk_t * chunk;
    vdash_api_buffer_t * buffer;
};

/**
 * @brief download content by using VLC's downloader
 */
class vdash_downloader_t : public vdash_runnable_t {
public:
    vdash_downloader_t (stream_t *s0);
    ~vdash_downloader_t ();
    virtual bool start (void);
    virtual void stop (void);
    virtual void * work (void);
    //virtual void stop (void);

    bool request (vdash_downrequest_t * req0); /*<! add new request to the waiting list */
    void cancel_all_works (void); /*<! stop all requests and the thread */
    bool exist_request (mpd_chunk_t * chunk); /*!< check if one request exist in the waiting list */
    size_t requests_number (void) {return requests.size();} /*<! the number of the requested chunks */

    bool register_notify (vdash_api_received_t * v); /*!< register a listener; the listener will be nitified once some bytes received. */
    bool unregister_notify (vdash_api_received_t * v);

private:
    void notify (size_t byte_received); /*!< called by working thread, after read some bytes. */
    int vlc_download ( vdash_downsegment_t *segment, mpd_chunk_t * chunk );
    bool is_cancel_current_work (void) { return this->flg_cancel_current; }

    stream_t * s; /*<= VLC stream_t for downloading chunks */

    vlc_cond_t  cond_down;
    vlc_mutex_t mt_down; /*<= locker */
    std::vector<vdash_api_received_t *> notify_targets; /*<= the targets to be notifed by the received bytes  */
    std::deque<vdash_downrequest_t *> requests; /*<= the requested */

    bool flg_cancel_current;
};

#endif // LIBDASH_USE_DASH_DOWNLOAD

#endif // VLC_DOWNLOADER_H
