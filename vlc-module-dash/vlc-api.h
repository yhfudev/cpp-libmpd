/**
 * @file    vlc-api.h
 * @brief   api for VLC DASH module
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-09-05
 */
/*****************************************************************************
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifndef VLC_API_DASH_IMPLE_H
#define VLC_API_DASH_IMPLE_H

#include "vlc-debugfunc.h"

VLC_EXTERN int libmpd_vlc_open  (vlc_object_t *);
VLC_EXTERN void libmpd_vlc_close (vlc_object_t *);

#endif // VLC_API_DASH_IMPLE_H
