/**
 * @file    vlc-dash.h
 * @brief   VLC DASH module
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-09-05
 */
/*****************************************************************************
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifndef VLC_DASH_IMPLE_H
#define VLC_DASH_IMPLE_H
// TODO:
// 1. buffer. use sorted array, or use map/queue to support instance access and sequence read(buffer)
// 2. svn version 28 can be compiled with mpd+dash 2.0.8;
//         29,32, work if fix STREAM_CAN_FASTSEEK
//         33, work if fix get_current_read_position0
//         34, work if fix define LIBDASH_USE_DASH_DOWNLOAD 0
//         38-41, work if fix def(LIBDASH_USE_DASH_DOWNLOAD), STREAM_CAN_FASTSEEK, get_current_read_position0
//         42, work if fix STREAM_CAN_FASTSEEK, get_current_read_position0
//         43-45, work if fix STREAM_CAN_FASTSEEK
//         46- works.
// 3. svn version 49 works for 2.0.8-libmpd, not support libav Width/Height change?.
// 4. svn version 50 works for vlc-git

// VLC 2.0.8, defined in Makefile.am/Module.am
//#define LIBDASH_USE_208 0
//#define LIBDASH_USE_DASH_DOWNLOAD 1

#include <math.h>
#include <stdint.h> // UINT64_C(), add -D__STDC_CONSTANT_MACROS in your compiler commanline

#include <map>
#include <vector>

#include "vlc-api.h"

#include "mympd.h"

#include "mympdalg.h"

#if 1 //DEBUG
#define TRACE_READ(cid, id, pos, sz, ret)   TW ("DASHIO: READ, category=%d, id=%d, current pos=%d, size=%d, ret=%d", (int)(cid), (int)(id), (int)(pos), (int)(sz), (int)(ret))
#define TRACE_PEEK(cid, id, pos, sz, ret)   TW ("DASHIO: PEEK, category=%d, id=%d, current pos=%d, size=%d, ret=%d", (int)(cid), (int)(id), (int)(pos), (int)(sz), (int)(ret))
#define TRACE_GETPOS(cid, id, pos, chunksz) TW ("DASHIO: GETPOS, category=%d, id=%d, current pos=%d", (int)(cid), (int)(id), (int)(pos), (int)(chunksz))
#define TRACE_SETPOS(cid, id, pos, posnew)  TW ("DASHIO: SETPOS, category=%d, id=%d, current pos=%d, new pos=%d", (int)(cid), (int)(id), (int)(pos), (int)(posnew))
#define TRACE_GETSIZE(cid, id, pos, sz)     TW ("DASHIO: GETSIZE, category=%d, id=%d, current pos=%d, media size=%d", (int)(cid), (int)(id), (int)(pos), (int)(sz))
#else
#define TRACE_READ(cid, id, pos, sz, ret)
#define TRACE_PEEK(cid, id, pos, sz, ret)
#define TRACE_GETPOS(cid, id, pos, chunksz)
#define TRACE_SETPOS(cid, id, pos, posnew)
#define TRACE_GETSIZE(cid, id, pos, sz)
#endif

class vdash_streambuf_t {
public:
    virtual bool start0 (void) = 0;
    virtual bool stop (void) = 0;

    // for byte base interface:
    virtual bool seek (off_t offset, int whence = SEEK_CUR) = 0;
    virtual ssize_t read (void * buf, size_t len) = 0;
    virtual ssize_t peek( const uint8_t **pp_peek, size_t i_peek ) = 0;
    virtual void set_paused (bool is_paused = true) = 0;

    virtual off_t get_size_of_media (void) = 0; /*!< the size of video+audio calculated by throughput(not exact size) for upper layer */
    virtual off_t get_current_read_position0 (void) = 0; /*!< the current read position, precisely in current segment */

    // for time base interfaces:
#if USE_DASH_DEMUX
    virtual double get_position_time (void) = 0; /*!< the current time position of the stream */
#endif
};

#if LIBDASH_USE_DASH_DOWNLOAD

// use Libdash http downloader
#include "http/HTTPConnectionManager.h"
#include "adaptationlogic/AbstractAdaptationLogic.h"

#if LIBDASH_USE_208
#include "exceptions/EOFException.h"
#else
#include "DASHDownloader.h"
#endif

class vdash_adaptlogic_t : public dash::logic::AbstractAdaptationLogic {
public:
    //vdash_adaptlogic_t (dash::mpd::IMPDManager *mpdManager, stream_t *stream) : intelogic(NULL), AbstractAdaptationLogic(NULL,NULL) {}
    vdash_adaptlogic_t (mpd_t *mpd0)
      :
#if LIBDASH_USE_208
      AbstractAdaptationLogic(NULL)
#else
      AbstractAdaptationLogic(NULL,NULL)
#endif
      , intelogic(NULL)
    {init(mpd0);}
    //vdash_adaptlogic_t () : intelogic(NULL) : AbstractAdaptationLogic(NULL, NULL) {}
    bool init (mpd_t *mpd0);

    virtual dash::http::Chunk * getNextChunk ()
#if LIBDASH_USE_208
      throw(dash::exception::EOFException)
#endif
    ;
    virtual const dash::mpd::Representation * getCurrentRepresentation() const;
    virtual void downloadRateChanged (uint64_t bpsAvg, uint64_t bpsLastChunk);
    //virtual void bufferLevelChanged (mtime_t bufferedMicroSec, int bufferedPercent);

private:
    mpd_adaptationlogic_t * intelogic;
};

inline bool
vdash_adaptlogic_t::init (mpd_t *mpd0)
{
    this->intelogic = new mpd_adaptationlogic_yhfu_t (mpd0);
    if (NULL != this->intelogic) {
        return true;
    }
    return false;
}

inline const dash::mpd::Representation *
vdash_adaptlogic_t::getCurrentRepresentation() const
{
    throw new std::string("Not avaiable any more! vdash_adaptlogic_t::getCurrentRepresentation()");
    return NULL;
}

inline dash::http::Chunk *
vdash_adaptlogic_t::getNextChunk()
#if LIBDASH_USE_208
      throw(dash::exception::EOFException)
#endif
{
    return intelogic->get_next_chunk ("video", "en");
}

inline void
vdash_adaptlogic_t::downloadRateChanged (uint64_t bpsAvg, uint64_t bpsLastChunk)
{
    dash::logic::AbstractAdaptationLogic::downloadRateChanged (bpsAvg, bpsLastChunk);
    //msg_Dbg( s, "[DBG] bandwidth received: avg=%d, last=%d\n", (int)bpsAvg, (int)bpsLastChunk);
    //printf ("[DBG] bandwidth received: avg=%d, last=%d\n", (int)bpsAvg, (int)bpsLastChunk);
    std::cout << "[DBG] bandwidth received: avg=" << bpsAvg << ", last=" << bpsLastChunk << std::endl;
    intelogic->notify_avg_throughput (bpsAvg);
    //intelogic->notify_avg_throughput (bpsLastChunk);
}

class vdash_streambuf_libdash_t : public vdash_streambuf_t {
public:
    vdash_streambuf_libdash_t (mpd_t * mpd0, stream_t * stream0)
        : mpd(mpd0), stream(stream0), position(0)
        , conManager(NULL), adaptationLogic(NULL)
#if LIBDASH_USE_208
          , currentChunk(NULL)
#else
          , downloader(NULL)
          , buffer(NULL)
#endif
#if 1 //DEBUG
      , id_current_segment(0)
#endif
        {}
    ~vdash_streambuf_libdash_t ();
    virtual bool start0 (void);
    virtual bool stop (void);
    virtual bool seek (off_t offset, int whence = SEEK_SET);
    virtual ssize_t read (void * buf, size_t len);
    virtual ssize_t peek( const uint8_t **pp_peek, size_t i_peek );
    virtual void set_paused (bool is_paused = true) {}

    virtual off_t get_size_of_media (void) {
        const dash::mpd::Representation *rep = NULL;
        //rep = p_sys->p_dashManager->getAdaptionLogic()->getCurrentRepresentation();
        if ( rep != NULL ) {
            //return p_sys->p_mpd->getDuration() * rep->getBandwidth() / 8;
        }
        TRACE_GETSIZE(0, this->id_current_segment, this->get_current_read_position0(), 0);
        return 0;
    }
    virtual off_t get_current_read_position0 (void) { return this->position; }

private:
    bool set_read_position (off_t new_pos) {
        TRACE_SETPOS(0, this->id_current_segment, this->get_current_read_position0(), new_pos);
        this->position = new_pos;
        return true;
    }

    mpd_t * mpd;
    stream_t * stream;
    off_t position;

    dash::http::HTTPConnectionManager * conManager;
    dash::logic::IAdaptationLogic * adaptationLogic;
#if LIBDASH_USE_208
    dash::http::Chunk * currentChunk;
#else
    dash::DASHDownloader * downloader;
    dash::buffer::BlockBuffer * buffer;
#endif
#if 1//DEBUG
    size_t id_current_segment; /*!< current read segment id, start from 1 to total_segment */
#endif
};

#else // LIBDASH_USE_DASH_DOWNLOAD

#include "vlc-downloader.h"

class vdash_streambuf_libmpd_t : public vdash_streambuf_t, public vdash_api_buffer_t, public vdash_api_bps_t {
public:
    vdash_streambuf_libmpd_t (mpd_t * mpd0, stream_t * stream0)
        : stream(stream0), mpd(mpd0), tpstat(10), downer(stream0), interlogic(NULL)
        , id_current_segment(0), id_requested_segment(0), init_segment_size(0), avg_segment_size(0), total_segment(0)
        {
            vlc_mutex_init( &(this->mt_buffer) );
            vlc_cond_init( &(this->ct_buffer) );
        }
    ~vdash_streambuf_libmpd_t ();

    virtual bool start0 (void);
    virtual bool stop (void);
    virtual void set_paused (bool is_paused = true);

    /* We assume that only the downloader is producer(insert new data),
     * and only the player is the consumer (remove data)
     * We also implemented the downloader as a seperate thread from the player,
     * the data exchanging between the downloader and player is using the buffer.
     * So, for the lock/unlock critical section(the buffer in this case),
     * we only apply the locks to the interface(API),
     * the internal functions would not use the lock/unlock process.
     *
     * buffer: the new data will be inserted by the downloader
     *     the player thread (call read() etc.) will manage the buffer by API read/seek
     *     to keep the memory usage lower than the maximum size.
     *     The following API will use lock/unlock for buffer:
     *        append_segment (downloader)
     *        seek/read/peek/get_size_of_media/get_current_read_position/set_read_position (player)
     *        bps_notify (tpstat)
     *
     * stat structure: the downloader will update the received size to the tpstat
     *     so there's lock/unlock in tpstat
     */
    virtual bool seek (off_t offset, int whence = SEEK_SET);
    virtual ssize_t read (void * buf, size_t len);
    virtual ssize_t peek( const uint8_t **pp_peek, size_t i_peek );

    //virtual bool set_percent (double posperc); /*!< the % of the content */

    virtual off_t get_size_of_media (void); /*!< the size of video+audio calculated by throughput(not exact size) for upper layer */
    virtual off_t get_current_read_position0 (void); /*!< the current read position, precisely in current segment */

    virtual double get_position_time (void) {if (NULL == interlogic) return -1; return interlogic->tell();} /*!< the current time position of the stream */

    virtual bool append_segment (mpd_chunk_t * chunk, vdash_downsegment_t *seg);
    virtual void bps_notify (size_t instant, size_t smooth);

    ssize_t feed_requests (size_t idx_request);

private:
    void update_init_size ();
    off_t _get_current_read_position ();
    bool set_read_position (off_t new_pos); /*!< (lock/unlock) set the read position, precisely in current segment, roughly in other segment(will set to the begining of that segment) */

    stream_t * stream;
    mpd_t * mpd;
    vdash_throughput_stat_t tpstat; /*!< the stat info of the received data */
    vdash_downloader_t downer;      /*!< downloader of the segments */
    mpd_adaptationlogic_t * interlogic; /*!< adaptation logic */

    bool estimate_media_byte_length (); /*!< estimate the byte size of media */
    off_t get_chunk_id_size (int id); /*!< the size of current chunk(video+audio) */
    off_t get_current_chunk_size (void) { return this->get_chunk_id_size (this->id_current_segment); }

    off_t get_chunk_start_position (int id);

    // the buffer,
    vlc_mutex_t mt_buffer; /*!< the locker for buffer, both player and downloader thread will access buffer */
    vlc_cond_t ct_buffer; /*!< signal message for buffer, the player block on read() and need be signaled by downloader once received data */
    std::vector<std::map<int, vdash_downsegment_t *> > buffer; /*!< media buffer queue. catid=map_to_category("video"); buffer[catid][id] */
    //std::vector<std::deque<vdash_downsegment_t *> > buffer; /*!< media buffer queue. catid=map_to_category("video"); buffer[catid][id] */

    std::map<std::string, int> map_to_category; /*!< string to category id, such as 'video'->0 */
    class _stat {
    public:
        _stat (void) : received(0), requested(0) {}
        void init (void) { received = 0; requested = 0;}
        double received;
        double requested;
    };
    std::vector<_stat> buffer_stat; /*!< the buffer content size (in seconds) */

    size_t id_current_segment; /*!< current read segment id, start from 1 to total_segment */
    size_t id_requested_segment; /*!< the next requested segment id, start from 1 to total_segment */
    //size_t cnt_requested; /*!< the # of request time blocks */

    size_t init_segment_size; /*!< the size of init chunk, it is used to prevent unable to seek back to init chunk due to low average chunk size */
    size_t avg_segment_size; /*!< the calculated average segment size */
    size_t total_segment; /*!< total number of segments, not includes the init segment */

    vdash_downsegment_t * get_buffer_segment (int category, int id); /*!< get the original segment structure */
    vdash_downsegment_t * get_current_read_segment (void);
    void clear_buffer (void); /*!< remove all of the vdash_downsegment_t * in buffer */
};

#endif // LIBDASH_USE_DASH_DOWNLOAD

#endif // VLC_DASH_IMPLE_H
