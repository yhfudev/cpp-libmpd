/**
 * @file    vlc-debugfunc.h
 * @brief   debug functions
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-12-15
 */
/*****************************************************************************
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifndef VLC_DEBUG_FUNCTIONS_H
#define VLC_DEBUG_FUNCTIONS_H

#include "vlcfix.h"

#define opaque_t unsigned char
VLC_EXTERN typedef int (*bhd_cb_writer_t) (void *fd, opaque_t * fragment, size_t size);
VLC_EXTERN size_t bulk_hex_dump (void *fd, opaque_t * fragment, size_t size, bhd_cb_writer_t writer, int use_c_style);

#if USE_VLCLIB
VLC_EXTERN int bhd_cb_writer_vlcstream (void *fd, opaque_t * fragment, size_t size);
#define hex_dump_to_vlcstream(fd_stream, fragment, size) bulk_hex_dump ((void *)(fd_stream), fragment, size, bhd_cb_writer_vlcstream, 0)
#define HEXDUMP(frag, size) hex_dump_to_vlcstream(g_dbg_stream, frag, size)
#else
VLC_EXTERN int bhd_cb_writer_fd (void *fd, opaque_t * fragment, size_t size);
#define HEXDUMP(frag, size) bulk_hex_dump ((void *)1, ((opaque_t *)(frag)), size, bhd_cb_writer_fd, 0)
#endif

// only for debug
//#define USE_DASH_DEMUX 1

#endif // VLC_DEBUG_FUNCTIONS_H
