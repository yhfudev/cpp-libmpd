/**
 * @file    vlc-downloader.cc
 * @brief   VLC DASH Downloader module
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-09-05
 */
/*****************************************************************************
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <errno.h>
#include <unistd.h> // sleep

#include "vlc-api.h"

#if LIBDASH_USE_DASH_DOWNLOAD
#else

#include "vlc-downloader.h"

#define INTERVAL 2

bool
vdash_downsegment_mem_t::seek (off_t related_off)
{
    if (related_off > this->get_size()) {
        related_off = this->get_size();
    }
    this->set_relpos (related_off);
    return true;
}

ssize_t
vdash_downsegment_mem_t::peek (const uint8_t **pp_peek, size_t size)
{
    if (NULL == pp_peek) {
        return -1;
    }
    if (size < 1) {
        return 0;
    }
    if (this->get_relpos () >= this->get_size()) {
        return 0;
    }
    if (size + this->get_relpos () > this->get_size()) {
        size = this->get_size() - this->get_relpos ();
    }
    if (size < 1) {
        return 0;
    }
    *pp_peek = this->buffer + this->get_relpos ();
    return size;
}

ssize_t
vdash_downsegment_mem_t::peek_val (void * buffer0, size_t size)
{
    if (size < 1) {
        return -1;
    }
    if (this->get_relpos () >= this->get_size()) {
        return 0;
    }
    if (size + this->get_relpos () > this->get_size()) {
        size = this->get_size() - this->get_relpos ();
    }
    if (size < 1) {
        return -1;
    }
    memmove (buffer0, this->buffer + this->get_relpos (), size);
    return size;
}

ssize_t
vdash_downsegment_mem_t::read (void * buffer0, size_t size)
{
    ssize_t sz = this->peek_val (buffer0, size);
    if (sz > 0) {
        this->set_relpos (this->get_relpos () + sz);
    }
    return sz;
}

vdash_downrequest_mpd_t::~vdash_downrequest_mpd_t()
{
    if (NULL != this->chunk) {
        delete this->chunk;
    }
}

bool
vdash_downrequest_mpd_t::on_finish (vdash_downsegment_t * seg)
{
    assert (NULL != this->buffer);
    assert (NULL != this->get_request());
    return this->buffer->append_segment (this->get_request(), seg);
}

bool
vdash_downsegment_mem_t::append_data (void *buffer, size_t size)
{
    if (this->get_size() + size >= this->sz_buf) {
        void *newbuf = NULL;
        size_t szbuf = (size + this->sz_buf) * 2;
        newbuf = realloc (this->buffer, szbuf);
        if (NULL != newbuf) {
            this->buffer = (unsigned char *)newbuf;
            this->sz_buf = szbuf;
        }
    }
    if (this->get_size() + size >= this->sz_buf) {
        return false;
    }
    memmove (this->buffer + this->get_size(), buffer, size);
    this->set_size (this->get_size() + size);
    return true;
}

void *
vdash_runnable_work0 (void *arg)
{
    vdash_runnable_t *thisp = (vdash_runnable_t *)arg;
    TI ("  start user defined work()");
    return thisp->work ();
}

// join the first # of threads
void
vdash_runnable_t::join (void)
{
    TI ("join thread ...\n");
    std::vector<vlc_thread_t>::iterator it;
    for (it = workers.begin(); (it != workers.end()); it ++) {
        vlc_join (*it, NULL);
    }
}

bool
vdash_runnable_t::start (void)
{
    vlc_mutex_lock ( &(this->mt_flag) );
    this->flg_paused = false;
    this->flg_stop = false;
    vlc_mutex_unlock ( &(this->mt_flag) );
    TI ("Start new thread");
    size_t i = 0;
    for (i = 0; i < this->num_worker; i ++) {
        if (this->workers.size() <= i) {
            this->workers.push_back((vlc_thread_t)0);
        }
        assert (this->workers.size() > i);
        if(vlc_clone (&(this->workers[i]), vdash_runnable_work0, (void*)this, VLC_THREAD_PRIORITY_LOW)) {
            //this->stop ();
            //this->join ();
            if (i < 1) {
                return false;
            }
            return true;
        }
    }
    return true;
}

void
vdash_runnable_t::stop (void)
{
    vlc_mutex_lock ( &(this->mt_flag) );
    this->flg_paused = false;
    this->flg_stop = true;
    vlc_cond_signal (&(this->ct_flag));
    vlc_mutex_unlock ( &(this->mt_flag) );
    // TODO: waiting for thread to exit?
}

void
vdash_throughput_stat_t::received_notify (size_t instant)
{
    vlc_mutex_lock ( &(this->mt_bps) );
    this->cur_bytes += instant;
    this->times_cb ++;
    if ((this->times_cb > 100) || (0 == instant)) {
        // too fast, told the upper user
        this->times_cb = 0;
        vlc_cond_signal (&(this->ct_bps));
    }
    vlc_mutex_unlock ( &(this->mt_bps) );
}

static double mtime_to_second (mtime_t date)
{
    lldiv_t d = lldiv (date, CLOCK_FREQ);
    double ret = d.quot;
    ret += (1.0 * (d.rem * (1000000000 / CLOCK_FREQ)) / 1000000000.0);

    return ret;
}

void *
vdash_throughput_stat_t::work (void)
{
    size_t avg = 0;
    size_t instant = 0;
    size_t cur_bytes0 = 0;

    TD ("BEGIN thread vdash_throughput_stat_t::%s", __func__);
    this->notify (instant, avg);
    for (; ! this->is_stop();) {
        /*TI ("sleep(%d)", INTERVAL);
        sleep (INTERVAL);
        */
        vlc_mutex_lock ( &(this->mt_bps) );
        this->start_time = mdate ();
        mtime_t timeout_limit = this->start_time + (INTERVAL * UINT64_C(1000000));
        int res = vlc_cond_timedwait (&(this->ct_bps), &(this->mt_bps), timeout_limit);
        switch (res) {
        case ETIMEDOUT:
            TE ("timeout limit reached!");
            break;
        case EINVAL: // lock is not locked so we can just return
            break;
        }
        vlc_mutex_unlock(&(this->mt_bps));

        if (this->is_stop()) {
            break;
        }
        mtime_t stop;

        vlc_mutex_lock ( &(this->mt_bps) );
        stop = mdate ();
        cur_bytes0 = this->cur_bytes;
        this->cur_bytes = 0;
        vlc_mutex_unlock ( &(this->mt_bps) );

        //if (cur_bytes0 > 0) {
            instant = (1.0 * cur_bytes0 * 2 / mtime_to_second (stop - this->start_time));
            this->rec.insert_value (instant);
            assert (this->rec.get_size () > 0);
            avg = this->rec.get_sum () / this->rec.get_size ();
            // notify
            TI ("notify(instant=%d, avg=%d)", (int)instant, (int)avg);
            this->notify (instant, avg);
            TI ("end of notify(instant=%d, avg=%d)", (int)instant, (int)avg);
        //}
    }
    TD ("END thread vdash_throughput_stat_t::%s", __func__);
    return NULL;
}

int
vdash_downloader_t::vlc_download ( vdash_downsegment_t *segment, mpd_chunk_t * chunk )
{
    assert (NULL != chunk);
    std::string & url = chunk->get_url ();
    int ret = VLC_SUCCESS;
    assert (NULL != this->s);
    assert (NULL != segment);

    stream_t * p_ts = stream_UrlNew ( this->s, url.c_str() );
    if( p_ts == NULL ) {
        return VLC_EGENERIC;
    }

    int64_t size = 0;
    int64_t sz = 0;
    char buf[500];

    // seek?
    if (chunk->has_byterange ()) {
        size = chunk->get_byte_end() - chunk->get_byte_start() + 1;
        //stream_Control(p_ts, STREAM_SET_POSITION, ??);
        stream_Seek (p_ts, chunk->get_byte_start());
    } else {
        size = stream_Size( p_ts );
    }

    TI("Download size=%d", (int)size);
    for (; size > 0; ) {
        sz = sizeof (buf);
        if (sz > size) {
            sz = size;
        }
        if ((this->is_cancel_current_work ()) || (this->is_stop())) {
            ret = VLC_EGENERIC;
            break;
        }
        int read = stream_Read ( p_ts, buf, sz );
        //TI("stream_Read() return %d", read);
        if (read <= 0) {
            break;
        }
        if (read < sz) {
            //TW( "sms_Download: I requested %"PRIi64" bytes, but I got only %i", sz, read );
        }
        if ((this->is_cancel_current_work ()) || (this->is_stop())) {
            ret = VLC_EGENERIC;
            break;
        }
        //TI("append data sz=%d", read);
        segment->append_data (buf, read);
        this->notify (read);
        size -= read;
        //TI("not received data %d", (int)size);
    }
    this->notify (0); /* notify the listener that the end of the block */

    stream_Delete( p_ts );

    TI("vlc download done");
    return ret;
}

bool
vdash_downloader_t::start (void)
{
    this->flg_cancel_current = false;
    return vdash_runnable_t::start ();
}

void
vdash_downloader_t::stop (void)
{
    this->flg_cancel_current = true;
    vdash_runnable_t::stop ();
}

void
vdash_downloader_t::cancel_all_works (void)
{
    TD ("BEGIN vdash_downloader_t::%s", __func__);
    vdash_downrequest_t *req = NULL;
    this->flg_cancel_current = true;
    vlc_mutex_lock ( &(this->mt_down) );
    for (; this->requests.size() > 0;) {
        req = this->requests.front();
        this->requests.pop_front(); //this->requests.pop ();
        assert (NULL != req);
        delete req;
    }
    vlc_mutex_unlock ( &(this->mt_down) );
    TD ("END vdash_downloader_t::%s", __func__);
}

void *
vdash_downloader_t::work (void)
{
    TD ("BEGIN thread vdash_downloader_t::%s", __func__);
    vdash_downrequest_t *req;
    for (; ! this->is_stop();) {
        req = NULL;
        vlc_mutex_lock ( &(this->mt_down) );
        TI ("down request queue size = %d", (int)requests.size ());
        if (requests.size () <= 0) {
            TI ("waiting for request queue ...");
            vlc_cond_wait (&(this->cond_down), &(this->mt_down) );
        }
        if (requests.size () > 0) {
            TI ("got one request from queue");
            req = requests.front();
            requests.pop_front(); //requests.pop ();
            assert (NULL != req);
        }
        vlc_mutex_unlock ( &(this->mt_down) );
        if (req) {
            TI ("process one request");
            assert (NULL != req->get_request ());
            vdash_downsegment_t * seg = new vdash_downsegment_mem_t ();
            TI ("vlc_download()");
            if (VLC_SUCCESS == this->vlc_download (seg, req->get_request ())) {
                TI ("on_finish (seg)");
                req->on_finish (seg);
                TI ("END of on_finish(id=%d, bitrate=%d, size=%d)", seg->get_id(), seg->get_bitrate(), seg->get_size());
            } else {
                // error
                TI ("some error in vlc_download()");
                delete seg;
            }
            delete req;
        }
    }
    TD ("END thread vdash_downloader_t::%s", __func__);
    return NULL;
}

vdash_downloader_t::vdash_downloader_t (stream_t *s0)
: flg_cancel_current(false)
{
    assert (NULL != s0);
    s = s0;

    vlc_mutex_init ( &(this->mt_down) );
    vlc_cond_init (&(this->cond_down));
}

vdash_downloader_t::~vdash_downloader_t ()
{
    vlc_mutex_destroy ( &(this->mt_down) );
    vlc_cond_destroy (&(this->cond_down));
}

bool
vdash_downloader_t::request (vdash_downrequest_t * req0)
{
    vlc_mutex_lock ( &(this->mt_down) );
    requests.push_back (req0); //requests.push (req0);
    TI ("down request queue size = %d", (int)requests.size ());
    vlc_cond_signal (&(this->cond_down));
    vlc_mutex_unlock ( &(this->mt_down) );
    return true;
}

bool
vdash_downloader_t::exist_request (mpd_chunk_t * req0)
{
    bool ret = false;
    vlc_mutex_lock ( &(this->mt_down) );
    for (std::deque<vdash_downrequest_t *>::iterator it = requests.begin(); it != requests.end(); ++ it) {
        if (*((*it)->get_request()) == *req0) {
            ret = true;
            break;
        }
    }
    vlc_mutex_unlock ( &(this->mt_down) );
    return ret;
}

// there's no protection for the register queue for performance/looplock consideration,
// so the user should call the functions at the begin/end of thread.
// TODO: ???
bool
vdash_downloader_t::register_notify (vdash_api_received_t * v)
{
    //vlc_mutex_lock ( &(this->mt_down) );
#if DEBUG
    bool flg_found = false;
    for (std::vector<vdash_api_received_t *>::iterator it = notify_targets.begin(); it != notify_targets.end(); ++ it) {
        if ( v == (*it)) {
            flg_found = true;
            break;
        }
    }
    if (flg_found) {
        // print message
    } else
#endif
        this->notify_targets.push_back (v);

    //vlc_mutex_unlock ( &(this->mt_down) );
    return true;
}

bool
vdash_downloader_t::unregister_notify (vdash_api_received_t * v)
{
    //vlc_mutex_lock ( &(this->mt_down) );
    for (std::vector<vdash_api_received_t *>::iterator it = notify_targets.begin(); it != notify_targets.end(); ++ it) {
        if ( v == (*it)) {
            notify_targets.erase (it);
            break;
        }
    }
    //vlc_mutex_unlock ( &(this->mt_down) );
    return true;
}

void
vdash_downloader_t::notify (size_t byte_received)
{
    //vlc_mutex_lock ( &(this->mt_down) );
    for (std::vector<vdash_api_received_t *>::iterator it = notify_targets.begin(); it != notify_targets.end(); ++ it) {
        (*it)->received_notify (byte_received);
    }
    //vlc_mutex_unlock ( &(this->mt_down) );
}

bool
vdash_throughput_stat_t::register_notify (vdash_api_bps_t * v)
{
    vlc_mutex_lock ( &(this->mt_bps) );
#if DEBUG
    bool flg_found = false;
    for (std::vector<vdash_api_bps_t *>::iterator it = notify_targets.begin(); it != notify_targets.end(); ++ it) {
        if ( v == (*it)) {
            flg_found = true;
            break;
        }
    }
    if (flg_found) {
        // print message
        vlc_mutex_unlock ( &(this->mt_bps) );
        return true;
    } else
#endif
        this->notify_targets.push_back (v);

    vlc_mutex_unlock ( &(this->mt_bps) );
    return true;
}

bool
vdash_throughput_stat_t::unregister_notify (vdash_api_bps_t * v)
{
    vlc_mutex_lock ( &(this->mt_bps) );
    for (std::vector<vdash_api_bps_t *>::iterator it = notify_targets.begin(); it != notify_targets.end(); ++ it) {
        if ( v == (*it)) {
            notify_targets.erase (it);
        }
    }
    vlc_mutex_unlock ( &(this->mt_bps) );
    return true;
}

void
vdash_throughput_stat_t::notify (size_t instant, size_t smooth)
{
    vlc_mutex_lock ( &(this->mt_bps) );
    for (std::vector<vdash_api_bps_t *>::iterator it = notify_targets.begin(); it != notify_targets.end(); ++ it) {
        (*it)->bps_notify (instant, smooth);
    }
    vlc_mutex_unlock ( &(this->mt_bps) );
}

#endif // LIBDASH_USE_DASH_DOWNLOAD
