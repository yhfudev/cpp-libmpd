/**
 * @file    mympd2string.cc
 * @brief   verify and convert to string from the DASH MPD file data structures mapped directly from the description of the
 *          ISO/IEC 23009-1:2012 Dynamic adaptive streaming over HTTP (DASH) -- Part 1: Media presentation description and segment formats
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-04-16
 */

#include <stdio.h>
#include <assert.h>

#include "mympd.h"

/**
 * @brief format the URL to xml string
 *
 * @return the XML string
 *
 * format the URL to xml string
 */
std::string
mpd_url_t::toString (void)
{
    std::string str = std::string("<");
    str += this->get_title();

    // attributes
    if (this->sourceURL.size () > 0) {
        str += " sourceURL='" + this->sourceURL + "'";
    }
#if 1
    if (this->range.size () > 0) {
        str += " range='" + this->range + "'";
    }
#else
    if (this->range > 0) {
        str += " range='";
        std::ostringstream s;
        s << this->range;
        str += s.str();
        str += "'";
    }
#endif
    str += " />\n";

    return str;
}

std::string
mpd_descriptor_t::toString (void)
{
    std::string str("<");
    str += this->get_title();

    // attributes
    if (this->schemeIdUri.size () > 0) {
        str += " schemeIdUri='" + this->schemeIdUri + "'";
    }
    if (this->value.size () > 0) {
        str += " value='" + this->value + "'";
    }
    str += " />\n";
    return str;
}

/**
 * @brief verify if the content is valid or not
 *
 * @return false on failed, true on successed
 *
 * verify if the content is valid or not
 */
bool
mpd_descriptor_t::verify (void)
{
    if (this->schemeIdUri.size () < 1) {
        ERROR ("verify() error no schemeIdUri\n");
        return false;
    }
    return true;
}

bool
mpd_segmentcommon_t::verify (void)
{
    bool ret = true;
    size_t i;
    for (i = 0; i < this->BaseURL.size (); i ++) {
        if (! this->BaseURL[i]->verify ()) {
            ERROR ("verify() error in BaseURL[%" PRIuSZ "]\n", i);
            ret = false;
        }
    }
    if (NULL != this->SegmentTemplate) {
        if (! this->SegmentTemplate->verify ()) {
            ERROR ("verify() error in SegmentTemplate\n");
            ret = false;
        }
    }
    if (NULL != this->SegmentBase) {
        if (! this->SegmentBase->verify ()) {
            ERROR ("verify() error in SegmentBase\n");
            ret = false;
        }
    }
    if (NULL != this->SegmentList) {
        if (! this->SegmentList->verify ()) {
            ERROR ("verify() error in SegmentList\n");
            ret = false;
        }
    }
    return ret;
}

std::string
mpd_segmentcommon_t::toStringContent (void)
{
    size_t i;
    std::string str;
    for (i = 0; i < this->BaseURL.size (); i ++) {
        str += this->BaseURL[i]->toString ();
    }
    if (NULL != this->SegmentTemplate) {
        str += this->SegmentTemplate->toString ();
    }
    if (NULL != this->SegmentBase) {
        str += this->SegmentBase->toString ();
    }
    if (NULL != this->SegmentList) {
        str += this->SegmentList->toString ();
    }
    return str;
}

std::string
mpd_representation_base_t::toStringAttr (void)
{
    size_t i;
    std::string str;
    // attributes
    if (this->mimeType.size () > 0) {
        str += " mimeType='" + this->mimeType + "'";
    }
    if (this->codecs.size () > 0) {
        str += " codecs='" + this->codecs + "'";
    }
    if (this->width > 0) {
        str += " width='";
        std::ostringstream s;
        s << this->width;
        str += s.str();
        str += "'";
    }
    if (this->height > 0) {
        str += " height='";
        std::ostringstream s;
        s << this->height;
        str += s.str();
        str += "'";
    }
    if (this->profiles.size () > 0) {
        str += " profiles='";
        str += this->profiles[0];
        for (i = 1; i < this->profiles.size(); i ++) {
            str += ",";
            str += this->profiles[i];
        }
        str += "'";
    }
    if (this->sar.size () > 0) {
        str += " sar='" + this->sar + "'";
    }
    if (this->frameRate.size () > 0) {
        str += " frameRate='" + this->frameRate + "'";
    }
    if (this->audioSamplingRate.size () > 0) {
        assert (1 ==  this->audioSamplingRate.size () || 2 == this->audioSamplingRate.size ());
        str += " audioSamplingRate='";
        str += this->audioSamplingRate[0];
        for (i = 1; i < this->audioSamplingRate.size(); i ++) {
            str += " ";
            str += this->audioSamplingRate[i];
        }
        str += "'";
    }
    if (this->segmentProfiles.size () > 0) {
        str += " segmentProfiles='";
        str += this->segmentProfiles[0];
        for (i = 1; i < this->segmentProfiles.size(); i ++) {
            str += ",";
            str += this->segmentProfiles[i];
        }
        str += "'";
    }
    if (this->scanType.size () > 0) {
        str += " scanType='" + this->scanType + "'";
    }
    if (this->maximumSAPPeriod > 0) {
        str += " maximumSAPPeriod='";
        std::ostringstream s;
        s << this->maximumSAPPeriod;
        str += s.str();
        str += "'";
    }
    if (this->startWithSAP > 0) {
        str += " startWithSAP='";
        std::ostringstream s;
        s << this->startWithSAP;
        str += s.str();
        str += "'";
    }
    if (this->maxPlayoutRate > 0) {
        str += " maxPlayoutRate='";
        str += this->maxPlayoutRate;
        str += "'";
    }
    str += " codingDependency='";
    str += (this->codingDependency?"true":"false");
    str += "'";

    return str;
}

std::string
mpd_representation_base_t::toStringContent (void)
{
    std::string str;
    size_t i;

    for (i = 0; i < this->FramePacking.size (); i ++) {
        str += this->FramePacking[i]->toString ();
    }
    for (i = 0; i < this->AudioChannelConfiguration.size (); i ++) {
        str += this->AudioChannelConfiguration[i]->toString ();
    }
    for (i = 0; i < this->ContentProtection.size (); i ++) {
        str += this->ContentProtection[i]->toString ();
    }
    return str;
}

std::string
mpd_representation_base_t::toString (void)
{
    std::string str = std::string("<RepresentationBaseType");
    // attributes
    str += mpd_representation_base_t::toStringAttr();
    str += " >\n";
    str += mpd_representation_base_t::toStringContent();
    str += "</RepresentationBaseType>\n";

    return str;
}

bool
mpd_representation_base_t::verify (void)
{
    bool ret = true;
    if (this->mimeType.size () < 1) {
        ERROR ("verify() error no mimeType\n");
        ret = false;
    }
    if (this->codecs.size () < 1) {
        ERROR ("verify() error no codecs\n");
        ret = false;
    }
    size_t i;
    for (i = 0; i < this->FramePacking.size (); i ++) {
        if (! this->FramePacking[i]->verify ()) {
            ERROR ("verify() error in FramePacking[%" PRIuSZ "]!\n", i);
            ret = false;
        }
    }
    for (i = 0; i < this->AudioChannelConfiguration.size (); i ++) {
        if (! this->AudioChannelConfiguration[i]->verify ()) {
            ERROR ("verify() error in AudioChannelConfiguration[%" PRIuSZ "]!\n", i);
            ret = false;
        }
    }
    for (i = 0; i < this->ContentProtection.size (); i ++) {
        if (! this->ContentProtection[i]->verify ()) {
            ERROR ("verify() error in ContentProtection[%" PRIuSZ "]!\n", i);
            ret = false;
        }
    }
    if (this->startWithSAP > 3) {
        ERROR ("verify() error startWithSAP\n");
        ret = false;
    }
    return ret;
}

std::string
mpd_representation_t::toString (void)
{
    size_t i;
    std::string str = std::string("<Representation");
    // attributes
    str += mpd_representation_base_t::toStringAttr();
    if (this->id.size () > 0) {
        str += " id='" + this->id + "'";
    }
    if (this->bandwidth > 0) {
        str += " bandwidth='";
        std::ostringstream s;
        s << this->bandwidth;
        str += s.str();
        str += "'";
    }
    //if (this->qualityRanking >= 0) {
        str += " qualityRanking='";
        std::ostringstream s;
        s << this->qualityRanking;
        str += s.str();
        str += "'";
    //}
    if (this->dependencyId.size () > 0) {
        str += " dependencyId='";
        for (i = 0; i < this->dependencyId.size (); i ++) {
            str += this->dependencyId[i];
            str += " ";
        }
        str += "'";
    }
    if (this->mediaStreamStructureId.size () > 0) {
        str += " mediaStreamStructureId='";
        for (i = 0; i < this->mediaStreamStructureId.size (); i ++) {
            str += this->mediaStreamStructureId[i];
            str += " ";
        }
        str += "'";
    }
    str += " >\n";
    str += mpd_representation_base_t::toStringContent();
    str += mpd_segmentcommon_t::toStringContent ();
    for (i = 0; i < this->SubRepresentation.size (); i ++) {
        str += this->SubRepresentation[i]->toString ();
    }
    str += "</Representation>\n";

    return str;
}

bool
mpd_representation_t::verify (void)
{
    bool ret = true;
    size_t i;

    if (this->id.size () < 1) {
        ERROR ("verify() error no id\n");
        ret = false;
    }
    if (this->bandwidth < 1) {
        ERROR ("verify() error no bandwidth\n");
        ret = false;
    }
    for (i = 0; i < this->SubRepresentation.size (); i ++) {
        if (! this->SubRepresentation[i]->verify()) {
            ERROR ("verify() error in SubRepresentation[%" PRIuSZ "]!\n", i);
            ret = false;
        }
    }
    if (! mpd_representation_base_t::verify()) {
            ERROR ("verify() error in representation_base\n");
        ret = false;
    }
    if (! mpd_segmentcommon_t::verify()) {
        ERROR ("verify() error in BaseURL/SegmentBase/SegmentTemplate/SegmentList\n");
        ret = false;
    }
    return ret;
}

bool
mpd_segmenturl_t::verify (void)
{
    bool ret = true;
    if ((this->media.size() < 1) && (this->index.size() < 1)) {
        std::cerr << "Error: media or index!" << std::endl;
        ret = false;
    }
    return ret;
}

std::string
mpd_segmenturl_t::toString (void)
{
    std::string str("<SegmentURL");
    if (this->media.size () > 0) {
        str += " media='" + this->media + "'";
    }
    if (this->mediaRange.size () > 0) {
        str += " mediaRange='" + this->mediaRange + "'";
    }
    if (this->index.size () > 0) {
        str += " index='" + this->index + "'";
    }
    if (this->indexRange.size () > 0) {
        str += " indexRange='" + this->indexRange + "'";
    }
    str += " />\n";
    return str;
}

bool
mpd_segmentlist_t::verify (void)
{
    bool ret = true;
    size_t i;
    for (i = 0; i < this->SegmentURL.size (); i ++) {
        if (! this->SegmentURL[i]->verify ()) {
            ERROR ("verify() error in SegmentURL[%" PRIuSZ "]!\n", i);
            ret = false;
        }
    }
    if (! mpd_segmentmultibase_t::verify()) {
        ERROR ("verify() error in segmentmultibase\n");
        ret = false;
    }
    return ret;
}

std::string
mpd_segmentlist_t::toString (void)
{
    size_t i;
    std::string str("<SegmentList");
    str += mpd_segmentmultibase_t::toStringAttr ();
    if (this->xhref.size () > 0) {
        str += " xlink:href='" + this->xhref + "'";
    }
    if (this->xactuate.size () > 0) {
        str += " xlink:actuate='" + this->xactuate + "'";
    }
    str += " >\n";
    str += mpd_segmentmultibase_t::toStringContent ();
    if (this->SegmentURL.size () > 0) {
        for (i = 0; i < this->SegmentURL.size(); i ++) {
            str += this->SegmentURL[i]->toString ();
        }
    }
    str += "</SegmentList>\n";
    return str;
}

std::string
mpd_segmenttimeline_s_t::toString (void)
{
    std::string str("<S");
    if (this->d >= 0) {
        str += " d='";
        std::ostringstream s;
        s << this->d;
        str += s.str();
        str += "'";
    }
    if (this->t >= 0) {
        str += " t='";
        std::ostringstream s;
        s << this->t;
        str += s.str();
        str += "'";
    }
    //if (this->r >= 0) {
        str += " r='";
        std::ostringstream s;
        s << this->r;
        str += s.str();
        str += "'";
    //}
    str += " />\n";
    return str;
}

bool
mpd_segmenttimeline_s_t::verify (void)
{
    if (this->d < 0) {
        ERROR ("verify() error in d\n");
        return false;
    }
    return true;
}

std::string
mpd_segmenttimeline_t::toString (void)
{
    size_t i;
    std::string str("<SegmentTimeline>\n");
    for (i = 0; i < S.size(); i ++) {
        str += S[i]->toString ();
    }
    str += "</SegmentTimeline>\n";
    return str;
}

bool
mpd_segmenttimeline_t::verify (void)
{
    bool ret = true;
    if (S.size() < 1) {
        ERROR ("verify() error no S\n");
        ret = false;
    }
    size_t i;
    for (i = 0; i < this->S.size (); i ++) {
        if (! this->S[i]->verify ()) {
            ERROR ("verify() error in S[%" PRIuSZ "]!\n", i);
            ret = false;
        }
    }
    return ret;
}

/**
 * @brief attribute string of the mpd_segmentbase_t (SegmentBaseType)
 *
 * @return the attribute string, exclude the tag, '<', and '>' etc.
 *
 * convert the attributes of the SegmentBaseType to a XML attribute string,
 * without the tag, '<', and '>'.
 */
std::string
mpd_segmentbase_t::toStringAttr (void)
{
    std::string str;
    // attributes
    //if (this->timescale >= 0) {
        str += " timescale='";
        std::ostringstream s;
        s << this->timescale;
        str += s.str();
        str += "'";
    //}
    //if (this->presentationTimeOffset >= 0) {
        str += " presentationTimeOffset='";
        std::ostringstream s1;
        s1 << this->presentationTimeOffset;
        str += s1.str();
        str += "'";
    //}
    str += " indexRangeExact='"; str += (this->indexRangeExact?"true":"false"); str += "'";
    if (this->indexRange.size () > 0) {
        str += " indexRange='" + this->indexRange + "'";
    }
    return str;
}

std::string
mpd_segmentbase_t::toStringContent (void)
{
    std::string str;
    if (NULL != this->Initialization) {
        this->Initialization->set_title ("Initialization");
        str += this->Initialization->toString();
    }
    if (NULL != this->RepresentationIndex) {
        this->RepresentationIndex->set_title ("RepresentationIndex");
        str += this->RepresentationIndex->toString();
    }
    return str;
}

std::string
mpd_segmentbase_t::toString (void)
{
    std::string str = "<SegmentBase";
    str += this->toStringAttr ();
    str += " >\n";
    str += this->toStringContent ();
    str += "</SegmentBase>\n";
    return str;
}

bool
mpd_segmentbase_t::verify (void)
{
    bool ret = true;
    if (NULL != Initialization) {
        if (! this->Initialization->verify()) {
            ERROR ("verify() error in Initialization\n");
            ret = false;
        }
    }
    if (NULL != RepresentationIndex) {
        if (! this->RepresentationIndex->verify()) {
            ERROR ("verify() error in RepresentationIndex\n");
            ret = false;
        }
    }
    return ret;
}

bool
mpd_segmentmultibase_t::verify ()
{
    bool ret = true;
    if (NULL != this->SegmentTimeline) {
        if (! this->SegmentTimeline->verify ()) {
            ERROR ("verify() error in SegmentTimeline\n");
            ret = false;
        }
    }
    if (NULL != this->BitstreamSwitching) {
        if (! this->BitstreamSwitching->verify ()) {
            ERROR ("verify() error in BitstreamSwitching\n");
            ret = false;
        }
    }
    if (! mpd_segmentbase_t::verify()) {
        ERROR ("verify() error in segmentbase\n");
        ret = false;
    }
    return ret;
}

std::string
mpd_segmentmultibase_t::toStringAttr (void)
{
    std::string str;
    // attributes
    str += mpd_segmentbase_t::toStringAttr();
    if (this->duration >= 0) {
        str += " duration='";
        std::ostringstream s;
        s << this->duration;
        str += s.str();
        str += "'";
    }
    if (this->startNumber >= 0) {
        str += " startNumber='";
        std::ostringstream s;
        s << this->startNumber;
        str += s.str();
        str += "'";
    }
    return str;
}

std::string
mpd_segmentmultibase_t::toStringContent (void)
{
    std::string str;
    str += mpd_segmentbase_t::toStringContent();
    if (NULL != this->SegmentTimeline) {
        str += this->SegmentTimeline->toString();
    }
    if (NULL != this->BitstreamSwitching) {
        this->BitstreamSwitching->set_title ("BitstreamSwitching");
        str += this->BitstreamSwitching->toString();
    }
    return str;
}

std::string
mpd_segmentmultibase_t::toString (void)
{
    std::string str = "<MultipleSegmentBaseType";
    str += this->toStringAttr ();
    str += " >\n";
    str += this->toStringContent ();
    str += "</MultipleSegmentBaseType>\n";
    return str;
}

std::string
mpd_segmenttemplate_t::toString (void)
{
    std::string str = std::string("<SegmentTemplate");
    str += mpd_segmentmultibase_t::toStringAttr();
    // attributes
    if (this->media.size () > 0) {
        str += " media='" + this->media + "'";
    }
    if (this->initialization.size () > 0) {
        str += " initialization='" + this->initialization + "'";
    }
    if (this->index.size () > 0) {
        str += " index='" + this->index + "'";
    }
    if (this->bitstreamSwitching.size () > 0) {
        str += " bitstreamSwitching='" + this->bitstreamSwitching + "'";
    }
    str += " >\n";
    str += mpd_segmentmultibase_t::toStringContent();
    str += "</SegmentTemplate>\n";

    return str;
}

std::string
mpd_subrepres_t::toString (void)
{
    size_t i;
    std::string str = std::string("<SubRepresentation");
    str += mpd_representation_base_t::toStringAttr();
    // attributes
    if (this->level >= 0) {
        str += " level='";
        std::ostringstream s;
        s << this->level;
        str += s.str();
        str += "'";
    }
    if (this->bandwidth >= 0) {
        str += " bandwidth='";
        std::ostringstream s;
        s << this->bandwidth;
        str += s.str();
        str += "'";
    }
    if (this->contentComponent.size () > 0) {
        str += " contentComponent='";
        std::ostringstream s0;
        s0 << this->contentComponent[0];
        str += s0.str();
        for (i = 1; i < this->contentComponent.size(); i ++) {
            str += " ";
            std::ostringstream s;
            s << this->contentComponent[i];
            str += s.str();
        }
        str += "'";
    }
    if (this->dependencyLevel.size () > 0) {
        str += " dependencyLevel='";
        std::ostringstream s0;
        s0 << this->dependencyLevel[0];
        str += s0.str();
        for (i = 1; i < this->dependencyLevel.size(); i ++) {
            str += " ";
            std::ostringstream s;
            s << this->dependencyLevel[i];
            str += s.str();
        }
        str += "'";
    }
    str += " >\n";
    str += mpd_representation_base_t::toStringContent();
    str += "</SubRepresentation>\n";

    return str;
}

bool
mpd_subrepres_t::verify (void)
{
    bool ret = true;
    if (this->level >= 0) {
        if (this->bandwidth < 0) {
            ERROR ("verify() error no bandwidth\n");
            ret = false;
        }
    }
    if (! mpd_representation_base_t::verify()) {
        ERROR ("verify() error in representation base\n");
        ret = false;
    }
    return ret;
}

std::string
mpd_baseurl_t::toString (void)
{
    std::string str = std::string("<BaseURL");
    // attributes
    if (this->serviceLocation.size () > 0) {
        str += " serviceLocation='" + this->serviceLocation + "'";
    }
    if (this->byteRange.size () > 0) {
        str += " byteRange='" + this->byteRange + "'";
    }
    if (this->url.size () > 0) {
        str += " >" + this->url + "</BaseURL>\n";
    } else {
        str += " />\n";
    }

    return str;
}

bool
mpd_contentcomponent_t::verify (void)
{
    bool ret = true;
    size_t i;
    for (i = 0; i < this->Accessibility.size (); i ++) {
        if (! this->Accessibility[i]->verify ()) {
            ERROR ("verify() error in Accessibility[%" PRIuSZ "]\n", i);
            ret = false;
        }
    }
    for (i = 0; i < this->Role.size (); i ++) {
        if (! this->Role[i]->verify ()) {
            ERROR ("verify() error in Role[%" PRIuSZ "]\n", i);
            ret = false;
        }
    }
    for (i = 0; i < this->Rating.size (); i ++) {
        if (! this->Rating[i]->verify ()) {
            ERROR ("verify() error in Rating[%" PRIuSZ "]\n", i);
            ret = false;
        }
    }
    for (i = 0; i < this->Viewpoint.size (); i ++) {
        if (! this->Viewpoint[i]->verify ()) {
            ERROR ("verify() error in Viewpoint[%" PRIuSZ "]\n", i);
            ret = false;
        }
    }
    return ret;
}

std::string
mpd_contentcomponent_t::toString (void)
{
    size_t i;
    std::string str = std::string("<ContentComponent");
    // attributes
    //if (this->id >= 0) {
        str += " id='";
        std::ostringstream s;
        s << this->id;
        str += s.str();
        str += "'";
    //}
    if (this->lang.size () > 0) {
        str += " lang='" + this->lang + "'";
    }
    if (this->contentType.size () > 0) {
        str += " contentType='" + this->contentType + "'";
    }
    if (this->par.size () > 0) {
        str += " par='" + this->par + "'";
    }
    str += " >\n";

    for (i = 0; i < this->Accessibility.size (); i ++) {
        str += this->Accessibility[i]->toString ();
    }
    for (i = 0; i < this->Role.size (); i ++) {
        str += this->Role[i]->toString ();
    }
    for (i = 0; i < this->Rating.size (); i ++) {
        str += this->Rating[i]->toString ();
    }
    for (i = 0; i < this->Viewpoint.size (); i ++) {
        str += this->Viewpoint[i]->toString ();
    }
    str += "</ContentComponent>\n";
    return str;
}

bool
mpd_adaptationset_t::verify (void)
{
    bool ret = true;
    size_t i;
    std::string type = this->get_mimetype();
    if (type.size() < 1) {
        type = this->get_cnttype();
    }
    for (i = 0; i < this->Representation.size (); i ++) {
        if (! this->Representation[i]->verify ()) {
            ERROR ("verify() error in Representation[%" PRIuSZ "]\n", i);
            ret = false;
        }
        if ((this->Representation[i]->get_mimetype().size() > 0) && (this->Representation[i]->get_mimetype() != type)) {
            ERROR ("verify() error in Representation[%" PRIuSZ "], mimeType(%s) mismatch with parent AdaptationSet(%s)\n", i, this->Representation[i]->get_mimetype().c_str(), type.c_str());
            ret = false;
        }
    }
    for (i = 0; i < this->ContentComponent.size (); i ++) {
        if (! this->ContentComponent[i]->verify ()) {
            ERROR ("verify() error in ContentComponent[%" PRIuSZ "]\n", i);
            ret = false;
        }
    }
    for (i = 0; i < this->Accessibility.size (); i ++) {
        if (! this->Accessibility[i]->verify ()) {
            ERROR ("verify() error in Accessibility[%" PRIuSZ "]\n", i);
            ret = false;
        }
    }
    for (i = 0; i < this->Role.size (); i ++) {
        if (! this->Role[i]->verify ()) {
            ERROR ("verify() error in Role[%" PRIuSZ "]\n", i);
            ret = false;
        }
    }
    for (i = 0; i < this->Rating.size (); i ++) {
        if (! this->Rating[i]->verify ()) {
            ERROR ("verify() error in Rating[%" PRIuSZ "]\n", i);
            ret = false;
        }
    }
    for (i = 0; i < this->Viewpoint.size (); i ++) {
        if (! this->Viewpoint[i]->verify ()) {
            ERROR ("verify() error in Viewpoint[%" PRIuSZ "]\n", i);
            ret = false;
        }
    }
    if (! mpd_representation_base_t::verify()) {
        ERROR ("verify() error in representation_base\n");
        ret = false;
    }
    if (! mpd_segmentcommon_t::verify()) {
        ERROR ("verify() error in BaseURL/SegmentBase/SegmentTemplate/SegmentList\n");
        ret = false;
    }

    if (this->subsegmentStartsWithSAP > 3) {
        ERROR ("verify() error in subsegmentStartsWithSAP\n");
        ret = false;
    }
    return ret;
}

std::string
mpd_adaptationset_t::toString (void)
{
    size_t i;
    std::string str = std::string("<AdaptationSet");
    // attributes
    str += mpd_representation_base_t::toStringAttr();
    if (this->id >= 0) {
        str += " id='";
        std::ostringstream s;
        s << this->id;
        str += s.str();
        str += "'";
    }

    str += " subsegmentAlignment='"; str += (this->subsegmentAlignment?"true":"false"); str += "'";

    //if (this->subsegmentStartsWithSAP >= 0) {
        str += " subsegmentStartsWithSAP='";
        std::ostringstream s;
        s << this->subsegmentStartsWithSAP;
        str += s.str();
        str += "'";
    //}
    if (this->lang.size () > 0) {
        str += " lang='" + this->lang + "'";
    }
    str += " bitstreamSwitching='"; str += (this->bitstreamSwitching?"true":"false"); str += "'";

    if (this->xhref.size () > 0) {
        str += " xlink:href='" + this->xhref + "'";
    }
    if (this->xactuate.size () > 0) {
        str += " xlink:actuate='" + this->xactuate + "'";
    }
    if (this->group >= 0) {
        str += " group='";
        std::ostringstream s;
        s << this->group;
        str += s.str();
        str += "'";
    }
    if (this->contentType.size () > 0) {
        str += " contentType='" + this->contentType + "'";
    }
    if (this->par.size () > 0) {
        str += " par='" + this->par + "'";
    }
    if (this->minBandwidth > 0) {
        str += " minBandwidth='";
        std::ostringstream s;
        s << this->minBandwidth;
        str += s.str();
        str += "'";
    }
    if (this->maxBandwidth > 0) {
        str += " maxBandwidth='";
        std::ostringstream s;
        s << this->maxBandwidth;
        str += s.str();
        str += "'";
    }
    if (this->minWidth > 0) {
        str += " minWidth='";
        std::ostringstream s;
        s << this->minWidth;
        str += s.str();
        str += "'";
    }
    if (this->maxWidth > 0) {
        str += " maxWidth='";
        std::ostringstream s;
        s << this->maxWidth;
        str += s.str();
        str += "'";
    }
    if (this->minHeight > 0) {
        str += " minHeight='";
        std::ostringstream s;
        s << this->minHeight;
        str += s.str();
        str += "'";
    }
    if (this->maxHeight > 0) {
        str += " maxHeight='";
        std::ostringstream s;
        s << this->maxHeight;
        str += s.str();
        str += "'";
    }
    if (this->minFrameRate.size () > 0) {
        str += " minFrameRate='" + this->minFrameRate + "'";
    }
    if (this->maxFrameRate.size () > 0) {
        str += " maxFrameRate='" + this->maxFrameRate + "'";
    }
    str += " segmentAlignment='"; str += (this->segmentAlignment?"true":"false"); str += "'";

    str += " >\n";
    str += mpd_representation_base_t::toStringContent();
    str += mpd_segmentcommon_t::toStringContent ();

    for (i = 0; i < this->ContentComponent.size (); i ++) {
        str += this->ContentComponent[i]->toString ();
    }
    for (i = 0; i < this->Accessibility.size (); i ++) {
        str += this->Accessibility[i]->toString ();
    }
    for (i = 0; i < this->Role.size (); i ++) {
        str += this->Role[i]->toString ();
    }
    for (i = 0; i < this->Rating.size (); i ++) {
        str += this->Rating[i]->toString ();
    }
    for (i = 0; i < this->Viewpoint.size (); i ++) {
        str += this->Viewpoint[i]->toString ();
    }
    for (i = 0; i < this->Representation.size (); i ++) {
        str += this->Representation[i]->toString ();
    }
    str += "</AdaptationSet>\n";
    return str;
}

std::string
mpd_subset_t::toString (void)
{
    size_t i;
    std::string str = std::string("<Subset");
    // attributes
    const std::vector<std::string> & s = get_contains ();
    if (s.size () > 0) {

        str += " id='";
        for (i = 0; i < s.size (); i ++) {
            str += s[i];
            str += " ";
        }
        str += "'";
    }
    str += " />\n";

    return str;
}

bool
mpd_subset_t::verify (void)
{
    if (this->contains.size() < 1) {
        return false;
    }
    return true;
}

std::string
mpd_period_t::toString (void)
{
    size_t i;
    std::string str = std::string("<Period");
    // attributes
    if (this->id.size () > 0) {
        str += " id='" + this->id + "'";
    }
    if (this->start.size () > 0) {
        str += " start='" + this->start + "'";
        std::cerr << "------------- this->start=" << this->start << std::endl;
        std::cerr << "------------- str=" << str << std::endl;
    }
    if (this->duration.size () > 0) {
        str += " duration='" + this->duration + "'";
    }

    str += " bitstreamSwitching='"; str += (this->bitstreamSwitching?"true":"false"); str += "'";

    if (this->xhref.size () > 0) {
        str += " xlink:href='" + this->xhref + "'";
    }
    if (this->xactuate.size () > 0) {
        str += " xlink:actuate='" + this->xactuate + "'";
    }
    str += " >\n";
    str += mpd_segmentcommon_t::toStringContent ();

    for (i = 0; i < this->adaptationSets.size (); i ++) {
        str += this->adaptationSets[i]->toString ();
    }
    for (i = 0; i < this->Subset.size (); i ++) {
        str += this->Subset[i]->toString ();
    }
    str += "</Period>\n";
    return str;
}

bool
mpd_period_t::verify (void)
{
    size_t i;
    bool ret = true;
    if (xhref.size () > 0) {
        if ((std::string::npos == xactuate.find("onRequest")) && (std::string::npos == xactuate.find("onLoad"))) {
            ret = false;
        }
    }
    for (i = 0; i < this->adaptationSets.size (); i ++) {
        if (! this->adaptationSets[i]->verify ()) {
            ERROR ("verify() error in adaptationSets[%" PRIuSZ "]\n", i);
            ret = false;
        }
    }
    for (i = 0; i < this->Subset.size (); i ++) {
        if (! this->Subset[i]->verify ()) {
            ERROR ("verify() error in Subset[%" PRIuSZ "]\n", i);
            ret = false;
        }
    }
    if (! mpd_segmentcommon_t::verify()) {
        ERROR ("verify() error in BaseURL/SegmentBase/SegmentTemplate/SegmentList\n");
        ret = false;
    }
    return ret;
}

std::string
mpd_proginfo_t::toString (void)
{
    std::string str = std::string("<ProgramInformation");
    if (this->lang.size () > 0) {
        str += " lang='" + this->lang + "'\n";
    }
    if (this->moreInformationURL.size () > 0) {
        str += " moreInformationURL='" + this->moreInformationURL + "'";
    }
    str += " >\n";
    // sequence
    if (this->Title.size () > 0) {
        str += "<Title>" + this->Title + "</Title>\n";
    }
    if (this->Source.size () > 0) {
        str += "<Source>" + this->Source + "</Source>\n";
    }
    if (this->Copyright.size () > 0) {
        str += "<Copyright>" + this->Copyright + "</Copyright>\n";
    }
    str += "</ProgramInformation>\n";
    return str;
}

std::string
mpd_range_t::toString (void)
{
    std::string str = std::string("<Range");
    if (this->starttime.size () > 0) {
        str += " starttime='" + this->starttime + "'\n";
    }
    if (this->duration.size () > 0) {
        str += " duration='" + this->duration + "'\n";
    }
    str += " />\n";
    return str;
}

std::string
mpd_metrics_t::toString (void)
{
    std::string str = std::string("<Metrics");
    if (this->metrics.size () > 0) {
        str += " metrics='" + this->metrics + "'\n";
    }
    str += " >\n";

    // sequences
    size_t i;
    for (i = 0; i < this->Reporting.size (); i ++) {
        str += this->Reporting[i]->toString ();
    }
    for (i = 0; i < this->Range.size (); i ++) {
        str += this->Range[i]->toString ();
    }
    str += "</Metrics>\n";
    return str;
}

bool
mpd_metrics_t::verify (void)
{
    bool ret = true;
    if (this->metrics.size () < 1) {
        ret = false;
    }
    if (this->Reporting.size() < 1) {
        ret = false;
    }
    size_t i;
    for (i = 0; i < this->Reporting.size (); i ++) {
        if (false == this->Reporting[i]->verify ()) {
            ERROR ("verify() error in Reporting[%" PRIuSZ "]\n", i);
            ret = false;
        }
    }
    for (i = 0; i < this->Range.size (); i ++) {
        if (false == this->Range[i]->verify ()) {
            ERROR ("verify() error in Range[%" PRIuSZ "]\n", i);
            ret = false;
        }
    }
    return ret;
}

std::string
mpd_t::toString (void)
{
    size_t i;
    std::string str = std::string("<?xml version='1.0' encoding='utf-8' ?>\n<MPD\n");
    // attributes
    if (this->xmlns_xsi.size () > 0) {
        str += " xmlns:xsi='" + this->xmlns_xsi + "'\n";
    }
    if (this->xmlns.size () > 0) {
        str += " xmlns='" + this->xmlns + "'\n";
    }
    if (this->xsi_schemaLocation.size () > 0) {
        str += " xsi:schemaLocation='" + this->xsi_schemaLocation + "'\n";
    }

    if (this->id.size () > 0) {
        str += " id='" + this->id + "'";
    }
    str += " type='"; str += presentation_type_to_string (this->type); str += "'";
    if (this->profiles.size () > 0) {
        str += " profiles='";
        str += this->profiles[0];
        for (i = 1; i < this->profiles.size(); i ++) {
            str += ",";
            str += this->profiles[i];
        }
        str += "'";
    }

    str += " minBufferTime='" + this->minBufferTime + "'";

    if (this->availabilityStartTime.size () > 0) {
        str += " availabilityStartTime='" + this->availabilityStartTime + "'";
    }
    if (this->availabilityEndTime.size () > 0) {
        str += " availabilityEndTime='" + this->availabilityEndTime + "'";
    }
    if (this->mediaPresentationDuration.size () > 0) {
        str += " mediaPresentationDuration='" + this->mediaPresentationDuration + "'";
    }
    if (this->minimumUpdatePeriod.size () > 0) {
        str += " minimumUpdatePeriod='" + this->minimumUpdatePeriod + "'";
    }
    if (this->timeShiftBufferDepth.size () > 0) {
        str += " timeShiftBufferDepth='" + this->timeShiftBufferDepth + "'";
    }
    if (this->suggestedPresentationDelay.size () > 0) {
        str += " suggestedPresentationDelay='" + this->suggestedPresentationDelay + "'";
    }
    if (this->maxSegmentDuration.size () > 0) {
        str += " maxSegmentDuration='" + this->maxSegmentDuration + "'";
    }
    if (this->maxSubsegmentDuration.size () > 0) {
        str += " maxSubsegmentDuration='" + this->maxSubsegmentDuration + "'";
    }
    str += " >\n";

    // sequences
    for (i = 0; i < this->BaseURL.size (); i ++) {
        str += this->BaseURL[i]->toString ();
    }
    for (i = 0; i < this->ProgramInformation.size (); i ++) {
        str += this->ProgramInformation[i]->toString ();
    }
    for (i = 0; i < this->Location.size (); i ++) {
        str += "<Location>";
        str += this->Location[i];
        str += "</Location>\n";
    }
    for (i = 0; i < this->Metrics.size (); i ++) {
        str += this->Metrics[i]->toString ();
    }
    for (i = 0; i < this->Periods.size (); i ++) {
        str += this->Periods[i]->toString ();
    }
    str += "</MPD>\n";
    return str;
}

bool
mpd_t::verify (void)
{
    bool ret = true;
    if (PDYNAMIC == this->type) {
        if (this->availabilityStartTime.size () < 1) {
            ERROR ("verify() error type dynamic have no correspond availabilityStartTime!\n");
            ret = false;
        }
    } else {
        assert (PSTATIC == this->type);
        if (this->mediaPresentationDuration.size() < 1) {
            ERROR ("verify() error type static have no correspond mediaPresentationDuration!\n");
            ret = false;
        }
    }
    if (this->minBufferTime.size () < 1) {
        ERROR ("verify() error no minBufferTime!\n");
        ret = false;
    }
    if (this->Periods.size () < 1) {
        ERROR ("verify() error no Periods!\n");
        ret = false;
    }
    if (this->profiles.size() < 1) {
        ERROR ("verify() error no profiles!\n");
        ret = false;

    } else {
        size_t i;
        for (i = 0; i < this->profiles.size (); i ++) {
            bool valid = false;
            // six profiles
            if (0 == this->profiles[i].compare("urn:mpeg:dash:profile:full:2011")) {
                // 8.2 Full profile
                valid = true;
            } else if (0 == this->profiles[i].compare("urn:mpeg:dash:profile:isoff-on-demand:2011")) {
                // 8.3 ISO Base media file format On Demand profile
                valid = true;
            } else if (0 == this->profiles[i].compare("urn:mpeg:dash:profile:isoff-live:2011")) {
                // 8.4 ISO Base media file format live profile
                valid = true;
            } else if (0 == this->profiles[i].compare("urn:mpeg:dash:profile:isoff-main:2011")) {
                // 8.5 ISO Base media file format main profile
                valid = true;
            } else if (0 == this->profiles[i].compare("urn:mpeg:dash:profile:mp2t-main:2011")) {
                // 8.6 MPEG-2 TS main profile
                valid = true;
            } else if (0 == this->profiles[i].compare("urn:mpeg:dash:profile:mp2t-simple:2011")) {
                // 8.7 MPEG-2 TS simple profile
                valid = true;
            } else if (0 == this->profiles[i].compare("urn:webm:dash:profile:webm-on-demand:2012")) {
                // WebM-DASH
                valid = true;
            }
            if (! valid) {
                ERROR ("verify() error in profiles('%s')\n", this->profiles[i].c_str());
                ret = false;
            }
        }
    }
    size_t i;
    for (i = 0; i < this->Periods.size (); i ++) {
        if (false == this->Periods[i]->verify ()) {
            ERROR ("verify() error in Periods[%" PRIuSZ "]!\n", i);
            ret = false;
        }
    }
    for (i = 0; i < this->ProgramInformation.size (); i ++) {
        if (false == this->ProgramInformation[i]->verify ()) {
            ERROR ("verify() error in ProgramInformation[%" PRIuSZ "]!\n", i);
            ret = false;
        }
    }
    for (i = 0; i < this->BaseURL.size (); i ++) {
        if (false == this->BaseURL[i]->verify ()) {
            ERROR ("verify() error in BaseURL[%" PRIuSZ "]!\n", i);
            ret = false;
        }
    }
    for (i = 0; i < this->Location.size (); i ++) {
        if (this->Location[i].size() < 1) {
            ERROR ("verify() error in Location[%" PRIuSZ "]!\n", i);
            ret = false;
        }
    }
    for (i = 0; i < this->Metrics.size (); i ++) {
        if (false == this->Metrics[i]->verify ()) {
            ERROR ("verify() error in Metrics[%" PRIuSZ "]!\n", i);
            ret = false;
        }
    }
   return ret;
}

