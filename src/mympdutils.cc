/**
 * @file    mympdutils.cc
 * @brief   functions for MPD
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-04-16
 */

#include <sstream>
#include <iostream>
#include <cstdarg>

#include <stdint.h>
#include <stdlib.h>
#include <sys/time.h>
#include <assert.h>

#include "mympdutils.h"

#if USE_CPP11X
#include <ratio>
#include <chrono>
#endif

#if 0
    struct timezone tz;
    struct timeval tv;
    struct tm tstruct;
    if (0 == gettimeofday (&tv, &tz)) {

    } else {
        time_t now = time(0);
        struct tm * tmp = localtime (&now);
        memset (&tstruct, 0, sizeof (tstruct));
        if (tmp) {
            tstruct = * tmp;
        }
        tstruct.tm_hour = hour;
        tstruct.tm_min = minute;
        tstruct.tm_sec = second;
    }
#endif // 0

/**
 * @brief convert string xs:dateTime to time_t
 *
 * @param str : the xs:dateTime string
 *
 * @return the dateTime
 *
 * convert string xs:dateTime to time_t, YYYY-MM-DDThh:mm:ss
 * 1. xs:dateTime examples:
 *  2002-05-30T09:00:00
 *  2002-05-30T09:30:10.5
 *  2002-05-30T09:30:10Z      (Z is UTC time)
 *  2002-05-30T09:30:10-06:00
 *  2002-05-30T09:30:10+06:00
 * 2. xs:time examples:
 *  09:00:00
 *  09:30:10.5
 *  09:30:10Z
 *  09:30:10-06:00
 *  09:30:10+06:00
 * 3. xs:date examples:
 *  2002-09-24
 *  2002-09-24Z  (Z is UTC time)
 *  2002-09-24-06:00
 *  2002-09-24+06:00
 */
time_t
xsdatetime_to_timet (const std::string & str)
{
    std::string::size_type pos_start = 0;
    std::string::size_type pos_end;
    std::string::size_type pos_max;
    bool flg_date = true;
    bool flg_time = true;
    //bool flg_negzone = true;

    // find first '-'
    pos_end = str.find_first_of ('-', pos_start);
    if (std::string::npos == pos_end) {
        // there's no year-month-day and no neg-zone
        flg_date = false;
        //flg_negzone = false;
    } else {
        // find first ':'
        pos_start = str.find_first_of (':', pos_start);
        if (std::string::npos == pos_start) {
            // there's no time
            flg_time = false;
        } else {
            if (pos_start < pos_end) {
                flg_date = false;
            }
            pos_end = str.find_first_of ('+', 0);
            if (std::string::npos != pos_end) {
                if (pos_start > pos_end) {
                    flg_time = false;
                }
            }
        }
    }
#define PARSE_VALUE(ret_val, str1, ch_end, pos_start1, pos_end1, pos_max1) \
    (pos_end1) = (str1).find_first_of ((ch_end), (pos_start1)); \
    if ((std::string::npos == (pos_end1)) | ((pos_end1) > (pos_max1))) { \
        (pos_end1) = (pos_max1); \
    } \
    /*(ret_val) = strtod ( (str1).substr( (pos_start1), (pos_end1) - (pos_start1) ).c_str(), 0, 10 base );*/ \
    (ret_val) = strtod ( (str1).substr( (pos_start1), (pos_end1) - (pos_start1) ).c_str(), NULL ); \
    /*std::cerr << "" # ret_val " substr='" << (str1).substr( (pos_start1) , (pos_end1) - (pos_start1) ) \
        << "', value=" << (ret_val) \
        << ", start=" << (pos_start1) \
        << ", end=" << (pos_end1) \
        << ", max=" << (pos_max1) \
        << std::endl;*/ \
    (pos_start1) = (pos_end1) + 1;

    double year = 0;
    double month = 0;
    double day = 0;
    double hour = 0;
    double minute = 0;
    double second = 0;
    bool flg_isneg = false;
    double hourz = 0;
    double minz = 0;

    pos_max = str.length();
    pos_start = 0;
    if (flg_date) {
        pos_max = str.find_first_of ('T', pos_start);
        if (std::string::npos == pos_max) {
            pos_max = str.length();
        }
        // parse date
        PARSE_VALUE (year,  str, '-',  pos_start, pos_end, pos_max);
        PARSE_VALUE (month, str, '-',  pos_start, pos_end, pos_max);
        PARSE_VALUE (day,   str, "TZ+-", pos_start, pos_end, pos_max);
    }
    if (flg_time) {
        if ((pos_start > 0) && ('T' != str.at(pos_start - 1))) {
            flg_time = false;
        }
    }
    if (flg_time) {
        pos_max = str.find_first_of ('Z', pos_start);
        if (std::string::npos == pos_max) {
            pos_max = str.length();
        }
        PARSE_VALUE (hour,   str, ':',   pos_start, pos_end, pos_max);
        PARSE_VALUE (minute, str, ':',   pos_start, pos_end, pos_max);
        PARSE_VALUE (second, str, "Z+-", pos_start, pos_end, pos_max);
    }
    pos_max = str.length();

    // set time zone to default (local)
    struct timezone tz;
    if (0 == gettimeofday (NULL, &tz)) {
        minz = - (tz.tz_minuteswest % 60);
        hourz = - (tz.tz_minuteswest / 60); // local value?
    }

    //std::cerr << "start=" << pos_start << ", pos_max=" << pos_max << std::endl;
    if (pos_start < pos_max) {
        if (pos_start > 0) {
            if ('-' == str.at(pos_start - 1)) {
                flg_isneg = true;
            }
        }
        if ('Z' == str.at(pos_start - 1)) {
            hourz = minz = 0.0;
        } else {
            //if (flg_isneg) { std::cerr << "sign neg ("<< str.at(pos_start - 1)  << ")" << std::endl; }
            //std::cerr << "sign ("<< str.at(pos_start - 1) << ")" << std::endl;
            pos_max = str.length();
            PARSE_VALUE (hourz, str, ':',   pos_start, pos_end, pos_max);
            PARSE_VALUE (minz,  str, ':',   pos_start, pos_end, pos_max);
            if (flg_isneg) {
                hourz = - hourz;
                minz = - minz;
            }
        }
    }
    //std::cerr << "zone_hour=" << hourz << ", zone_min=" << minz << std::endl;
    return ((hour * 60) + minute); // TODO: convert year-month-day hour-minute-second zone to time_t
}
#undef PARSE_VALUE

/**
 * @brief convert second to string xs:duration
 *
 * @param str : the xs:duration string
 * @param val : the seconds
 *
 * @return N/A
 *
 * convert second to string xs:duration, PnYnMnDTnHnMnS
 * Examples:
 *  P5Y  (a period of five years.)
 *  P5Y2M10D
 *  P5Y2M10DT15H
 *  PT15H
 *  -P10D (a negative duration)
 */
void
xsduration_from_seconds (std::string & str, double val)
{
    size_t minutes = 0;
    size_t hours = 0;

    if (val < 0) {
        val = -val;
        str += '-';
    }
    if (val > 60 * 60) {
        hours = val / 3600;
        val = val - hours * 3600;
    }
    if (val > 60) {
        minutes = val / 60;
        val = val - minutes * 60;
    }

    std::ostringstream s;
    s << "PT";
    if (hours > 0) {
        s << hours << 'H';
    }
    if (minutes > 0) {
        s << minutes << 'M';
    }
    s << val << 'S';
    str += s.str();
}

/**
 * @brief convert string xs:duration to time_t
 *
 * @param str : the xs:duration string
 *
 * @return the duration
 *
 * convert string xs:duration to time_t, PnYnMnDTnHnMnS
 * Examples:
 *  P5Y  (a period of five years.)
 *  P5Y2M10D
 *  P5Y2M10DT15H
 *  PT15H
 *  -P10D (a negative duration)
 */
double
xsduration_to_seconds (const std::string & str)
{
#if USE_CPP11X
#else
    std::string::size_type pos_start = 0;
    std::string::size_type pos_end;
    std::string::size_type pos_max;
    pos_start = str.find_first_of ("-P", pos_start);
    if (std::string::npos == pos_start) {
        return -1;
    }
    bool has_minus = false;
    if ('-' == str.at(pos_start)) {
        has_minus = true;
        pos_start = str.find_first_of ('P', pos_start);
        if (std::string::npos == pos_start) {
            return -1;
        }
    }

    double year = 0;
    double month = 0;
    double day = 0;
    double hour = 0;
    double minute = 0;
    double second = 0;

#define PARSE_VALUE(ret_val, str1, ch_end, pos_start1, pos_end1, pos_max1) \
    (pos_end1) = (str1).find_first_of ((ch_end), (pos_start1)); \
    if ((std::string::npos != (pos_end1)) && ((pos_end1) < (pos_max1))) { \
        /*(ret_val) = strtod ( (str1).substr( (pos_start1) + 1, (pos_end1) - (pos_start1) - 1 ).c_str(), 0, 10 base );*/ \
        (ret_val) = strtod ( (str1).substr( (pos_start1) + 1, (pos_end1) - (pos_start1) - 1 ).c_str(), NULL ); \
        /*std::cerr << "" # ret_val " substr='" << (str1).substr( (pos_start1) + 1, (pos_end1) - (pos_start1) - 1 ) << "', value=" << (ret_val) << std::endl;*/ \
        (pos_start1) = (pos_end1); \
    }
    pos_max = str.find_first_of ('T', pos_start);
    if (std::string::npos == pos_max) {
        pos_max = str.length();
    }
    PARSE_VALUE (year,  str, 'Y', pos_start, pos_end, pos_max);
    PARSE_VALUE (month, str, 'M', pos_start, pos_end, pos_max);
    PARSE_VALUE (day,   str, 'D', pos_start, pos_end, pos_max);

    pos_start = str.find_first_of ('T', pos_start);
    if (std::string::npos != pos_start) {
        pos_max = str.length();
        PARSE_VALUE (hour,   str, 'H', pos_start, pos_end, pos_max);
        PARSE_VALUE (minute, str, 'M', pos_start, pos_end, pos_max);
        PARSE_VALUE (second, str, 'S', pos_start, pos_end, pos_max);
    }
    second += (minute * 60);
    second += (hour * 60 * 60);
    second += (day * 60 * 60 * 24);
    second += (month * 60 * 60 * 24 * 30); // ?
    second += (year * 60 * 60 * 24 * 365); // ?

    //std::cerr << "sec = " << second << std::endl;
    if (has_minus) {
        return - second;
    }
    return second;
#undef PARSE_VALUE

#endif // USE_CPP11X
}

void
trim (std::string & str)
{
    std::string::size_type pos = str.find_last_not_of(" \t\n\r");
    if(pos != std::string::npos) {
        str.erase(pos + 1);
        pos = str.find_first_not_of(" \t\n\r");
        if(pos != std::string::npos) str.erase(0, pos);
    } else {
        str.erase(str.begin(), str.end());
    }
}

static void spfa (std::string &s, const std::string fmt, ...)
{
    size_t sz_orig;
    int n, size=100;
    bool b=false;
    va_list marker;
    sz_orig = s.size();

    while (!b)
    {
        s.resize(sz_orig + size);
        va_start(marker, fmt);
        n = vsnprintf((char*)(s.c_str() + sz_orig), size, fmt.c_str(), marker);
        va_end(marker);
        if ((n>0) && ((b=(n<size))==true)) s.resize(sz_orig + n); else size*=2;
    }
}

// page 48
/**
 * @brief format the SegmentTemplate string
 *
 * @param ret : the processed string
 * @param format : the format string
 * @param rid : RepresentationID, the value of the attribute Representation.id
 * @param number : Number, the number of the corresponding Segment.
 * @param bandwidth : Bandwidth, the value of Representation.bandwidth
 * @param time : Time, the value of the SegmentTimeline.t
 *
 * @return true on success, false on fail
 *
 * format the SegmentTemplate string, $RepresentationID$,
 * Examples:
 *  $RepresentationID$/seg_$Bandwidth%020$_$Number%05$.mp4
 *  $$$RepresentationID$/seg_$Bandwidth%020$_$Time%05$.mp4
 */
bool
segtempl_urlformat (std::string &ret, const std::string &format, const std::string & rid, size_t number, size_t bandwidth, size_t time)
{
    std::string::size_type pos_pre = 0;
    std::string::size_type pos;
    std::string::size_type pos_end;

    while ((pos = format.find_first_of ('$', pos_pre)) != std::string::npos) {
        pos_end = format.find_first_of ('$', pos + 1);
        if (std::string::npos == pos_end) {
            break;
        }
        if (pos > pos_pre) {
            ret.append (format.substr(pos_pre, pos - pos_pre));
        }
        if (pos + 1 >= pos_end) {
            // case $$
            ret.append ("$");
        } else {
            std::string substr = format.substr(pos + 1, pos_end - pos - 1);
            std::string fmt("%" PRIuSZ);
            std::string::size_type pos_fmt;

            if (0 == substr.find ("RepresentationID")) {
                ret += rid;

            } else if (0 == substr.find ("Number")) {
                pos_fmt = substr.find ("%0");
                if (std::string::npos != pos_fmt) {
                    fmt = substr.substr(pos_fmt);
                    if (isdigit(fmt.at(fmt.size() - 1))) {
                        fmt += PRIuSZ;
                    }
                }
                spfa (ret, fmt, number);

            } else if (0 == substr.find ("Bandwidth")) {
                pos_fmt = substr.find ("%0");
                if (std::string::npos != pos_fmt) {
                    fmt = substr.substr(pos_fmt);
                    if (isdigit(fmt.at(fmt.size() - 1))) {
                        fmt += PRIuSZ;
                    }
                }
                spfa (ret, fmt, bandwidth);

            } else if (0 == substr.find ("Time")) {
                pos_fmt = substr.find ("%0");
                if (std::string::npos != pos_fmt) {
                    fmt = substr.substr(pos_fmt);
                    if (isdigit(fmt.at(fmt.size() - 1))) {
                        fmt += PRIuSZ;
                    }
                }
                spfa (ret, fmt, time);
            }
        }
        pos_pre = pos_end + 1;
    }
    if (pos_pre < format.size()) {
        ret += format.substr (pos_pre);
    }
    return true;
}

bool
segtempl_urlformat (std::string &ret, const char * format1, const char * rid1, size_t number, size_t bandwidth, size_t time)
{
    std::string format(format1);
    std::string rid(rid1);
    return segtempl_urlformat (ret, format, rid, number, bandwidth, time);
}

#define DBGMSG(...)
/**
 * @brief Using binary search to find the position by data_pin
 *
 * @param userdata : User's data
 * @param num_data : the item number of the sorted data
 * @param cb_comp : the callback function to compare the user's data and pin
 * @param data_pin : The reference data to be found
 * @param ret_idx : the position of the required data; If failed, then it is the failed position, which is the insert position if possible.
 *
 * @return 0 on found, <0 on failed(fail position is saved in ret_idx)
 *
 * Using binary search to find the position by data_pin. The user's data should be sorted.
 */
int
pf_bsearch_r (void *userdata, size_t num_data, pf_bsearch_cb_comp_t cb_comp, void *data_pin, size_t *ret_idx)
{
    int retcomp;
    uint8_t flg_found;
    size_t ileft;
    size_t iright;
    size_t i;

    assert (NULL != ret_idx);
    /* 查找合适的位置 */
    if (num_data < 1) {
        *ret_idx = 0;
        DBGMSG (PFDBG_CATLOG_PF, PFDBG_LEVEL_ERROR, "num_data(%d) < 1", num_data);
        return -1;
    }

    /* 折半查找 */
    /* 为了不出现负数，以免缩小索引的所表示的数据范围
     * (负数表明减少一位二进制位的使用)，
     * 内部 ileft 和 iright使用从1开始的下标，
     *   即1表示C语言中的0, 2表示语言中的1，以此类推。
     * 对外还是使用以 0 为开始的下标
     */
    i = 0;
    ileft = 1;
    iright = num_data;
    flg_found = 0;
    for (; ileft <= iright;) {
        i = (ileft + iright) / 2 - 1;
        /* cb_comp should return the *userdata[i] - *data_pin */
        retcomp = cb_comp (userdata, i, data_pin);
        if (retcomp > 0) {
            iright = i;
        } else if (retcomp < 0) {
            ileft = i + 2;
        } else {
            /* found ! */
            flg_found = 1;
            break;
        }
    }

    if (flg_found) {
        *ret_idx = i;
        return 0;
    }
    if (iright <= i) {
        *ret_idx = i;
    } else if (ileft >= i + 2) {
        *ret_idx = i + 1;
    }
    DBGMSG (PFDBG_CATLOG_PF, PFDBG_LEVEL_DEBUG, "not found! num_data=%d; ileft=%d, iright=%d, i=%d", num_data, ileft, iright, i);
    return -1;
}


