/**
 * @file    myxmlparser.cc
 * @brief   General xml parser interface
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-04-16
 */

#include "myxmlparser.h"

static bool xml_parser_skip_current_node (xml_reader_base_t * xmlrd, int rank = 1);

/**
 * @brief skip the current node and its sub-nodes
 *
 * @param xmlrd : the XML reader pointer
 * @param rank : the number of cascaded layer
 *
 * @return false on error
 *
 * skip the current node and its sub-nodes.
 * Before call this function, the caller should call xmlrd->NextNode() first and get return value STARTELEM.
 *   For example, for following XML file,
 *   <A> <B> <C> <D></D> </C> <E></E><F></F> </B> <G></G> </A>
 *   if current node name is "C", then
 *   (1) xml_parser_skip_current_node (xmlrd, 1) will jump to the begin of "E"
 *   (2) xml_parser_skip_current_node (xmlrd, 2) will jump to the begin of "G"
 *   (3) xml_parser_skip_current_node (xmlrd, 3) will jump to the end of XML
 */
static bool
xml_parser_skip_current_node (xml_reader_base_t * xmlrd, int rank)
{
    assert (NULL != xmlrd);

    bool isEmpty = xmlrd->IsEmptyElement();
    if (isEmpty) {
        return true;
    }

    xml_reader_base_t::type_t type;
    const char *data = NULL;
    bool isNext = true;
    while (isNext) {
        type = xmlrd->NextNode (&data);

        switch (type) {
        case xml_reader_base_t::ENDELEM:
            std::cerr << "WARNING: skip END of " << data << std::endl;
            rank --;
            if (rank < 1) {
                isNext = false;
            }
            break;
        case xml_reader_base_t::NONE:
        case xml_reader_base_t::ERROR:
            isNext = false;
            break;

        case xml_reader_base_t::TEXT:
            isNext = true;
            break;

        default:
            isNext = true;
            rank ++;
            std::cerr << "WARNING: skip BEGIN of " << data << std::endl;
            break;
        }
    }
    if (xml_reader_base_t::ERROR == type) {
        return false;
    }
    return true;
}

bool
xml_parser_base_t::parse_current (xml_reader_base_t * xmlrd)
{
    if (NULL == xmlrd) {
        return false;
    }
    assert (NULL != xmlrd);

    bool isEmpty = xmlrd->IsEmptyElement();

    const char *attrValue;
    const char *attrName;

    while((attrName = xmlrd->NextAttr(&attrValue)) != NULL) {
        std::cerr << "Attr: " << attrName << "='" << attrValue << "'" << std::endl;
        if (! this->on_attribute (attrName, attrValue)) {
            std::cerr << "Unknown attribute: " << attrName << "='" << attrValue << "'" << std::endl;
        }
    }
    std::cerr << "empty? " << (isEmpty?"yes":"no") << std::endl;
    if (isEmpty) {
        return true;
    }

    bool isNext = true;
    while (isNext) {
        const char * data = NULL;
        int type = xmlrd->NextNode (&data);

        switch (type) {
        case xml_reader_base_t::ENDELEM:
            std::cerr << "Type: ENDELEM(" << type << "); Text: " << data << std::endl;
        case xml_reader_base_t::NONE:
        case xml_reader_base_t::ERROR:
            isNext = false;
            break;

        case xml_reader_base_t::TEXT:
            std::cerr << "Type: TEXT(" << type << "); Text: " << data << std::endl;
            this->on_text (data);
            isNext = true;
            break;

        default:
            isNext = true;
            assert (xml_reader_base_t::STARTELEM == type);
            std::cerr << "type: STARTELEM(" << type << "), node '" << data << "'" << std::endl;
            if (! this->parse_content (xmlrd, data)) {
                std::cerr << "IGNORE type: (" << type << "), node '" << data << "'" << std::endl;
                xml_parser_skip_current_node (xmlrd);
            }
            break;
        }
    }

    return true;
}
