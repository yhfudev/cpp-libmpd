/**
 * @file    mychunk.cc
 * @brief   DASH chunk
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-10-16
 */
#include "mychunk.h"

void
dump_mpdchunk_info (mpd_chunk_t *chk)
{
    if (NULL == chk) {
        std::cout << "mpdchunk NULL!" << std::endl;
        return;
    }
    assert (NULL != chk);
    std::cout << "mpdchunk: " << chk->get_url();
    std::cout << "(" << chk->get_bitrate() << ")";
    std::vector<std::string> & vstr = chk->get_opt_url ();
    if (vstr.size() > 0) {
        std::cout << "{";
        for (size_t i = 0; i < vstr.size(); i ++) {
            std::cout << vstr[i] << ",";
        }
        std::cout << "}";
    }
    if (chk->has_byterange()) {
        std::cout << chk->get_byte_start() << "-" << chk->get_byte_end();
    }
    std::cout << std::endl;
}

/**
 * @brief add the baseurls as the prefix of all chunk's url/opt_urls
 *
 * @param chunk :
 * @param u : the list of the url
 *
 * @return N/A
 *
 * add the baseurls as the prefix of all chunk's url/opt_urls
 */
static void
add_prefixs_to_chunk (mpd_chunk_t *chunk, std::vector<mpd_baseurl_t *> * u)
{
    size_t i;
    size_t j;

    assert (NULL != chunk);
    assert (NULL != u);
    assert (u->size() > 0);

    std::string file = u->at(0)->get_url() + chunk->get_url();
    std::vector<std::string> & chlst = chunk->get_opt_url ();
    size_t num = chlst.size();
    for (i = 1; i < u->size(); i ++) {
        chunk->add_opt_url(u->at(i)->get_url() + chunk->get_url());
    }
    // baseurs * opt_url except the original strings
    for (i = 0; i < u->size(); i ++) {
        //size_t len = u->at(i)->get_url().size();
        for (j = 0; j < num; j ++) {
            chunk->add_opt_url(u->at(i)->get_url() + chlst[j]);
        }
    }
    // remove original strings
    chlst = chunk->get_opt_url ();
    chlst.erase (chlst.begin(), chlst.begin() + num);
    if (file.size() > 0) {
        chunk->set_url(file);
    }
    // assert (chlst.size() == num * (u->size() + 1 ) - 1);
}

static void
chk_prefix (std::string &url, const std::string &prefix)
{
    if (std::string::npos == url.find ("://")) {
        for (; '/' == url[0];) {
            url.erase (0, 1);
        }
        url.insert (0, prefix );
    }
}

/**
 * @brief last chance to check if the URLs are valid
 *
 * @param chunk :
 * @param prefix : The default prefix for URLS, such "http://a.com/dash/"
 *
 * @return N/A
 *
 * if the prefix is not null, then check each of the URLs in the list, make sure it contains 'http://' or 'ftp://' etc.
 *   if not, then add the default prefix to the URL.
 */
static void
chk_prefixs_to_chunk (mpd_chunk_t *chunk, const std::string &prefix_default)
{
    assert (NULL != chunk);

    chk_prefix (chunk->get_url(), prefix_default);
    std::vector<std::string> & chlst = chunk->get_opt_url();
    std::vector<std::string>::iterator it = chlst.begin();
    for (; it != chlst.end(); it ++) {
        chk_prefix (*it, prefix_default);
    }
}

/**
 * @brief get file segment info from segcomm's SegmentTemplate/SegmentBase/SegmentList/BaseURL
 *
 * @param chunk :
 * @param segcomm : the node, one of Representation/AdaptationSet/Period
 * @param rep : the Representation
 * @param index : the number of the segment, 0 is the init segment, >= 1 segment
 *
 * @return true on success
 *
 * get file segment info from segcomm's SegmentTemplate/SegmentBase/SegmentList/BaseURL
 */
static bool
get_segment_from_segmentcommon (mpd_chunk_t *chunk, mpd_segmentcommon_t * segcomm, mpd_representation_t *rep, size_t index)
{
    std::string file;
    bool ret = false;
    //size_t i;

    assert (NULL != chunk);
    assert (NULL != rep);

    chunk->set_bitrate(rep->get_bandwidth());
    // SegmentTemplate@
    if ((! ret) && (NULL != segcomm->get_segtemplate())) {
        mpd_segmenttemplate_t *segtemp = segcomm->get_segtemplate();
        assert (NULL != segtemp);

        std::string initv = segtemp->get_media();
        ssize_t snum = segtemp->get_snum();
        if (snum < 0) {
            snum = 1; // default value
        }
        if (0 == index) {
            // get initialisation
            initv = segtemp->get_initialisation();
            if (initv.size() > 0) {
                ret = true;
            } else {
                mpd_url_t * url = segtemp->get_initurl();
                if (NULL != url) {
                    file = url->get_surl();
                    if (file.size() > 0) {
                        ret = true;
                    }
                    // range:
                    if (url->get_range().size() > 0) {
                        std::vector<size_t> rangelst;
                        val_split<size_t, std::string> (rangelst, url->get_range(), "-");
                        if (rangelst.size() > 0) {
                            chunk->set_byte_start(rangelst.at(0));
                        }
                        if (rangelst.size() > 1) {
                            chunk->set_byte_end(rangelst.at(1));
                        } else {
                            chunk->set_byte_end(chunk->get_byte_start());
                        }
                        chunk->set_media();
                    }
                    ret = true;
                }
            }
        } else {
            // get the max number of segment
        }
        if (initv.size() > 0) {
            ssize_t time = segtemp->get_timeline (index);
            if (time < 0) {
                time = 0;
            }
            assert (NULL != rep);
            ret = segtempl_urlformat (file, initv, rep->get_id(), index - 1 + snum, rep->get_bandwidth(), time);
        }
    }

    if ((! ret) && (NULL != segcomm->get_seglist())) {
        mpd_segmentlist_t * seglist = segcomm->get_seglist ();
        if (0 == index) {
            // init
            mpd_url_t * url = seglist->get_initurl();
            if (NULL != url) {
                file = url->get_surl();
                if (file.size() > 0) {
                    ret = true;
                }
                // range:
                if (url->get_range().size() > 0) {
                    std::vector<size_t> rangelst;
                    val_split<size_t, std::string> (rangelst, url->get_range(), "-");
                    if (rangelst.size() > 0) {
                        chunk->set_byte_start(rangelst.at(0));
                    }
                    if (rangelst.size() > 1) {
                        chunk->set_byte_end(rangelst.at(1));
                    } else {
                        chunk->set_byte_end(chunk->get_byte_start());
                    }
                    chunk->set_media();
                }
            }
        } else {
            std::vector<mpd_segmenturl_t *> & urls = seglist->get_segurl();
            if (urls.size() > 0) {
                if (index <= urls.size()) {
                    file = urls.at(index - 1)->get_media ();
                    ret = true;
                    if (urls.at(index - 1)->get_mediarange ().size() > 0) {
                        std::vector<size_t> rangelst;
                        val_split<size_t, std::string> (rangelst, urls.at(index - 1)->get_mediarange(), "-");
                        if (rangelst.size() > 0) {
                            chunk->set_byte_start(rangelst.at(0));
                        }
                        if (rangelst.size() > 1) {
                            chunk->set_byte_end(rangelst.at(1));
                        } else {
                            chunk->set_byte_end(chunk->get_byte_start());
                        }
                        chunk->set_media();
                    }
                }
            }
        }
    }

    if ((! ret) && (NULL != segcomm->get_segbase())) {
        mpd_segmentbase_t *segbase = segcomm->get_segbase();
        if (0 == index) {
            mpd_url_t * url = segbase->get_initurl();
            if (NULL != url) {
                file = url->get_surl();
                if (file.size() > 0) {
                    ret = true;
                }
                // range:
                if (url->get_range().size() > 0) {
                    std::vector<size_t> rangelst;
                    val_split<size_t, std::string> (rangelst, url->get_range(), "-");
                    if (rangelst.size() > 0) {
                        chunk->set_byte_start(rangelst.at(0));
                    }
                    if (rangelst.size() > 1) {
                        chunk->set_byte_end(rangelst.at(1));
                    } else {
                        chunk->set_byte_end(chunk->get_byte_start());
                    }
                    chunk->set_media();
                }
            }
        } else {
            // get index range
            //FIXME();
            if (segbase->get_idxrange().size() > 0) {
                std::vector<size_t> idxs;
                val_split<size_t, std::string> (idxs, segbase->get_idxrange(), "-");
                if (idxs.size() > 0) {
                    chunk->set_byte_start(idxs[0]);
                }
                if (idxs.size() > 1) {
                    chunk->set_byte_end(idxs[1]);
                } else {
                    chunk->set_byte_end(chunk->get_byte_start());
                }
                chunk->set_index(); // the caller should check if this chunk is for index
            }
        }
    }

    if (file.size() > 0) {
        chunk->set_url(file);
        ret = true;
    }
    if (segcomm->get_baseurl().size() > 0) {
        // add the baseurls as the prefix of all chunk's url/opt_urls
		// this will get a full URL for the segment file,
        // since this function(get_segment_from_segmentcommon) are called
        // in reverse order (representation, adaptation set, period)
        std::vector<mpd_baseurl_t *> & baseurls = segcomm->get_baseurl();
        add_prefixs_to_chunk (chunk, &baseurls);
        ret = true;
    }

    return ret;
}


/**
 * @brief get the next chunk
 *
 * @return the next Chunk of stream
 *
 * get the next chunk information according to current period/bitrate/render_ratio
 */
static mpd_chunk_t *
get_chunk_from_stack (mpd_period_t * period, mpd_adaptationset_t *adap, mpd_representation_t *rep, size_t index)
{
    bool ret;
    std::string file;
    std::string dir;

    mpd_chunk_t *chunk = new mpd_chunk_t();
    if (NULL == chunk) {
        return NULL;
    }

    ret = false;
    if (get_segment_from_segmentcommon (chunk, rep, rep, index)) {
        ret = true;
    }
    if (get_segment_from_segmentcommon (chunk, adap, rep, index)) {
        ret = true;
    }
    if (get_segment_from_segmentcommon (chunk, period, rep, index)) {
        ret = true;
    }
    if (! ret) {
        delete chunk;
        chunk = NULL;
    }
    return chunk;
}

/**
 * @brief get the segment chunk at position idx
 *
 * @param type : "video"/"audio"/"text"("application/ttml+xml")
 * @param lang : language, such as "en"/"de"/"fr", if not exist, use the default value in the MPD, such as "en", "und"
 * @param idx : the segment index. 0-init segment, 1-start of the startNumber, index == 1 + startNumber
 *
 * @return the chunk, NULL on failure
 *
 * get the segment chunk at position idx, the idx is the relative index in one Period.
 */
mpd_chunk_t *
get_chunk (mpd_t *pmpd, std::string & type, std::string & lang, double start_time, size_t avg_throughput, size_t idx)
{
    assert (NULL != pmpd);

    mpd_period_t * period = NULL;
    mpd_adaptationset_t *adap = NULL;
    mpd_representation_t *rep = NULL;
    if (false == pmpd->get_period_adapset_rep (type, lang, start_time, avg_throughput, &period, &adap, &rep)) {
        return NULL;
    }

    // calculate the current bandwidth by avg-throughput
    mpd_chunk_t * ret = get_chunk_from_stack (period, adap, rep, idx);
    if (NULL != ret) {
        std::vector<mpd_baseurl_t *> & baseurls = pmpd->get_baseurl();
        if (baseurls.size() > 0) {
            add_prefixs_to_chunk (ret, &baseurls);
        }
        //std::string prefix_default(pmpd->get_default_baseurl ());

        const std::string & url1 = pmpd->get_default_baseurl ();
        size_t pos1 = url1.rfind ('/');
        std::string baseurl;
        if (std::string::npos == pos1) {
            chk_prefixs_to_chunk (ret, baseurl);
        } else if (pos1 < url1.size()) {
            baseurl = url1;
            baseurl.erase (pos1 + 1, baseurl.size() - pos1);
            chk_prefixs_to_chunk (ret, baseurl);
        } else {
            chk_prefixs_to_chunk (ret, pmpd->get_default_baseurl ());
        }
        dump_mpdchunk_info (ret);
    }
    return ret;
}

