/**
 * @file    mympdparser.cc
 * @brief   parse the DASH MPD file data structures mapped directly from the description of the
 *          ISO/IEC 23009-1:2012 Dynamic adaptive streaming over HTTP (DASH) -- Part 1: Media presentation description and segment formats
 *          This file is a part of mympd, it contains the XML parser related functions
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-04-16
 */

#include <assert.h>

#include "mympd.h"

///////////////////////////////////////////////////////////////////////////////
// parser functions
/**
 * @brief process the text content of URLType
 *
 * @param text : the TEXT of XML tag
 *
 * @return false on no node
 *
 * process the text content of URLType
 */
//bool
//mpd_url_t::on_text (const char *text)
//{
//    return false;
//}

/**
 * @brief process the attributes of URLType
 *
 * @param attrName : the name of a attribute
 * @param attrValue : the value of a attribute
 *
 * @return false on no node
 *
 * process the text content of URLType
 */
bool
mpd_url_t::on_attribute (const char *attrName, const char *attrValue)
{
    if (! strcmp (attrName, "range")) {
        this->set_range (attrValue);
    } else if (! strcmp (attrName, "sourceURL")) {
        this->set_surl (attrValue);
    } else {
        return false;
    }
    return true;
}

/**
 * @brief process the text content of URLType
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of URLType
 */
//bool
//mpd_url_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
//{
//    return false;
//}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief process the text content of SegmentTimeline@S
 *
 * @param text : the TEXT of XML tag
 *
 * @return false on no node
 *
 * process the text content of SegmentTimeline@S
 */
//bool
//mpd_segmenttimeline_s_t::on_text (const char *text)
//{
//    return false;
//}

/**
 * @brief process the attributes of SegmentTimeline@S
 *
 * @param attrName : the name of a attribute
 * @param attrValue : the value of a attribute
 *
 * @return false on no node
 *
 * process the text content of SegmentTimeline@S
 */
bool
mpd_segmenttimeline_s_t::on_attribute (const char *attrName, const char *attrValue)
{
    if (! strcmp (attrName, "t")) {
        std::string val(attrValue);
        this->set_t (val);

    } else if (! strcmp (attrName, "d")) {
        std::string val(attrValue);
        this->set_d (val);

    } else if (! strcmp (attrName, "r")) {
        std::string val(attrValue);
        this->set_r (val);

    } else {
        return false;
    }
    return true;
}

/**
 * @brief process the text content of SegmentTimeline@S
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of SegmentTimeline@S
 */
//bool
//mpd_segmenttimeline_s_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
//{
//    return false;
//}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief process the text content of SegmentTimeline
 *
 * @param text : the TEXT of XML tag
 *
 * @return false on no node
 *
 * process the text content of SegmentTimeline
 */
//bool
//mpd_segmenttimeline_t::on_text (const char *text)
//{
//    return false;
//}

/**
 * @brief process the attributes of SegmentTimeline
 *
 * @param attrName : the name of a attribute
 * @param attrValue : the value of a attribute
 *
 * @return false on no node
 *
 * process the text content of SegmentTimeline
 */
//bool
//mpd_segmenttimeline_t::on_attribute (const char *attrName, const char *attrValue)
//{
//    return false;
//}

/**
 * @brief process the text content of SegmentTimeline
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of SegmentTimeline
 */
bool
mpd_segmenttimeline_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
{
    if (! strcmp (node_name, "S")) {
        mpd_segmenttimeline_s_t * burl = new mpd_segmenttimeline_s_t ();
        burl->parse_current (xmlrd);
        this->add_s (burl);

    } else {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief process the text content of SegmentBase
 *
 * @param text : the TEXT of XML tag
 *
 * @return false on no node
 *
 * process the text content of SegmentBase
 */
//bool
//mpd_segmentbase_t::on_text (const char *text)
//{
//    return false;
//}

/**
 * @brief process the attributes of SegmentBase
 *
 * @param attrName : the name of a attribute
 * @param attrValue : the value of a attribute
 *
 * @return false on no node
 *
 * process the text content of SegmentBase
 */
bool
mpd_segmentbase_t::on_attribute (const char *attrName, const char *attrValue)
{
    if (! strcmp (attrName, "timescale")) {
        std::string val(attrValue);
        this->set_timescale (val);

    } else if (! strcmp (attrName, "presentationTimeOffset")) {
        std::string val(attrValue);
        this->set_prestmoffset (val);

    } else if (! strcmp (attrName, "indexRange")) {
        std::string val(attrValue);
        this->set_idxrange (val);

    } else if (! strcmp (attrName, "indexRangeExact")) {
        std::string val(attrValue);
        this->set_idxrangeexact (val);

    } else {
        return false;
    }
    return true;
}

/**
 * @brief process the text content of SegmentBase
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of SegmentBase
 */
bool
mpd_segmentbase_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
{
    if (! strcmp (node_name, "Initialization")) {
        mpd_url_t * burl = new mpd_url_t ("Initialization");
        burl->parse_current (xmlrd);
        this->set_initurl (burl);

#if USE_COMPATIBILITY
    } else if (! strcmp (node_name, "InitialisationSegmentURL")) {
        mpd_url_t * burl = new mpd_url_t ("Initialization");
        burl->parse_current (xmlrd);
        this->set_initurl (burl);
#endif // USE_COMPATIBILITY

    } else if (! strcmp (node_name, "RepresentationIndex")) {
        mpd_url_t * burl = new mpd_url_t ("RepresentationIndex");
        burl->parse_current (xmlrd);
        this->set_initurl (burl);

    } else {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief process the text content of MultipleSegmentBaseInformation
 *
 * @param text : the TEXT of XML tag
 *
 * @return false on no node
 *
 * process the text content of MultipleSegmentBaseInformation
 */
//bool
//mpd_segmentmultibase_t::on_text (const char *text)
//{
//    return false;
//}

/**
 * @brief process the attributes of MultipleSegmentBaseInformation
 *
 * @param attrName : the name of a attribute
 * @param attrValue : the value of a attribute
 *
 * @return false on no node
 *
 * process the text content of MultipleSegmentBaseInformation
 */
bool
mpd_segmentmultibase_t::on_attribute (const char *attrName, const char *attrValue)
{
    if (mpd_segmentbase_t::on_attribute (attrName, attrValue)) {
        return true;

    } else if (! strcmp (attrName, "duration")) {
        std::string val(attrValue);
        this->set_duration (val);

    } else if (! strcmp (attrName, "startNumber")) {
        std::string val(attrValue);
        this->set_snum (val);

    } else {
        return false;
    }
    return true;
}

/**
 * @brief process the text content of MultipleSegmentBaseInformation
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of MultipleSegmentBaseInformation
 */
bool
mpd_segmentmultibase_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
{
    assert (NULL != node_name);

    if (mpd_segmentbase_t::parse_content (xmlrd, node_name)) {
        return true;

    } else if (! strcmp (node_name, "SegmentTimeline")) {
        mpd_segmenttimeline_t * burl = new mpd_segmenttimeline_t ();
        burl->parse_current (xmlrd);
        this->set_segtmln (burl);

    } else if (! strcmp (node_name, "BitstreamSwitching")) {
        mpd_url_t * burl = new mpd_url_t ("BitstreamSwitching");
        burl->parse_current (xmlrd);
        this->set_bitswitch (burl);

    } else {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief process the text content of SegmentURL
 *
 * @param text : the TEXT of XML tag
 *
 * @return false on no node
 *
 * process the text content of SegmentURL
 */
//bool
//mpd_segmenturl_t::on_text (const char *text)
//{
//    return false;
//}

/**
 * @brief process the attributes of SegmentURL
 *
 * @param attrName : the name of a attribute
 * @param attrValue : the value of a attribute
 *
 * @return false on no node
 *
 * process the text content of SegmentURL
 */
bool
mpd_segmenturl_t::on_attribute (const char *attrName, const char *attrValue)
{
    if (! strcmp (attrName, "media")) {
        this->set_media (attrValue);
#if USE_COMPATIBILITY
    } else if (! strcmp (attrName, "sourceURL")) {
        this->set_media (attrValue);
#endif // USE_COMPATIBILITY
    } else if (! strcmp (attrName, "index")) {
        this->set_index (attrValue);
    } else if (! strcmp (attrName, "mediaRange")) {
        this->set_mediarange (attrValue);
    } else if (! strcmp (attrName, "indexRange")) {
        this->set_indexrange (attrValue);

    } else {
        return false;
    }
    return true;
}

/**
 * @brief process the text content of SegmentURL
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of SegmentURL
 */
//bool
//mpd_segmenturl_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
//{
//    return false;
//}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief process the text content of MultipleSegmentBaseInformation
 *
 * @param text : the TEXT of XML tag
 *
 * @return false on no node
 *
 * process the text content of MultipleSegmentBaseInformation
 */
//bool
//mpd_segmentlist_t::on_text (const char *text)
//{
//    return false;
//}

/**
 * @brief process the attributes of MultipleSegmentBaseInformation
 *
 * @param attrName : the name of a attribute
 * @param attrValue : the value of a attribute
 *
 * @return false on no node
 *
 * process the text content of MultipleSegmentBaseInformation
 */
bool
mpd_segmentlist_t::on_attribute (const char *attrName, const char *attrValue)
{
    if (! strcmp (attrName, "xlink:href")) {
        this->set_xhref (attrValue);
    } else if (! strcmp (attrName, "xlink:actuate")) {
        this->set_xactuate (attrValue);


    } else if (! mpd_segmentmultibase_t::on_attribute (attrName, attrValue)) {

        return false;
    }
    return true;
}

/**
 * @brief process the text content of MultipleSegmentBaseInformation
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of MultipleSegmentBaseInformation
 */
bool
mpd_segmentlist_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
{
    assert (NULL != node_name);

    if (! strcmp (node_name, "SegmentURL")) {
        mpd_segmenturl_t * burl = new mpd_segmenturl_t ();
        burl->parse_current (xmlrd);
        this->add_segurl (burl);

#if USE_COMPATIBILITY
    } else if (! strcmp (node_name, "Url")) {
        mpd_segmenturl_t * burl = new mpd_segmenturl_t ();
        burl->parse_current (xmlrd);
        this->add_segurl (burl);
#endif // USE_COMPATIBILITY

    } else if (! mpd_segmentmultibase_t::parse_content (xmlrd, node_name)) {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief process the text content of SegmentTemplate
 *
 * @param text : the TEXT of XML tag
 *
 * @return false on no node
 *
 * process the text content of SegmentTemplate
 */
//bool
//mpd_segmenttemplate_t::on_text (const char *text)
//{
//    return false;
//}

/**
 * @brief process the attributes of SegmentTemplate
 *
 * @param attrName : the name of a attribute
 * @param attrValue : the value of a attribute
 *
 * @return false on no node
 *
 * process the text content of SegmentTemplate
 */
bool
mpd_segmenttemplate_t::on_attribute (const char *attrName, const char *attrValue)
{
    if (! strcmp (attrName, "media")) {
        this->set_media (attrValue);
    } else if (! strcmp (attrName, "index")) {
        this->set_index (attrValue);
    } else if (! strcmp (attrName, "initialization")) {
        this->set_initialisation (attrValue);
#if FIX_SORENSON_SQUEEZE
    } else if (! strcmp (attrName, "initialisation")) { // typo in sorenson
        this->set_initialisation (attrValue);
#endif
    } else if (! strcmp (attrName, "bitstreamSwitching")) {
        this->set_bitswitch (attrValue);

    } else if (! mpd_segmentmultibase_t::on_attribute (attrName, attrValue)) {
        return false;
    }
    return true;
}

/**
 * @brief process the text content of SegmentTemplate
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of SegmentTemplate
 */
//bool
//mpd_segmenttemplate_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
//{
//    assert (NULL != node_name);
//
//    if (! mpd_segmentmultibase_t::parse_content (xmlrd, node_name)) {
//        return false;
//    }
//    return true;
//}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief process the text content of DescriptorType
 *
 * @param text : the TEXT of XML tag
 *
 * @return false on no node
 *
 * process the text content of DescriptorType
 */
//bool
//mpd_descriptor_t::on_text (const char *text)
//{
//    return false;
//}

/**
 * @brief process the attributes of DescriptorType
 *
 * @param attrName : the name of a attribute
 * @param attrValue : the value of a attribute
 *
 * @return false on no node
 *
 * process the text content of DescriptorType
 */
bool
mpd_descriptor_t::on_attribute (const char *attrName, const char *attrValue)
{
    if (! strcmp (attrName, "schemeIdUri")) {
        this->set_schid (attrValue);
    } else if (! strcmp (attrName, "value")) {
        this->set_value (attrValue);

    } else {
        return false;
    }
    return true;
}

/**
 * @brief process the text content of DescriptorType
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of DescriptorType
 */
//bool
//mpd_descriptor_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
//{
//    return false;
//}


///////////////////////////////////////////////////////////////////////////////
/**
 * @brief process the text content of BaseURL
 *
 * @param text : the TEXT of XML tag
 *
 * @return false on no node
 *
 * process the text content of BaseURL
 */
bool
mpd_baseurl_t::on_text (const char *text)
{
    this->set_url (text);
    return true;
}

/**
 * @brief process the attributes of BaseURL
 *
 * @param attrName : the name of a attribute
 * @param attrValue : the value of a attribute
 *
 * @return false on no node
 *
 * process the text content of BaseURL
 */
bool
mpd_baseurl_t::on_attribute (const char *attrName, const char *attrValue)
{
    if (! strcmp (attrName, "byteRange")) {
        this->set_brange (attrValue);
    } else if (! strcmp (attrName, "serviceLocation")) {
        this->set_svcloc (attrValue);

    } else {
        return false;
    }
    return true;
}

/**
 * @brief process the text content of BaseURL
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of BaseURL
 */
//bool
//mpd_baseurl_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
//{
//    return false;
//}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief process the text content of RepresentationBaseType
 *
 * @param text : the TEXT of XML tag
 *
 * @return false on no node
 *
 * process the text content of RepresentationBaseType
 */
//bool
//mpd_representation_base_t::on_text (const char *text)
//{
//    return false;
//}

/**
 * @brief process the attributes of RepresentationBaseType
 *
 * @param attrName : the name of a attribute
 * @param attrValue : the value of a attribute
 *
 * @return false on no node
 *
 * process the text content of RepresentationBaseType
 */
bool
mpd_representation_base_t::on_attribute (const char *attrName, const char *attrValue)
{
    if (! strcmp (attrName, "mimeType")) {
        std::string val(attrValue);
        this->set_mimetype (val);

    } else if (! strcmp (attrName, "codecs")) {
        std::string val(attrValue);
        this->set_codecs (val);

    } else if (! strcmp (attrName, "width")) {
        std::string val(attrValue);
        this->set_width (val);

    } else if (! strcmp (attrName, "height")) {
        std::string val(attrValue);
        this->set_height (val);

    } else if (! strcmp (attrName, "startWithSAP")) {
        std::string val(attrValue);
        this->set_startsap(val);

    } else if (! strcmp (attrName, "profiles")) {
        std::string val(attrValue);
        this->set_profiles (val);

    } else if (! strcmp (attrName, "sar")) {
        std::string val(attrValue);
        this->set_sar(val);

    } else if (! strcmp (attrName, "frameRate")) {
        std::string val(attrValue);
        this->set_fmrate(val);

    } else if (! strcmp (attrName, "audioSamplingRate")) {
        std::string val(attrValue);
        this->set_audsamp(val);

    } else if (! strcmp (attrName, "segmentProfiles")) {
        std::string val(attrValue);
        this->set_segprofiles(val);

    } else if (! strcmp (attrName, "maximumSAPPeriod")) {
        std::string val(attrValue);
        this->set_sapperiodmax(val);

    } else if (! strcmp (attrName, "maxPlayoutRate")) {
        std::string val(attrValue);
        this->set_plratemax(val);

    } else if (! strcmp (attrName, "codingDependency")) {
        std::string val(attrValue);
        this->set_codedep(val);

    } else if (! strcmp (attrName, "scanType")) {
        std::string val(attrValue);
        this->set_scantype(val);

    } else {
        return false;
    }
    return true;
}

/**
 * @brief process the text content of RepresentationBaseType
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of RepresentationBaseType
 */
bool
mpd_representation_base_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
{
    assert (NULL != node_name);

    if (! strcmp (node_name, "FramePacking")) {
        mpd_descriptor_t * peri = new mpd_descriptor_t ("FramePacking");
        peri->parse_current (xmlrd);
        this->add_framepck (peri);

    } else if (! strcmp (node_name, "AudioChannelConfiguration")) {
        mpd_descriptor_t * peri = new mpd_descriptor_t ("AudioChannelConfiguration");
        peri->parse_current (xmlrd);
        this->add_audconf (peri);

    } else if (! strcmp (node_name, "ContentProtection")) {
        mpd_descriptor_t * peri = new mpd_descriptor_t ("ContentProtection");
        peri->parse_current (xmlrd);
        this->add_cntprotect (peri);

    } else {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief process the text content of SubRepresentation
 *
 * @param text : the TEXT of XML tag
 *
 * @return false on no node
 *
 * process the text content of SubRepresentation
 */
//bool
//mpd_subrepres_t::on_text (const char *text)
//{
//    return false;
//}

/**
 * @brief process the attributes of SubRepresentation
 *
 * @param attrName : the name of a attribute
 * @param attrValue : the value of a attribute
 *
 * @return false on no node
 *
 * process the text content of SubRepresentation
 */
bool
mpd_subrepres_t::on_attribute (const char *attrName, const char *attrValue)
{
    if (! strcmp (attrName, "level")) {
        this->set_level (attrValue);
    } else if (! strcmp (attrName, "dependencyLevel")) {
        this->set_deplevel (attrValue);
    } else if (! strcmp (attrName, "bandwidth")) {
        this->set_bandwidth (attrValue);
    } else if (! strcmp (attrName, "contentComponent")) {
        this->set_cntcomp (attrValue);

    } else if (! mpd_representation_base_t::on_attribute (attrName, attrValue)) {
        return false;
    }
    return true;
}

/**
 * @brief process the text content of SubRepresentation
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of SubRepresentation
 */
bool
mpd_subrepres_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
{
    assert (NULL != node_name);

    if (! mpd_representation_base_t::parse_content (xmlrd, node_name)) {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief process the text content of RepresentationType
 *
 * @param text : the TEXT of XML tag
 *
 * @return false on no node
 *
 * process the text content of RepresentationType
 */
//bool
//mpd_representation_t::on_text (const char *text)
//{
//    return false;
//}

/**
 * @brief process the attributes of RepresentationType
 *
 * @param attrName : the name of a attribute
 * @param attrValue : the value of a attribute
 *
 * @return false on no node
 *
 * process the text content of RepresentationType
 */
bool
mpd_representation_t::on_attribute (const char *attrName, const char *attrValue)
{
    if (! strcmp (attrName, "id")) {
        this->set_id (attrValue);
    } else if (! strcmp (attrName, "bandwidth")) {
        this->set_bandwidth (attrValue);
    } else if (! strcmp (attrName, "qualityRanking")) {
        this->set_qranking (attrValue);
    } else if (! strcmp (attrName, "dependencyId")) {
        this->set_depid (attrValue);
    } else if (! strcmp (attrName, "mediaStreamStructureId")) {
        this->set_structid (attrValue);
    } else if (! mpd_representation_base_t::on_attribute (attrName, attrValue)) {
        return false;
    }
    return true;
}

/**
 * @brief process the text content of RepresentationType
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of RepresentationType
 */
bool
mpd_representation_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
{
    assert (NULL != node_name);
    if (! strcmp (node_name, "SubRepresentation")) {
        mpd_subrepres_t * segtmp = new mpd_subrepres_t ();
        segtmp->parse_current (xmlrd);
        this->add_subrepres (segtmp);

#if USE_COMPATIBILITY
    } else if (! strcmp (node_name, "SegmentInfo")) {
        mpd_segmentlist_t * segtmp = new mpd_segmentlist_t ();
        segtmp->parse_current (xmlrd);
        this->set_seglist (segtmp);
#endif // USE_COMPATIBILITY

    } else if (mpd_segmentcommon_t::parse_content (xmlrd, node_name)) {

    } else if (! mpd_representation_base_t::parse_content (xmlrd, node_name)) {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief process the text content of ContentComponent
 *
 * @param text : the TEXT of XML tag
 *
 * @return false on no node
 *
 * process the text content of ContentComponent
 */
//bool
//mpd_contentcomponent_t::on_text (const char *text)
//{
//    return false;
//}

/**
 * @brief process the attributes of ContentComponent
 *
 * @param attrName : the name of a attribute
 * @param attrValue : the value of a attribute
 *
 * @return false on no node
 *
 * process the text content of ContentComponent
 */
bool
mpd_contentcomponent_t::on_attribute (const char *attrName, const char *attrValue)
{
    if (! strcmp (attrName, "id")) {
        this->set_id (attrValue);
    } else if (! strcmp (attrName, "contentType")) {
        this->set_cnttype (attrValue);
    } else if (! strcmp (attrName, "lang")) {
        this->set_lang (attrValue);
    } else if (! strcmp (attrName, "par")) {
        this->set_par (attrValue);
    } else {
        return false;
    }
    return true;
}

/**
 * @brief process the text content of ContentComponent
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of ContentComponent
 */
bool
mpd_contentcomponent_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
{
    assert (NULL != node_name);
    if (! strcmp (node_name, "Accessibility")) {
        mpd_descriptor_t * peri = new mpd_descriptor_t ("Accessibility");
        peri->parse_current (xmlrd);
        this->add_access (peri);

    } else if (! strcmp (node_name, "Role")) {
        mpd_descriptor_t * peri = new mpd_descriptor_t ("Role");
        peri->parse_current (xmlrd);
        this->add_role (peri);

    } else if (! strcmp (node_name, "Rating")) {
        mpd_descriptor_t * peri = new mpd_descriptor_t ("Rating");
        peri->parse_current (xmlrd);
        this->add_rating (peri);

    } else if (! strcmp (node_name, "Viewpoint")) {
        mpd_descriptor_t * peri = new mpd_descriptor_t ("Viewpoint");
        peri->parse_current (xmlrd);
        this->add_viewpoint (peri);

    } else {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief process the text content of AdaptationSet
 *
 * @param text : the TEXT of XML tag
 *
 * @return false on no node
 *
 * process the text content of AdaptationSet
 */
//bool
//mpd_adaptationset_t::on_text (const char *text)
//{
//    return false;
//}

/**
 * @brief process the attributes of AdaptationSet
 *
 * @param attrName : the name of a attribute
 * @param attrValue : the value of a attribute
 *
 * @return false on no node
 *
 * process the text content of AdaptationSet
 */
bool
mpd_adaptationset_t::on_attribute (const char *attrName, const char *attrValue)
{
    if (! strcmp (attrName, "id")) {
        this->set_id (attrValue);
    } else if (! strcmp (attrName, "subsegmentAlignment")) {
        this->set_subsegalign (attrValue);
    } else if (! strcmp (attrName, "subsegmentStartsWithSAP")) {
        this->set_subsegsap (attrValue);
    } else if (! strcmp (attrName, "lang")) {
        this->set_lang (attrValue);
    } else if (! strcmp (attrName, "bitstreamSwitching")) {
        this->set_bitswitch (attrValue);
    } else if (! strcmp (attrName, "group")) {
        this->set_group (attrValue);
    } else if (! strcmp (attrName, "par")) {
        this->set_par (attrValue);
    } else if (! strcmp (attrName, "segmentAlignment")) {
        this->set_segalign (attrValue);
#if USE_COMPATIBILITY
    } else if (! strcmp (attrName, "segmentAlignmentFlag")) {
        this->set_segalign (attrValue);
#endif // USE_COMPATIBILITY
    } else if (! strcmp (attrName, "xlink:href")) {
        this->set_xhref (attrValue);
    } else if (! strcmp (attrName, "xlink:actuate")) {
        this->set_xactuate (attrValue);
    } else if (! strcmp (attrName, "contentType")) {
        this->set_cnttype (attrValue);
    } else if (! strcmp (attrName, "minBandwidth")) {
        this->set_bwmin (attrValue);
    } else if (! strcmp (attrName, "maxBandwidth")) {
        this->set_bwmax (attrValue);
    } else if (! strcmp (attrName, "minWidth")) {
        this->set_widthmin (attrValue);
    } else if (! strcmp (attrName, "maxWidth")) {
        this->set_widthmax (attrValue);
    } else if (! strcmp (attrName, "minHeight")) {
        this->set_heightmin (attrValue);
    } else if (! strcmp (attrName, "maxHeight")) {
        this->set_heightmax (attrValue);
    } else if (! strcmp (attrName, "minFrameRate")) {
        this->set_fmratemin (attrValue);
    } else if (! strcmp (attrName, "maxFrameRate")) {
        this->set_fmratemax (attrValue);

    } else if (! mpd_representation_base_t::on_attribute (attrName, attrValue)) {

        return false;
    }
    return true;
}

/**
 * @brief process the text content of AdaptationSet
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of AdaptationSet
 */
bool
mpd_adaptationset_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
{
    assert (NULL != node_name);
    if (! strcmp (node_name, "Representation")) {
        mpd_representation_t * peri = new mpd_representation_t ();
        peri->parse_current (xmlrd);
#if DEBUG
        peri->verify ();
#endif // DEBUG
        this->add_represent (peri);

    } else if (! strcmp (node_name, "ContentComponent")) {
        mpd_contentcomponent_t * peri = new mpd_contentcomponent_t ();
        peri->parse_current (xmlrd);
        this->add_cntcomponent (peri);

    } else if (! strcmp (node_name, "Accessibility")) {
        mpd_descriptor_t * peri = new mpd_descriptor_t ("Accessibility");
        peri->parse_current (xmlrd);
        this->add_access (peri);

    } else if (! strcmp (node_name, "Role")) {
        mpd_descriptor_t * peri = new mpd_descriptor_t ("Role");
        peri->parse_current (xmlrd);
        this->add_role (peri);

    } else if (! strcmp (node_name, "Rating")) {
        mpd_descriptor_t * peri = new mpd_descriptor_t ("Rating");
        peri->parse_current (xmlrd);
        this->add_rating (peri);

    } else if (! strcmp (node_name, "Viewpoint")) {
        mpd_descriptor_t * peri = new mpd_descriptor_t ("Viewpoint");
        peri->parse_current (xmlrd);
        this->add_viewpoint (peri);

    } else if (mpd_segmentcommon_t::parse_content (xmlrd, node_name)) {

    } else if (! mpd_representation_base_t::parse_content (xmlrd, node_name)) {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief process the text content of SubsetType
 *
 * @param text : the TEXT of XML tag
 *
 * @return false on no node
 *
 * process the text content of SubsetType
 */
//bool
//mpd_subset_t::on_text (const char *text)
//{
//    return false;
//}

/**
 * @brief process the attributes of SubsetType
 *
 * @param attrName : the name of a attribute
 * @param attrValue : the value of a attribute
 *
 * @return false on no node
 *
 * process the text content of SubsetType
 */
bool
mpd_subset_t::on_attribute (const char *attrName, const char *attrValue)
{
    if (! strcmp (attrName, "contains")) {
        this->set_contains (attrValue);

    } else {
        return false;
    }
    return true;
}

/**
 * @brief process the text content of SubsetType
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of SubsetType
 */
//bool
//mpd_subset_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
//{
//    return false;
//}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief process the text content of Period
 *
 * @param text : the TEXT of XML tag
 *
 * @return false on no node
 *
 * process the text content of Period
 */
//bool
//mpd_period_t::on_text (const char *text)
//{
//    return false;
//}

/**
 * @brief process the attributes of Period
 *
 * @param attrName : the name of a attribute
 * @param attrValue : the value of a attribute
 *
 * @return false on no node
 *
 * process the text content of Period
 */
bool
mpd_period_t::on_attribute (const char *attrName, const char *attrValue)
{
    if (! strcmp (attrName, "id")) {
        this->set_id (attrValue);
    } else if (! strcmp (attrName, "start")) {
std::cerr << "in onattribute -------------before this->start=" << this->start << std::endl;
        this->set_start (attrValue);
std::cerr << "in onattribute -------------after this->start=" << this->start << std::endl;
    } else if (! strcmp (attrName, "duration")) {
        this->set_duration (attrValue);
    } else if (! strcmp (attrName, "xlink:href")) {
        this->set_xhref (attrValue);
    } else if (! strcmp (attrName, "xlink:actuate")) {
        this->set_xactuate (attrValue);
    } else if (! strcmp (attrName, "bitstreamSwitching")) {
        this->set_bitswitch (attrValue);

    } else {
        return false;
    }
    return true;
}

/**
 * @brief process the text content of Period
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of Period
 */
bool
mpd_period_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
{
    assert (NULL != node_name);
    if (! strcmp (node_name, "AdaptationSet")) {
        mpd_adaptationset_t * peri = new mpd_adaptationset_t ();
        peri->parse_current (xmlrd);
        this->add_adaptset (peri);

#if USE_COMPATIBILITY
    } else if (! strcmp (node_name, "Group")) {
        mpd_adaptationset_t * peri = new mpd_adaptationset_t ();
        peri->parse_current (xmlrd);
        this->add_adaptset (peri);

    } else if (! strcmp (node_name, "Representation")) {
        // todo: add adaptationset
        mpd_adaptationset_t * adapset = new mpd_adaptationset_t ();
        this->add_adaptset (adapset);
        mpd_representation_t * reps = new mpd_representation_t ();
        reps->parse_current (xmlrd);
#if DEBUG
        reps->verify ();
#endif // DEBUG
        adapset->add_represent (reps);
#endif // USE_COMPATIBILITY

    } else if (! strcmp (node_name, "Subset")) {
        mpd_subset_t * peri = new mpd_subset_t ();
        peri->parse_current (xmlrd);
        this->add_subset (peri);

    } else if (! mpd_segmentcommon_t::parse_content (xmlrd, node_name)) {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief process the text content of segmentcommon
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of segmentcommon
 */
bool
mpd_segmentcommon_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
{
    assert (NULL != node_name);
    if (! strcmp (node_name, "BaseURL")) {
        mpd_baseurl_t * burl = new mpd_baseurl_t ();
        burl->parse_current (xmlrd);
        this->add_baseurl (burl);

    } else if (! strcmp (node_name, "SegmentTemplate")) {
        mpd_segmenttemplate_t * segtmp = new mpd_segmenttemplate_t ();
        segtmp->parse_current (xmlrd);
        this->set_segtemplate (segtmp);

    } else if (! strcmp (node_name, "SegmentBase")) {
        mpd_segmentbase_t * segtmp = new mpd_segmentbase_t ();
        segtmp->parse_current (xmlrd);
        this->set_segbase (segtmp);

    } else if (! strcmp (node_name, "SegmentList")) {
        mpd_segmentlist_t * segtmp = new mpd_segmentlist_t ();
        segtmp->parse_current (xmlrd);
        this->set_seglist (segtmp);

    } else {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief process the text content of ProgramInformationType
 *
 * @param text : the TEXT of XML tag
 *
 * @return false on no node
 *
 * process the text content of ProgramInformationType
 */
//bool
//mpd_proginfo_t::on_text (const char *text)
//{
//    return false;
//}

/**
 * @brief process the attributes of ProgramInformationType
 *
 * @param attrName : the name of a attribute
 * @param attrValue : the value of a attribute
 *
 * @return false on no node
 *
 * process the text content of ProgramInformationType
 */
bool
mpd_proginfo_t::on_attribute (const char *attrName, const char *attrValue)
{
    if (! strcmp (attrName, "lang")) {
        this->set_lang (attrValue);
    } else if (! strcmp (attrName, "moreInformationURL")) {
        this->set_moreinfourl (attrValue);

    } else {
        return false;
    }
    return true;
}

/**
 * @brief process the text content of ProgramInformationType
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of ProgramInformationType
 */
bool
mpd_proginfo_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
{
    assert (NULL != node_name);
    if (! strcmp (node_name, "Title")) {
        xml_parser_stdstring_t burl;
        burl.parse_current (xmlrd);
        this->set_title (burl);

    } else if (! strcmp (node_name, "Source")) {
        xml_parser_stdstring_t peri;
        peri.parse_current (xmlrd);
        this->set_source (peri);

    } else if (! strcmp (node_name, "Copyright")) {
        xml_parser_stdstring_t segtmp;
        segtmp.parse_current (xmlrd);
        this->set_cyright (segtmp);

    } else {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief process the text content of RangeType
 *
 * @param text : the TEXT of XML tag
 *
 * @return false on no node
 *
 * process the text content of RangeType
 */
//bool
//mpd_range_t::on_text (const char *text)
//{
//    return false;
//}

/**
 * @brief process the attributes of RangeType
 *
 * @param attrName : the name of a attribute
 * @param attrValue : the value of a attribute
 *
 * @return false on no node
 *
 * process the text content of RangeType
 */
bool
mpd_range_t::on_attribute (const char *attrName, const char *attrValue)
{
    if (! strcmp (attrName, "starttime")) {
        this->set_starttime (attrValue);
    } else if (! strcmp (attrName, "duration")) {
        this->set_duration (attrValue);

    } else {
        return false;
    }
    return true;
}

/**
 * @brief process the text content of RangeType
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of RangeType
 */
//bool
//mpd_range_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
//{
//    return false;
//}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief process the text content of MetricsType
 *
 * @param text : the TEXT of XML tag
 *
 * @return false on no node
 *
 * process the text content of MetricsType
 */
//bool
//mpd_metrics_t::on_text (const char *text)
//{
//    return false;
//}

/**
 * @brief process the attributes of MetricsType
 *
 * @param attrName : the name of a attribute
 * @param attrValue : the value of a attribute
 *
 * @return false on no node
 *
 * process the text content of MetricsType
 */
bool
mpd_metrics_t::on_attribute (const char *attrName, const char *attrValue)
{
    if (! strcmp (attrName, "metrics")) {
        this->set_metrics (attrValue);

    } else {
        return false;
    }
    return true;
}

/**
 * @brief process the text content of MetricsType
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of MetricsType
 */
bool
mpd_metrics_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
{
    assert (NULL != node_name);
    if (! strcmp (node_name, "Reporting")) {
        mpd_descriptor_t * peri = new mpd_descriptor_t ("Reporting");
        peri->parse_current (xmlrd);
        this->add_report (peri);

    } else if (! strcmp (node_name, "Range")) {
        mpd_range_t * peri = new mpd_range_t ();
        peri->parse_current (xmlrd);
        this->add_range (peri);

    } else {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief process the text content of MPD
 *
 * @param text : the TEXT of XML tag
 *
 * @return false on no node
 *
 * process the text content of MPD
 */
//bool
//mpd_t::on_text (const char *text)
//{
//    return false;
//}

/**
 * @brief process the attributes of MPD
 *
 * @param attrName : the name of a attribute
 * @param attrValue : the value of a attribute
 *
 * @return false on no node
 *
 * process the text content of MPD
 */
bool
mpd_t::on_attribute (const char *attrName, const char *attrValue)
{
    if (! strcmp (attrName, "id")) {
        this->set_id (attrValue);
    } else if (! strcmp (attrName, "xmlns:xsi")) {
        this->set_xxsi (attrValue);
    } else if (! strcmp (attrName, "xmlns")) {
        this->set_xmlns (attrValue);
    } else if (! strcmp (attrName, "xsi:schemaLocation")) {
        this->set_xxsischloc (attrValue);
    } else if (! strcmp (attrName, "profiles")) {
#if FIX_SORENSON_SQUEEZE
        if (! strcmp (attrValue, "urn:mpeg:dash:profiles:isoff-main:2011")) {
            // fix sorenson squeeze's error (squeeze pro 8)
            attrValue = "urn:mpeg:dash:profile:isoff-main:2011";
        }
#endif
#if USE_COMPATIBILITY
        if (! strcmp (attrValue, "urn:mpeg:mpegB:profile:dash:isoff-basic-on-demand:cm")) {
            // mtbike.mpd
            attrValue = "urn:mpeg:dash:profile:mp2t-simple:2011";
        }
#endif // USE_COMPATIBILITY
        if (0 == strcmp("urn:mpeg:dash:profile:isoff-live:2011", attrValue)) {
            this->set_live(true);
        }
        this->set_profiles (attrValue);
    } else if (! strcmp (attrName, "type")) {
        this->set_type (attrValue);
    } else if (! strcmp (attrName, "mediaPresentationDuration")) {
        this->set_pres_duration (attrValue);
    } else if (! strcmp (attrName, "minBufferTime")) {
        this->set_min_buf_time (attrValue);
    } else if (! strcmp (attrName, "availabilityStartTime")) {
        this->set_avstarttime (attrValue);
    } else if (! strcmp (attrName, "availabilityEndTime")) {
        this->set_avendtime (attrValue);
    } else if (! strcmp (attrName, "minimumUpdatePeriod")) {
        this->set_minupdperiod (attrValue);
    } else if (! strcmp (attrName, "timeShiftBufferDepth")) {
        this->set_bufdeptmsft (attrValue);
    } else if (! strcmp (attrName, "suggestedPresentationDelay")) {
        this->set_sugpresentdelay (attrValue);
    } else if (! strcmp (attrName, "maxSegmentDuration")) {
        this->set_maxsegduration (attrValue);
    } else if (! strcmp (attrName, "maxSubsegmentDuration")) {
        this->set_maxsubsegduration (attrValue);
    } else {
        return false;
    }
    return true;
}

#if 1
/**
 * @brief process the text content of MPD
 *
 * @param xmlrd : the XML reader pointer
 * @param node_name : the node name
 *
 * @return false on no node
 *
 * process the text content of MPD
 */
bool
mpd_t::parse_content (xml_reader_base_t * xmlrd, const char *node_name)
{
    assert (NULL != node_name);
    if (! strcmp (node_name, "BaseURL")) {
        mpd_baseurl_t * burl = new mpd_baseurl_t ();
        burl->parse_current (xmlrd);
        this->add_baseurl (burl);

    } else if (! strcmp (node_name, "Period")) {
        mpd_period_t * peri = new mpd_period_t ();
        peri->parse_current (xmlrd);
        this->add_period (peri);

    } else if (! strcmp (node_name, "ProgramInformation")) {
        mpd_proginfo_t * peri = new mpd_proginfo_t ();
        peri->parse_current (xmlrd);
        this->add_proginfo (peri);

    } else if (! strcmp (node_name, "Location")) {
        xml_parser_stdstring_t peri;
        peri.parse_current (xmlrd);
        this->add_location (peri);

    } else if (! strcmp (node_name, "Metrics")) {
        mpd_metrics_t * peri = new mpd_metrics_t ();
        peri->parse_current (xmlrd);
        this->add_metric (peri);

    } else {
        return false;
    }
    return true;
}
#endif

/**
 * @brief parse the MPD file
 *
 * @param xmlrd : the XML reader pointer
 * @param default_url : default URL for MPD
 *
 * @return false on no node
 *
 * parse the MPD file
 */
mpd_t *
mpd_parse_current (xml_reader_base_t * xmlrd, std::string &default_url)
{
    xml_reader_base_t::type_t type;
    const char *data = NULL;
    type = xmlrd->NextNode (&data);

    switch (type) {
    case xml_reader_base_t::ENDELEM:
    case xml_reader_base_t::NONE:
    case xml_reader_base_t::ERROR:
        std::cout << "Unprocessed Type: " << type << "; Text: " << data << std::endl;
        return NULL;

    case xml_reader_base_t::TEXT:
        std::cout << "Type: " << type << "; Text: " << data << std::endl;
        return NULL;
        break;

    default:
    {
        if (strcmp (data, "MPD")) {
            std::cout << "Not MPD, Type: " << type << "; Text: " << data << std::endl;
            return NULL;
        }
        mpd_t * mpd = new mpd_t (default_url);
        mpd->parse_current (xmlrd);
        mpd->post_parser ();
        return mpd;
    }
    }
    return NULL;
}
