/**
 * @file    mympdalg.cc
 * @brief   DASH adaptitive algorithms
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-04-16
 */
#include <math.h> // ceil(), floor()
#include <iostream>

#include "mympdutils.h"
#include "mympdalg.h"

// check if the current throughput is higher range [bandwidth_upper1, bandwidth_upper2]
// pre-condition: bandwidth_upper1 <= bandwidth_upper2
bool
mpd_adaptationlogic_yhfu_t::is_gt_upper (size_t bandwidth_current, size_t bandwidth_upper1, size_t bandwidth_upper2) const
{
    assert (bandwidth_upper1 <= bandwidth_upper2);
    size_t bitrate_threshold_increase;
    bitrate_threshold_increase = (1 - ratio_threshold_increase) * (double)bandwidth_upper1
        + ratio_threshold_increase * (double)bandwidth_upper2;
    if (last_aavg >= bitrate_threshold_increase) {
        return true;
    }
    return false;
}

// check if the current throughput is below the range [bandwidth_lower1, bandwidth_lower2]
// pre-condition: bandwidth_lower1 <= bandwidth_lower2
bool
mpd_adaptationlogic_yhfu_t::is_lt_lower (size_t throughput_avg, size_t bandwidth_lower1, size_t bandwidth_lower2) const
{
    size_t bitrate_threshold_reduction;
    assert (bandwidth_lower1 <= bandwidth_lower2);
    bitrate_threshold_reduction = (1 - ratio_threshold_reduction) * (double)bandwidth_lower2
        + ratio_threshold_reduction * (double)bandwidth_lower1;
    if (throughput_avg <= bitrate_threshold_reduction) {
        return true;
    }
    return false;
}

/**
 * @brief peek the next available chunk
 *
 * @param type : "video"/"audio"/"text"("application/ttml+xml")
 * @param lang : language, such as "en"/"de"/"fr"
 * @param pidx : a pointer to a value, which will be returned with the current used chunk idx
 * @param pstart_time: a pointer to a value, which will be returned with the start_time of the chunk
 *
 * @return the chunk, NULL on failure
 *
 * peek the next available chunk, base on the current avg throughput, video type, language etc.
 */
mpd_chunk_t *
mpd_adaptationlogic_t::peek_next_chunk (std::string & type, std::string & lang, size_t * pidx, double * pstart_time)
{
    size_t idx = 0;
    double start_time = 0.0;
    std::vector<mpd_period_t *> & periods = this->mpd->get_periods ();
    if (periods.size () < 1) {
        return NULL;
    }
    // get start time
    start_time = this->mpd->get_period_starttime (0);
    if (start_time < this->tell()) {
        start_time = this->tell();
    }

    if (this->cur_idx.end() == this->cur_idx.find (type)) {
        // no record for type
        idx = 0;
        this->cur_idx[type] = 0;
    } else {
        idx = this->cur_idx[type];
    }

    if (NULL != pidx) {
        *pidx = idx;
    }
    if (pstart_time) {
        *pstart_time = 0.0;
    }
    // get duration
    double duration_sec = this->mpd->get_segment_duration (type, lang, start_time, this->get_avg_throughput(), idx);
    if (0 != idx) {
        if (duration_sec <= 0.0) {
            // error
            return NULL;
        }
    }

    mpd_chunk_t * chk = ::get_chunk (this->mpd, type, lang, start_time, this->get_avg_throughput(), idx);
    //if (0 != idx) {
        //std::cerr << "start_time=" << start_time << std::endl;
        start_time += duration_sec;
        if (pstart_time) {
            *pstart_time = start_time;
        }
    //}
    //std::cerr << "duration_sec=" << duration_sec << std::endl;
    //std::cerr << "this->cur_time=" << this->tell() << std::endl;
    //std::cerr << "idx=" << idx << std::endl;
    return chk;
}

/**
 * @brief get the next available chunk and advanced one step
 *
 * @param type : "video"/"audio"/"text"("application/ttml+xml")
 * @param lang : language, such as "en"/"de"/"fr"
 *
 * @return the chunk, NULL on failure
 *
 * get the next available chunk and advanced one step, base on the current avg throughput, video type, language etc.
 */
mpd_chunk_t *
mpd_adaptationlogic_t::get_next_chunk (std::string & type, std::string & lang)
{
    mpd_chunk_t * chk;
    size_t idx = 0;
    double start_time = 0.0;
    chk = this->peek_next_chunk (type, lang, &idx, &start_time);
    if (NULL == chk) {
        return NULL;
    }
    if (start_time > 0.0) {
        this->set_cur_time (start_time);
    }
    chk->set_id (idx);
    //chk->set_category(??); // video/audio/???
    this->cur_idx[type] = idx + 1;
    return chk;
}

/**
 * @brief get all of the avaiable mimetypes
 *
 * @return the mimetype list
 *
 * get all of the avaiable mimetypes from current period
 */
std::vector<std::string>
mpd_adaptationlogic_t::get_mimetype_list (void)
{
    std::vector<std::string> bwlst;
    mpd_period_t * period = this->mpd->get_period_from_stime(this->tell());
    if (NULL != period) {
        bwlst = period->get_mimetype_list ();
    }
    return bwlst;
}

/**
 * @brief get all of the avaiable languages
 *
 * @param type : "video"/"audio"/"text"("application/ttml+xml")
 *
 * @return the language list
 *
 * gget all of the avaiable languages, such as "en"/"de"/"fr", from current period
 */
std::vector<std::string>
mpd_adaptationlogic_t::get_language_list (std::string & type)
{
    std::vector<std::string> bwlst;
    mpd_period_t * period = this->mpd->get_period_from_stime(this->tell());
    if (NULL != period) {
        bwlst = period->get_language_list (type);
    }
    return bwlst;
}

/**
 * @brief get all of the avaiable bandwidth
 *
 * @param type : "video"/"audio"/"text"("application/ttml+xml")
 * @param lang : language, such as "en"/"de"/"fr"
 *
 * @return the bandwidth list
 *
 * get all of the avaiable bandwidth from current period
 */
std::vector<size_t>
mpd_adaptationlogic_t::get_bandwith_list (std::string & type, std::string & lang)
{
    std::vector<size_t> bwlst;
    mpd_period_t * period = this->mpd->get_period_from_stime(this->tell());
    if (NULL != period) {
        bwlst = period->get_bandwith_list (type, lang);
    }
    return bwlst;
}

size_t
mpd_adaptationlogic_t::get_current_bitrate (std::string & type, std::string & lang)
{
    //mpd_period_t * period = NULL;
    //mpd_adaptationset_t *adap = NULL;
    mpd_representation_t *rep = NULL;
    if (false == this->mpd->get_period_adapset_rep (type, lang, 0, this->get_avg_throughput(), NULL, NULL, &rep)) {
        return 0;
    }
    return rep->get_bandwidth();
}

void
mpd_adaptationlogic_t::init (void)
{
    double segduration;
    size_t cnt = 0;
    double dcnt;
    size_t ceil_val = 0;
    std::string lang("");
    std::string type("video");

    this->period_stime_list = mpd->get_period_stime_list();
    this->id_list.push_back(0);
    for (int i = 0; i + 1 < this->period_stime_list.size(); i ++) {
        segduration = this->mpd->get_segment_duration (type, lang, this->period_stime_list[i], 0, 1);
        //TD ("segduration=%f; this->period_stime_list[%d]=%f, this->period_stime_list[%d]=%f,", segduration, i, this->period_stime_list[i], i + 1, this->period_stime_list[i + 1]);
        std::cout << "segduration=" << segduration << "; this->period_stime_list[" << i << "]=" << this->period_stime_list[i]
           << ", this->period_stime_list[" << i + 1 << "]=" << this->period_stime_list[i + 1] << std::endl;
        this->segduration_list.push_back(segduration);
        //TD ("segment_list.size=%d", this->segduration_list.size());
        std::cout << "segment_list.size=" << this->segduration_list.size() << std::endl;
        assert (segduration > 0);
        assert (i + 1 < this->period_stime_list.size());
        dcnt = (this->period_stime_list[i + 1] - this->period_stime_list[i]) / segduration;
        ceil_val = (size_t)dcnt;
        if (dcnt > ceil_val) {
            ceil_val ++;
        }
        //TD ("dcnt=%f; ceil_val=%d, cnt=%d", dcnt, (int)ceil_val, (int)cnt);
        std::cout << "dcnt=" << dcnt << ", ceil_val=" << ceil_val << ", cnt=" << cnt << std::endl;
        cnt += 1; /* the init segment*/
        cnt += ceil_val;
        id_list.push_back(cnt);
    }
}

size_t
mpd_adaptationlogic_t::get_total_number_segments (void)
{
    if (id_list.size() < 1) {
        return 0;
    }
    assert (id_list[id_list.size() - 1] >= id_list.size());
    TD ("id_list(%d) = %d, sz=%d", id_list.size() - 1, id_list[id_list.size() - 1], id_list.size());
    return (id_list[id_list.size() - 1] - id_list.size());
}

static int
pf_bsearch_cb_comp_idpos (void *userdata, size_t idx, void * data_pin) /*"data_list[idx] - *data_pin"*/
{
    std::vector<size_t> *p_id_list = (std::vector<size_t> *)userdata;
    size_t * pin = (size_t *)data_pin;
    if ((*p_id_list)[idx] > *pin) {
        return 1;
    } else if ((*p_id_list)[idx] < *pin) {
        return -1;
    }
    return 0;
}

/* get the position in the id_list from id value */
ssize_t
mpd_adaptationlogic_t::search_id_list_position (size_t id)
{
    size_t idx = 0;
    // binary search the position
    if (0 != pf_bsearch_r (&(this->id_list), this->id_list.size(),
                           pf_bsearch_cb_comp_idpos, &id, &idx)) {
        if (idx >= this->id_list.size()) {
            // not in the range
            return -1;
        }
        if (id != this->id_list[idx]) {
            assert (id < this->id_list[idx]);
            assert (idx > 0);
            idx --;
        }
    }
    return idx;
}

double
mpd_adaptationlogic_t::get_segment_duration (size_t id)
{
    // binary find the position
    ssize_t idx = this->search_id_list_position (id);
    if (idx < 0) {
        // error
        return 0;
    }
    assert (id >= this->id_list[idx]);
    if (id == this->id_list[idx]) {
        return 0; // the init segment
    }
    return this->segduration_list[idx];
}

static int
pf_bsearch_cb_comp_period_stime (void *userdata, size_t idx, void * data_pin) /*"data_list[idx] - *data_pin"*/
{
    std::vector<double> *pperiod_stime_list = (std::vector<double> *)userdata;
    double * pin = (double *)data_pin;
    if ((*pperiod_stime_list)[idx] > *pin) {
        return 1;
    } else if ((*pperiod_stime_list)[idx] < *pin) {
        return -1;
    }
    return 0;
}

// convert the second to the actural unique segment id
size_t
mpd_adaptationlogic_t::time2id_abstract (double seconds)
{
    // binary find the position
    size_t id = 0;
    size_t idx;
    if (0 != pf_bsearch_r (&(this->period_stime_list), this->period_stime_list.size(),
                           pf_bsearch_cb_comp_period_stime, &seconds, &idx)) {
        if (idx >= this->period_stime_list.size()) {
            // not in the range
            return 0;
        }
    }
    assert (seconds >= this->period_stime_list[idx]);
    double dfloor = (seconds - this->period_stime_list[idx]) / segduration_list[idx];
    id = (size_t)dfloor;
    id ++;
    id += id_list[idx];
    return id;
}

/**
 * @brief seek to the abstract postion
 *
 * @param pos_sec : position in seconds
 *
 * @return true on success
 *
 * seek to the abstract postion
 */
bool
mpd_adaptationlogic_t::seek (double pos_sec)
{
    // WARNING:
    // assumtion: the init segment will be used as minimal period time
    size_t idx;
    pf_bsearch_r (&(this->period_stime_list), this->period_stime_list.size(),
                           pf_bsearch_cb_comp_period_stime, &pos_sec, &idx);
    if (idx + 1 >= this->period_stime_list.size()) {
        // not in the range
        return false;
    }
    std::map<std::string, size_t>::iterator it2;
    if ((this->get_cur_time() > this->period_stime_list[idx + 1])
        || (this->get_cur_time() < this->period_stime_list[idx])
        ) {
        // not in the same period
        // reset the related index in period
        for (it2 = this->cur_idx.begin(); it2 != this->cur_idx.end(); it2 ++) {
            this->cur_idx.erase(it2);
        }
    } else {
        // update the current related idx
        double dfloor = (pos_sec - this->period_stime_list[idx]) / this->segduration_list[idx];
        size_t idx_rel = (size_t)dfloor;
        for (it2 = this->cur_idx.begin(); it2 != this->cur_idx.end(); it2 ++) {
            it2->second = idx_rel + 1; // 0 is means that a switching of peroid occured. so here start from 1.
        }
    }

    this->set_cur_time(pos_sec);
    return true;
}

bool
mpd_adaptationlogic_t::seek (size_t id)
{
    // WARNING:
    // assumtion: the init segment will be used as minimal period time
    ssize_t idx = this->search_id_list_position (id);
    if (idx < 0) {
        // not in the range
        return false;
    }
    assert (id >= this->id_list[idx]);
    double pos_sec = this->period_stime_list[idx];

    std::map<std::string, size_t>::iterator it2;
    if ((this->get_cur_time() > this->period_stime_list[idx + 1])
        || (this->get_cur_time() < this->period_stime_list[idx])
        ) {
        // not in the same period
        // reset the related index in period
        for (it2 = this->cur_idx.begin(); it2 != this->cur_idx.end(); it2 ++) {
            this->cur_idx.erase(it2);
        }
    } else {
        // update the current related idx
        assert (id >= this->id_list[idx]);
        size_t idx_rel = id - this->id_list[idx];
        for (it2 = this->cur_idx.begin(); it2 != this->cur_idx.end(); it2 ++) {
            it2->second = idx_rel; // 0 is means that a switching of peroid occured. so here start from 1.
        }
    }

    this->set_cur_time(pos_sec);
    return true;
}

void
mpd_adaptationlogic_yhfu_t::notify_avg_throughput (size_t bw) {
    std::cerr << "received avg nofify: " << bw << std::endl;
    if (last_aavg < 1) {
        last_aavg = bw;
    }
    last_aavg = last_aavg * 0.8 + 0.2 * bw;
    //last_aavg = 10000000; // DEBUG
    //last_aavg = 10; // DEBUG
    //last_aavg = bw; // DEBUG
    TD ("alg yhfu use %d as avg throughput!", (int)last_aavg);
    mpd_adaptationlogic_t::notify_avg_throughput(last_aavg);
}

void
mpd_adaptationlogic_yhfu_t::notify_buf_level (double sec_max, double sec_cur, double sec_requested)
{
    buffer_sec_max = sec_max;
    buffer_sec_cur = sec_cur;
    buffer_sec_requested = sec_requested;
}

bool
mpd_adaptationlogic_yhfu_t::is_need_request (void)
{
    // TODO: need to consider the current requested but not received segments
    // TODO: the seconds in the request list of downloader?
    TD ("is_need_request()? sec_req=%f, sec_max=%f, sec_cur=%f", buffer_sec_requested, buffer_sec_max, buffer_sec_cur);
    return (buffer_sec_requested < 10) && (buffer_sec_max > buffer_sec_cur + buffer_sec_requested);
}
