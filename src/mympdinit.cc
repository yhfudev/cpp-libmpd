/**
 * @file    mympdinit.cc
 * @brief   init the DASH MPD file data structures mapped directly from the description of the
 *          ISO/IEC 23009-1:2012 Dynamic adaptive streaming over HTTP (DASH) -- Part 1: Media presentation description and segment formats
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-04-16
 */

#include <stdio.h>
#include <assert.h>

#include <set>
#include <algorithm> // std::sort

#include "mympd.h"

#if USE_VLCLIB
stream_t *g_dbg_stream = NULL;
#endif

static double get_total_duration_from_segcomm (mpd_segmentcommon_t * segcomm);

mpd_segmentcommon_t::~mpd_segmentcommon_t()
{
    size_t i;
    for (i = 0; i < this->BaseURL.size (); i ++) {
        delete this->BaseURL[i];
        this->BaseURL[i] = NULL;
    }
    if (NULL != this->SegmentTemplate) {
        delete this->SegmentTemplate;
    }
    if (NULL != this->SegmentBase) {
        delete this->SegmentBase;
    }
    if (NULL != this->SegmentList) {
        delete this->SegmentList;
    }
}

// get the start time of the segment idx of timeline
size_t
mpd_segmentmultibase_t::get_timeline (size_t idx)
{
    ssize_t time = -1;
    if (NULL != this->get_segtmln()) {
        std::vector<mpd_segmenttimeline_s_t *> & S = this->get_segtmln()->get_s();
        size_t cnt = 0;
        size_t i;
        for (i = 0; i < S.size(); i ++) {
            cnt += S.at(i)->get_r() + 1;
            if (cnt >= idx) {
                cnt -= (S.at(i)->get_r() + 1);
                time = S.at(i)->get_t() + S.at(i)->get_d() * (idx - cnt);
                break;
            }
        }
    }
    return time;
}

mpd_representation_base_t::~mpd_representation_base_t ()
{
    size_t i;
    for (i = 0; i < this->FramePacking.size (); i ++) {
        delete this->FramePacking[i];
        this->FramePacking[i] = NULL;
    }
    for (i = 0; i < this->AudioChannelConfiguration.size (); i ++) {
        delete this->AudioChannelConfiguration[i];
        this->AudioChannelConfiguration[i] = NULL;
    }
    for (i = 0; i < this->ContentProtection.size (); i ++) {
        delete this->ContentProtection[i];
        this->ContentProtection[i] = NULL;
    }
}

mpd_representation_t::mpd_representation_t ()
: bandwidth(0)
{
}

mpd_representation_t::~mpd_representation_t ()
{
    size_t i;
    for (i = 0; i < this->SubRepresentation.size (); i ++) {
        delete this->SubRepresentation[i];
        this->SubRepresentation[i] = NULL;
    }
}

mpd_segmentlist_t::~mpd_segmentlist_t ()
{
    size_t i;
    for (i = 0; i < this->SegmentURL.size (); i ++) {
        delete this->SegmentURL[i];
        this->SegmentURL[i] = NULL;
    }
}

/**
 * @brief constructor of the mpd_segmentbase_t (SegmentBaseType)
 *
 * @return N/A
 *
 * initialize the internal variables
 */
mpd_segmentbase_t::mpd_segmentbase_t ()
: timescale(1)
, presentationTimeOffset(0)
, indexRangeExact(false)
, Initialization(NULL)
, RepresentationIndex(NULL)
{
}

/**
 * @brief destructor of the mpd_segmentbase_t (SegmentBaseType)
 *
 * @return N/A
 *
 * free all of the memory occupied by the internal variables
 */
mpd_segmentbase_t::~mpd_segmentbase_t ()
{
    if (NULL != this->Initialization) {
        delete this->Initialization;
    }
    if (NULL != this->RepresentationIndex) {
        delete this->RepresentationIndex;
    }
}

mpd_segmentmultibase_t::mpd_segmentmultibase_t ()
: duration(-1)
, startNumber(-1)
, SegmentTimeline(NULL)
, BitstreamSwitching(NULL)
{
}

mpd_segmentmultibase_t::~mpd_segmentmultibase_t ()
{
    if (NULL != this->SegmentTimeline) {
        delete this->SegmentTimeline;
    }
    if (NULL != this->BitstreamSwitching) {
        delete this->BitstreamSwitching;
    }
}

double
mpd_segmentmultibase_t::inference_duration (size_t idx)
{
    double timescale = 1.0;
    double duration_sec = -1;
    timescale = this->get_timescale ();
    std::cerr << "from timescale, got timescale=" << timescale << std::endl;
    if (this->get_duration () > 0) {
        duration_sec = this->get_duration () / timescale;
        //TD ("duration_sec=this->get_duration (%f) / timescale(%f) = %f", this->get_duration (), timescale, duration_sec);
        std::cerr << "from duration, got duration=" << this->get_duration () << std::endl;
        std::cerr << "from duration, got duration_sec=" << duration_sec << std::endl;
    } else {
        if (this->get_segtmln()) {
            std::vector<mpd_segmenttimeline_s_t *> & lines = this->get_segtmln()->get_s();
            size_t cnt = 0;
            size_t i;
            for (i = 0; i < lines.size(); i ++) {
                cnt += (lines.at(i)->get_r() + 1);
                if (cnt > idx) {
                    duration_sec = lines.at(i)->get_d() / timescale;
                    //TD ("duration_sec=lines.at(i)->get_d(%d) / timescale(%f) = %f", (int)lines.at(i)->get_d(), timescale, duration_sec);
                    std::cerr << "from segtimeline(" << i << "), got duration_sec=" << duration_sec << std::endl;
                }
            }
        }
    }
    //TD ("return duration_sec=%f", duration_sec);
    std::cerr << "return duration_sec=" << duration_sec << std::endl;
    return duration_sec;
}

mpd_subrepres_t::mpd_subrepres_t ()
: level(-1)
, bandwidth(-1)
{
}

mpd_contentcomponent_t::mpd_contentcomponent_t ()
: id(0)
{
}

mpd_contentcomponent_t::~mpd_contentcomponent_t ()
{
    size_t i;
    for (i = 0; i < this->Accessibility.size (); i ++) {
        delete this->Accessibility[i];
        this->Accessibility[i] = NULL;
    }
    for (i = 0; i < this->Role.size (); i ++) {
        delete this->Role[i];
        this->Role[i] = NULL;
    }
    for (i = 0; i < this->Rating.size (); i ++) {
        delete this->Rating[i];
        this->Rating[i] = NULL;
    }
    for (i = 0; i < this->Viewpoint.size (); i ++) {
        delete this->Viewpoint[i];
        this->Viewpoint[i] = NULL;
    }
}

mpd_adaptationset_t::mpd_adaptationset_t ()
: id(-1)
, subsegmentAlignment(false)
, subsegmentStartsWithSAP(0)
, bitstreamSwitching(false)
, group (-1)
, minBandwidth(0), maxBandwidth(0), minWidth(0), maxWidth(0), minHeight(0), maxHeight(0)
, segmentAlignment(false)
{
}

mpd_adaptationset_t::~mpd_adaptationset_t ()
{
    size_t i;
    for (i = 0; i < this->Representation.size (); i ++) {
        delete this->Representation[i];
        this->Representation[i] = NULL;
    }
    for (i = 0; i < this->Accessibility.size (); i ++) {
        delete this->Accessibility[i];
        this->Accessibility[i] = NULL;
    }
    for (i = 0; i < this->Role.size (); i ++) {
        delete this->Role[i];
        this->Role[i] = NULL;
    }
    for (i = 0; i < this->Rating.size (); i ++) {
        delete this->Rating[i];
        this->Rating[i] = NULL;
    }
    for (i = 0; i < this->Viewpoint.size (); i ++) {
        delete this->Viewpoint[i];
        this->Viewpoint[i] = NULL;
    }
    for (i = 0; i < this->ContentComponent.size (); i ++) {
        delete this->ContentComponent[i];
        this->ContentComponent[i] = NULL;
    }
}

bool
mpd_adaptationset_t::is_match_lang (std::string & lang)
{
    if (lang.size() < 1 || lang == "und") {
        return true;
    }
    if (std::string::npos != this->get_lang().find (lang)) {
        return true;
    }
    return false;
}

bool
mpd_adaptationset_t::is_match0 (std::string & type, std::string & lang)
{
    if (! this->is_match_type (type)) {
        return false;
    }
    return this->is_match_lang(lang);
}

/**
 * @brief check if the adaptation set match the media type
 *
 * @param type : the media type, such as "video", "audio", "text" etc.
 *
 * @return true on match, otherwise false
 *
 * check if the adaptation set match the media type
 */
bool
mpd_adaptationset_t::is_match_type (std::string & type)
{
    bool flg_type = false;
    if (type.size() < 1 || type == "und") {
        flg_type = true;
    }
    if (! flg_type) {
        if (this->get_cnttype().size() > 0) {
            if (std::string::npos != this->get_cnttype().find (type)) {
                flg_type = true;
            }
        }
    }
    if (! flg_type) {
        if (this->get_mimetype().size() > 0) {
            if (std::string::npos != this->get_mimetype().find (type)) {
                flg_type = true;
            }
        }
    }
    size_t i;
    if (! flg_type) {
        for (i = 0; i < this->get_cntcomponent().size(); i ++) {
            if (this->get_cntcomponent().at(i)->get_cnttype().size() > 0) {
                if (std::string::npos != this->get_cntcomponent().at(i)->get_cnttype().find (type)) {
                    flg_type = true;
                }
            }
        }
    }
    if (! flg_type) {
        for (i = 0; i < this->get_represent().size(); i ++) {
            if (this->get_represent().at(i)->get_mimetype().size() > 0) {
                if (std::string::npos != this->get_represent().at(i)->get_mimetype().find (type)) {
                    flg_type = true;
                }
            }
        }
    }
    if (! flg_type) {
        if ((0 == type.compare("audio")) && (this->get_audconf().size() > 0)) {
            flg_type = true;
        }
    }

    return flg_type;
}

struct sort_by_bandwidth {
    inline bool operator() (const mpd_representation_t * s1, const mpd_representation_t * s2) {
        return s1->get_bandwidth () < s2->get_bandwidth();
    }
};
bool
mpd_adaptationset_t::post_parser (void)
{
    std::sort (Representation.begin(), Representation.end(), sort_by_bandwidth());
    mpd_representation_base_t::post_parser();
    if (this->get_mimetype().size() < 1) {
        // try to get the type from Representation
        size_t i;
        std::string type;
        for (i = 0; i < this->Representation.size (); i ++) {
            if (type.size() < 1) {
                type = this->Representation[i]->get_mimetype();
            } else if (this->Representation[i]->get_mimetype() != type) {
                type = "";
                break;
            }
        }
        if (type.size() > 0) {
            this->set_mimetype(type);
        }
    }
    return true;
}

static bool
less_than_represent_bandwidth (mpd_representation_t * s1, mpd_representation_t * s2)
{
    assert (NULL != s1);
    assert (NULL != s2);
    return s1->get_bandwidth() < s2->get_bandwidth();
}

static int
pf_bsearch_cb_comp_repbw (void *userdata, size_t idx, void * data_pin) /*"data_list[idx] - *data_pin"*/
{
    std::vector<mpd_representation_t *> * d = (std::vector<mpd_representation_t *> *)userdata;
    mpd_representation_t * p = (mpd_representation_t *)data_pin;
    assert (NULL != data_pin);
    assert (NULL != (*d)[idx]);
    if ((*d)[idx]->get_bandwidth() > p->get_bandwidth()) {
        return 1;
    } else if ((*d)[idx]->get_bandwidth() < p->get_bandwidth()) {
        return -1;
    }
    return 0;
}

mpd_representation_t *
mpd_adaptationset_t::get_match_represent (size_t bandwidth)
{
    std::vector<mpd_representation_t *> & repset = this->get_represent();
    if (repset.size() < 1) {
        return NULL;
    }
#if 0
    size_t i;
    size_t min1 = 0;
    size_t max1 = repset.size() - 1;
    for (; max1 > min1 + 1;) {
        i = (max1 + min1) / 2;
        if (repset[i]->get_bandwidth() == bandwidth) {
            return repset[i];
        } if (repset[i]->get_bandwidth() > bandwidth) {
            max1 = i;
        } else {
            min1 = i;
        }
    }
    assert (max1 == min1 || max1 == min1 + 1);
    if (max1 == min1) {
        return repset[min1];
    } else if (repset[max1]->get_bandwidth() == bandwidth) {
        return repset[max1];
    } else {
        return repset[min1];
    }
    return rep;
#elif 1

    //std::vector<mpd_representation_t *> & repset = this->get_represent();
    mpd_representation_t rp;
    rp.set_bandwidth(bandwidth);
    if (repset.size() < 1) {
        return NULL;
    }
    size_t idx = 0;
    if (0 == pf_bsearch_r (&repset, repset.size(), pf_bsearch_cb_comp_repbw, &rp, &idx)) {
        // found
        return repset[idx];
    }
    if (idx < repset.size()) {
        return repset[idx];
    }
    return repset[repset.size() - 1];
#else
    mpd_representation_t * rep = NULL;
    mpd_representation_t mp;
    mp.set_bandwidth(bandwidth);
    //std::vector<mpd_representation_t *>::iterator upper = std::upper_bound(repset.begin(), repset.end(), &mp, less_than_represent_bandwidth);
    std::vector<mpd_representation_t *>::iterator upper = std::lower_bound(repset.begin(), repset.end(), &mp, less_than_represent_bandwidth);
    if (repset.end() == upper) {
        // not found
        rep = repset.at(repset.size () - 1);
    } else {
        rep = *upper;
    }
    return rep;
#endif
}

mpd_period_t::mpd_period_t ()
: bitstreamSwitching(false)
{
}

mpd_period_t::~mpd_period_t ()
{
    size_t i;
    for (i = 0; i < this->adaptationSets.size (); i ++) {
        delete this->adaptationSets[i];
        this->adaptationSets[i] = NULL;
    }
    for (i = 0; i < this->Subset.size (); i ++) {
        delete this->Subset[i];
        this->Subset[i] = NULL;
    }
}

bool
mpd_period_t::post_parser (void) {
    size_t i;
    size_t j;
    if (this->get_duration().size() < 1) {
        // find the duration
        double duration = get_total_duration_from_segcomm (this);
        if ((0 == duration) && (this->get_adaptset().size() > 0)) {
            std::vector<mpd_adaptationset_t *> & adplst = this->get_adaptset();
            for (i = 0; i < adplst.size(); i ++) {
                double d = get_total_duration_from_segcomm (adplst[i]);
                if (0 == d) {
                    std::vector<mpd_representation_t *> & replst = adplst[i]->get_represent();
                    for (j = 0; j < replst.size(); j ++) {
                        d = get_total_duration_from_segcomm (replst[j]);
                        if (d > duration) {
                            duration = d;
                        }
                    }
                } else if (d > duration) {
                    duration = d;
                }
            }
        }
        this->set_duration(duration);
    }
    for (i = 0; i < this->adaptationSets.size(); i ++) {
        this->adaptationSets.at(i)->post_parser();
    }
    return true;
}

/**
 * @brief get the next matched Adaptation Set from position idx_start
 *
 * @param period : the period
 * @param idx_start : the start position for searching
 * @param type : "video"/"audio"/"text"("application/ttml+xml")
 * @param lang : language, such as "en"/"de"/"fr"
 *
 * @return the matched position
 *
 * if not found, return the size of the Adaptation Set
 */
size_t
mpd_period_t::get_next_match_adapset_pos (size_t idx_start, std::string & type, std::string & lang)
{
    std::vector<mpd_adaptationset_t *> & adpset = this->get_adaptset ();
    std::vector<mpd_adaptationset_t *>::size_type i, last;
    bool flg_found_type = false;
    for (i = idx_start; i < adpset.size(); i ++) {
        if (adpset.at(i)->is_match_type (type)) {
            last = i;
            flg_found_type = true;
            if (adpset.at(i)->is_match_lang (lang)) {
                return i;
            }
        }
    }
    if (flg_found_type) {
        return last;
    }
    return i;
}

/**
 * @brief get all of the avaiable mimetypes
 *
 * @return the mimetype list, "video"/"audio"/"text"("application/ttml+xml")
 *
 * get all of the avaiable mimetypes from current period
 */
std::vector<std::string>
mpd_period_t::get_mimetype_list (void)
{
    std::set<std::string> all;
    std::vector<std::string> bwlst;

    std::vector<mpd_adaptationset_t *> & adpset = this->get_adaptset();
    size_t i;
    size_t j;
    for (i = 0; i < adpset.size(); i ++) {
        // get mimetype
        if (adpset.at(i)->get_mimetype().size() > 0) {
            all.insert(adpset.at(i)->get_mimetype());
        }
        std::vector<mpd_representation_t *> & reps = adpset.at(i)->get_represent();
        for (j = 0; j < reps.size(); j ++) {
            if (reps.at(j)->get_mimetype().size() > 0) {
                all.insert(reps.at(j)->get_mimetype());
            }
        }
    }
    for (std::set<std::string>::iterator it = all.begin(); it != all.end(); ++ it) {
        bwlst.push_back (*it);
    }
    return bwlst;
}

/**
 * @brief get all of the avaiable languages
 *
 * @param type : "video", "audio", etc.
 *
 * @return the mimetype list, such as "en"/"de"/"fr"
 *
 * get all of the avaiable languages from current period
 */
std::vector<std::string>
mpd_period_t::get_language_list (std::string & type)
{
    std::set<std::string> all;
    std::vector<std::string> bwlst;
    std::string lang("");

    std::vector<mpd_adaptationset_t *> & adpset = this->get_adaptset();
    size_t i;
    for (i = 0; i < adpset.size(); i ++) {
        // get languages
        if (adpset.at(i)->is_match_type (type)) {
            if (adpset.at(i)->get_lang().size() > 0) {
                all.insert(adpset.at(i)->get_lang());
            }
        }
    }
    for (std::set<std::string>::iterator it = all.begin(); it != all.end(); ++ it) {
        bwlst.push_back (*it);
    }
    if (bwlst.size () < 1) {
        bwlst.push_back(lang);
    }
    return bwlst;
}

/**
 * @brief get all of the avaiable bandwidth
 *
 * @param type : "video"/"audio"/"text"("application/ttml+xml")
 * @param lang : language, such as "en"/"de"/"fr"
 *
 * @return the bandwidth list
 *
 * get all of the avaiable bandwidth from the period
 */
std::vector<size_t>
mpd_period_t::get_bandwith_list (std::string & type, std::string & lang)
{
    std::vector<size_t> bwlst;
    std::vector<mpd_adaptationset_t *> & adpset = this->get_adaptset();
    size_t i;
    size_t j;
    for (i = 0; i < adpset.size(); i ++) {
        if (
            ((type.size() < 1) || ((type.size() > 0) && (
                    (adpset.at(i)->get_cnttype().find (type) == 0)
                    || (adpset.at(i)->get_mimetype().find (type) == 0)
                                                         )))
            && ((lang.size() < 1) || ((lang.size() > 0) && (
                    (adpset.at(i)->get_lang().size () < 1)
                    || (lang == "und")
                    || (adpset.at(i)->get_lang() == "und")
                    || (adpset.at(i)->get_lang() == lang)
                                                            )))
            ) {
            // get representation
            std::vector<mpd_representation_t *> & reps = adpset.at(i)->get_represent();
            for (j = 0; j < reps.size(); j ++) {
                bwlst.push_back(reps.at(j)->get_bandwidth());
            }
        }
    }
    if (bwlst.size() < 1) {
        return bwlst;
    }
    std::sort (bwlst.begin(), bwlst.end());
    std::vector<size_t>::iterator it1 = bwlst.begin();
    std::vector<size_t>::iterator it2;
    for (it2 = it1 + 1; it2 != bwlst.end(); it2 ++) {
        if (*it1 == *it2) {
            bwlst.erase(it2);
            it2 = it1;
        } else {
            it1 ++;
        }
    }
    return bwlst;
}

mpd_metrics_t::~mpd_metrics_t ()
{
    size_t i;
    for (i = 0; i < this->Reporting.size(); i ++) {
        delete this->Reporting[i];
        this->Reporting[i] = NULL;
    }
    for (i = 0; i < this->Range.size(); i ++) {
        delete this->Range[i];
        this->Range[i] = NULL;
    }
}

mpd_t::mpd_t (std::string &default_url)
: url_default(default_url)
, type (mpd_t::PSTATIC)
, flg_live (false)
{
}

mpd_t::~mpd_t ()
{
    size_t i;
    for (i = 0; i < this->Periods.size (); i ++) {
        delete this->Periods[i];
        this->Periods[i] = NULL;
    }
    for (i = 0; i < this->BaseURL.size (); i ++) {
        delete this->BaseURL[i];
        this->BaseURL[i] = NULL;
    }
    for (i = 0; i < this->ProgramInformation.size (); i ++) {
        delete this->ProgramInformation[i];
        this->ProgramInformation[i] = NULL;
    }
    for (i = 0; i < this->Location.size (); i ++) {
        this->Location[i].resize(0);
        //this->Location[i].shrink_to_fit();
    }
    for (i = 0; i < this->Metrics.size (); i ++) {
        delete this->Metrics[i];
        this->Metrics[i] = NULL;
    }
}

const std::string &
mpd_t::get_id () const
{
    if (id.size () < 1) {
        return url_default;
    }
    return id;
}

struct sort_by_start {
    inline bool operator() (const mpd_period_t * s1, const mpd_period_t * s2) {
        return (double)s1->get_start_value () < s2->get_start_value ();
    }
};

/**
 * @brief check and set the start/duration value
 *
 * @return false on error
 *
 * check and set the start/duration value; if there's no 'start'/'duration' then set the start.
 */
bool
mpd_t::check_set_period (void)
{
    size_t i;
    std::vector<mpd_period_t *> & periods = this->get_periods ();
    if (periods.size () < 1) {
        return false;
    }
    double pres_duration = this->get_pres_duration_value ();
    double cur_time = 0.0;
    size_t cnt_d = 0;
    size_t cnt_s = 0;
    for (i = 0; i < periods.size(); i ++) {
        if (periods.at(i)->get_start().size () > 0) {
            // has start value
            cnt_s ++;
        }
        if (periods.at(i)->get_duration().size () > 0 && periods.at(i)->get_duration_value() != 0) {
            // has start value
            cnt_d ++;
        }
    }
    if (cnt_d >= periods.size()) {
        // set the start time
        cur_time = 0.0;
        for (i = 0; i < periods.size(); i ++) {
            periods.at(i)->set_start (cur_time);
            cur_time += periods.at(i)->get_duration_value ();
        }
        return true;

    } else if (0 == cnt_d) {
        if (cnt_s < periods.size()) {
            if (1 == periods.size()) {
                periods.at(0)->set_start (0.0);
                periods.at(0)->set_duration (pres_duration);
            } else {
                // no enough info to build the start/duration
                return false;
            }
        } else {
            // sort the period by the start time
            std::sort (periods.begin(), periods.end(), sort_by_start());
            // set all of the durations: use the code below:
        }
    }
    // try to set start/duration
    cur_time = 0.0;
    for (i = 0; i < periods.size(); i ++) {
        if (periods.at(i)->get_start().size() <= 0) {
            if (periods.at(i)->get_duration().size() <= 0) {
                if ((i + 1 >= periods.size()) && (cur_time < pres_duration) ) {
                    periods.at(i)->set_duration (pres_duration - cur_time);
                } else {
                    // error
                    return false;
                }
            }
            periods.at(0)->set_start (cur_time);
        } else {
            if (cur_time > periods.at(i)->get_start_value ()) {
                // error
                return false;
            }
            if (periods.at(i)->get_duration().size() <= 0) {
                if (i + 1 < periods.size()) {
                    if (cur_time > periods.at(i)->get_start_value ()) {
                        // error
                        return false;
                    } else {
                        periods.at(i)->set_duration (periods.at(i)->get_duration_value () - cur_time);
                    }
                } else {
                    if (cur_time < pres_duration) {
                        periods.at(i)->set_duration (pres_duration - cur_time);
                    } else {
                        // error
                        return false;
                    }
                }
            }
        }
        periods.at(i)->set_start (cur_time);
        cur_time += periods.at(i)->get_duration_value ();
    }
    return true;
}

/**
 * @brief get the start time of the idx'th period
 *
 * @param idx : the period index
 *
 * @return the start time of the period in seconds
 *
 * get the start time of the idx'th period
 */
double
mpd_t::get_period_starttime (size_t idx)
{
    std::vector<mpd_period_t *> & periods = this->get_periods ();
    if (periods.size () < 1) {
        return -1;
    }
    if (idx >= periods.size ()) {
        return -1;
    }
    if (periods.at(idx)->get_start().size() <= 0) {
        if (! this->check_set_period ()) {
            return -1;
        }
    }
    return periods.at(idx)->get_start_value ();
}

static bool
less_than_period_stime (mpd_period_t * s1, mpd_period_t * s2)
{
    assert (NULL != s1); assert (NULL != s2);
    if (s1->get_duration_value() == 0) {
        if (s2->get_duration_value() == 0) {
            return s1->get_start_value() < s2->get_start_value();
        }
        if ((s2->get_start_value() <= s1->get_start_value()) && (s1->get_start_value() <= s2->get_start_value() + s2->get_duration_value())) {
            return true;
        }
        return false;
    }
    if ((s1->get_start_value() <= s2->get_start_value()) && (s2->get_start_value() <= s1->get_start_value() + s1->get_duration_value())) {
        return true;
    }
    return false;
}

static int
pf_bsearch_cb_comp_period_stime (void *userdata, size_t idx, void * data_pin) /*"data_list[idx] - *data_pin"*/
{
    std::vector<mpd_period_t*> * d = (std::vector<mpd_period_t*> *)userdata;
    mpd_period_t * p = (mpd_period_t *)data_pin;
    assert (NULL != data_pin);
    assert (NULL != (*d)[idx]);
    if ((*d)[idx]->get_start_value() > p->get_start_value()) {
        return 1;
    } else if ((*d)[idx]->get_start_value() + (*d)[idx]->get_duration_value() < p->get_start_value()) {
        return -1;
    }
    if (idx + 1 < d->size()) {
        if ((*d)[idx + 1]->get_start_value() == p->get_start_value()) {
            return -1;
        }
    }
    return 0;
}

/**
 * @brief get the period from the time
 *
 * @param stime : the period time in seconds
 *
 * @return the matched period
 *
 * get the period from the time, the time should be in the range of period.start and period.duration
 */
mpd_period_t *
mpd_t::get_period_from_stime (double stime)
{
    if ((this->get_pres_duration_value () > 0) && (stime > this->get_pres_duration_value ())) {
        return NULL;
    }

    mpd_period_t mp;
    mp.set_start(stime);
    mp.set_duration(0.0);
#if 1
    std::vector<mpd_period_t*> & plist = this->get_periods();
    size_t idx;
    if (0 == pf_bsearch_r (&plist, plist.size(), pf_bsearch_cb_comp_period_stime, &mp, &idx)) {
        // found
        return plist[idx];
    }
    return NULL;
#else
    std::vector<mpd_period_t*>::iterator upper = std::upper_bound(this->get_periods().begin(), this->get_periods().end(), &mp, less_than_period_stime);
    //std::vector<mpd_period_t*>::iterator upper = std::lower_bound(this->get_periods().begin(), this->get_periods().end(), &mp, less_than_period_stime);
    if (this->get_periods().end() == upper) {
        // not found
        return NULL;
    }
    return *upper;
#endif
}

bool
mpd_t::post_parser (void) {
#if USE_COMPATIBILITY
    // setup some unset variables after parse_current() for USE_COMPATIBILITY
    if (mpd_t::PERROR == this->type) {
        this->type = mpd_t::PSTATIC;
    }
#endif // USE_COMPATIBILITY
    size_t i;
    for (i = 0; i < this->get_periods().size(); i ++) {
        this->get_periods().at(i)->post_parser();
    }
    return check_set_period ();
}

// get the pointers of mpd_period_t/mpd_adaptationset_t/mpd_representation_t
bool
mpd_t::get_period_adapset_rep (std::string & type, std::string & lang, double start_time, size_t avg_throughput, mpd_period_t ** pperiod, mpd_adaptationset_t ** padap, mpd_representation_t ** prep)
{
    size_t i;

    mpd_period_t * period = this->get_period_from_stime(start_time);
    if (NULL == period) {
        return false;
    }

    i = period->get_next_match_adapset_pos (0, type, lang);
    if (i >= period->get_adaptset().size()) {
        // not found!
        return false;
    }
    mpd_adaptationset_t *adap = period->get_adaptset().at(i);
    assert (NULL != adap);
    if (NULL == adap) {
        return false;
    }

    mpd_representation_t *rep = adap->get_match_represent (avg_throughput);
    assert (NULL != rep);
    if (NULL == rep) {
        return false;
    }
    if (NULL != pperiod) {
        *pperiod = period;
    }
    if (NULL != padap) {
        *padap = adap;
    }
    if (NULL != prep) {
        *prep = rep;
    }
    return true;
}

double
mpd_t::get_segment_duration (std::string & type, std::string & lang, double start_time, size_t avg_throughput, size_t idx)
{
    mpd_period_t * period = NULL;
    mpd_adaptationset_t *adap = NULL;
    mpd_representation_t *rep = NULL;
    if (false == this->get_period_adapset_rep (type, lang, start_time, avg_throughput, &period, &adap, &rep)) {
        std::string type2 = "audio";
        if (false == this->get_period_adapset_rep (type2, lang, start_time, avg_throughput, &period, &adap, &rep)) {
            return 0.0;
        }
    }
    return ::get_segment_duration (idx, period, adap, rep);
}

// utils

/**
 * @brief get the start time of all periods
 *
 * @return the start time list, the last one is the end of the time of the periods
 *
 * get the start time of all periods
 */
std::vector<double>
mpd_t::get_period_stime_list (void)
{
    std::vector<double> bwlst;
    double last = 0;
    size_t i;
    for (i = 0; i < this->get_periods_num(); i ++) {
        last = this->get_period_starttime(i);
        bwlst.push_back(last);
    }
    mpd_period_t * peri = this->get_period_from_stime (last);
    if (NULL != peri) {
        bwlst.push_back(last + peri->get_duration_value());
    }
    return bwlst;
}

static double
get_total_duration_from_multibase (mpd_segmentmultibase_t *msmulti)
{
    double timescale = 1.0;
    double duration_sec = 0.0;
    timescale = msmulti->get_timescale ();
//std::cerr << "from timescale, got timescale=" << timescale << std::endl;
    if (msmulti->get_segtmln()) {
        std::vector<mpd_segmenttimeline_s_t *> & lines = msmulti->get_segtmln()->get_s();
        size_t i;
        for (i = 0; i < lines.size(); i ++) {
            duration_sec += lines.at(i)->get_d() * (lines.at(i)->get_r() + 1) / timescale;
        }
    } else if (msmulti->get_duration () > 0) {
        duration_sec = msmulti->get_duration () / timescale;
//std::cerr << "from duration, got duration=" << msmulti->get_duration () << std::endl;
//std::cerr << "from duration, got duration_sec=" << duration_sec << std::endl;
    }
    return duration_sec;
}

static double
get_total_duration_from_segcomm (mpd_segmentcommon_t * segcomm)
{
    double duration = 0;
    if (NULL != segcomm->get_seglist()) {
        if (segcomm->get_seglist()->get_segurl().size() > 0) {
            duration = get_total_duration_from_multibase (segcomm->get_seglist()) * segcomm->get_seglist()->get_segurl().size();
        } else {
            duration = get_total_duration_from_multibase (segcomm->get_seglist());
        }
    } else if (NULL != segcomm->get_segtemplate()) {
        duration = get_total_duration_from_multibase (segcomm->get_segtemplate());
    }
    return duration;
}

// get the duration time of one media segment
double
get_segment_duration (size_t idx, mpd_period_t * period, mpd_adaptationset_t * adap, mpd_representation_t * rep)
{
    // get duration
    double duration_sec = 0.0;
    if (0 != idx) {
        if (NULL != rep->get_seglist()) {
            duration_sec = rep->get_seglist()->inference_duration(idx);
//std::cerr << "from rep-seglist, got duration_sec=" << duration_sec << std::endl;
        } else if (NULL != rep->get_segtemplate()) {
            duration_sec = rep->get_segtemplate()->inference_duration(idx);
//std::cerr << "from rep-segtemp, got duration_sec=" << duration_sec << std::endl;
        } else {
            // search parent: AdaptationSet
            if (NULL != adap->get_seglist()) {
                duration_sec = adap->get_seglist()->inference_duration(idx);
//std::cerr << "from adap-seglist, got duration_sec=" << duration_sec << std::endl;
            } else if (NULL != adap->get_segtemplate()) {
                duration_sec = adap->get_segtemplate()->inference_duration(idx);
//std::cerr << "from adap-segtemp, got duration_sec=" << duration_sec << std::endl;
            } else {
                // search parent: period
                if (NULL != period->get_seglist()) {
                    duration_sec = period->get_seglist()->inference_duration(idx);
//std::cerr << "from period-seglist, got duration_sec=" << duration_sec << std::endl;
                } else if (NULL != period->get_segtemplate()) {
                    duration_sec = period->get_segtemplate()->inference_duration(idx);
//std::cerr << "from period-segtemp, got duration_sec=" << duration_sec << std::endl;
                } else if (period->get_duration().size() > 0) {
                    duration_sec = period->get_duration_value();
                //} else if (this->mpd->get_min_buf_time().size() > 0) {
                //    duration_sec = this->mpd->get_min_buf_time_value();
                } else {
                    // error, not found
                    std::cerr << "Not found duration!" << std::endl;
                    return 0.0;
                }
            }
        }
    }
    return duration_sec;
}
