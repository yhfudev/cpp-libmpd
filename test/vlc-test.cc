/**
 * @file    vlc-test.cc
 * @brief   VLC test functions
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-10-10
 */
/*****************************************************************************
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#include <sys/types.h> // ssize_t
#include <sys/time.h> // gettimeofday()
#include <stdint.h> // uint8_t
#include <stdlib.h> // malloc
#include <string.h> // memmove
#include <stdio.h> // printf
#include <unistd.h> // sleep
#include <errno.h>

#include <assert.h>

#include <vector>
#include <deque>

#include "vlc-api.h"
#include "vlc-downloader.h"
#include "vlc-test.h"
/*
void
test_vlc_read (stream_t *sp)
{
    int ret;
    uint64_t pos = 0;
    uint64_t i;
    ssize_t rt;
    size_t rd;
    size_t pos_rd = 0;
    char buf[101];
    uint8_t *bufp = NULL;
    char lastch = 0;
    char flg_is_
    TI ("data sz=%d", (int)pos);
    for (i = 0; (pos == 0) || (i < pos);) {
        rd = sizeof (buf) - 1;
        if (i + rd > pos) {
            rd = pos - i;
        }
        rt = stream_Peek (sp, (const uint8_t **)&bufp, rd);
        rt = stream_Read (sp, buf + pos_rd, rd);
        TI ("Received data sz=%d", (int)rt);
        TI("read sz=%d, max=%d", (int)i, (int)pos);
        buf[sizeof (buf) - 1] = 0;
        TI("Content of data:\n%s", buf);
        ret = stream_Control (sp, STREAM_GET_POSITION, &pos);
        CHKVLC_RETURN (ret);
        TI ("next pos=%d", (int)pos);
        if (rt > 0) {
            if (i > 0) {
                // check last char
                if (! NUM2CH_ADJECENT(lastch, buf[0])) {
                    TI("content error: lastch=%c, buf[last]=%c", lastch, buf[0]);
                    assert (0);
                }
            }
            lastch = buf [rt + pos_rd - 1];

            i += rt;
        }

        // delay:
        //sleep (1);
        // get the new size:
        ret = stream_Control (sp, STREAM_GET_SIZE, &pos);
        CHKVLC_RETURN (ret);
        TI("new stream size=%d", (int)pos);
    }

}
*/

void
test_vlc_dash (void)
{
    int ret;
    uint64_t pos = 0;
    bool cannt = false;
    stream_t *sphttp = NULL;
    stream_t *sp = NULL;

    sphttp = stream_UrlNew (NULL, CSTR_MPD_URL);
    if (NULL == sphttp) {
        TE ("Something wrong in stub function stream_UrlNew(http)\n");
        return;
    }
    sp = stream_UrlNew (sphttp, "fake://localhost/dash/bbb-1080p-dash-sorenson/short20s.mpd");
    if (NULL == sp) {
        stream_Delete (sphttp);
        TE ("Something wrong in stub function stream_UrlNew(fake)\n");
        return;
    }
    ret = stream_module_open (sp);
    CHKVLC_RETURN (ret);
    assert (NULL != sp->pf_read);
    assert (NULL != sp->pf_peek);
    assert (NULL != sp->pf_control);

    ret = stream_Control (sp, STREAM_CAN_FASTSEEK, &cannt);
    CHKVLC_RETURN (ret);
    ret = stream_Control (sp, STREAM_CAN_SEEK, &cannt);
    CHKVLC_RETURN (ret);
    ret = stream_Control (sp, STREAM_CAN_CONTROL_PACE, &cannt);
    CHKVLC_RETURN (ret);
    ret = stream_Control (sp, STREAM_CAN_PAUSE, &cannt);
    CHKVLC_RETURN (ret);
    ret = stream_Control (sp, STREAM_GET_POSITION, &pos);
    CHKVLC_RETURN (ret);
    TI("read position=%d", (int)pos);
    ret = stream_Control (sp, STREAM_SET_POSITION, pos);
    CHKVLC_RETURN (ret);
    TI("set position=%d", (int)pos);

    ret = stream_Control (sp, STREAM_GET_SIZE, &pos);
    CHKVLC_RETURN (ret);
    TI("stream size=%d", (int)pos);

    uint64_t i;
    ssize_t rt;
    size_t rd;
    char buf[101];
    uint8_t *bufp = NULL;
    char lastch = 0;
    TI ("data sz=%d", (int)pos);
    for (i = 0; (pos == 0) || (i < pos);) {
        rd = sizeof (buf) - 1;
        if (rd > pos - i) {
            rd = pos - i;
        }
        rt = stream_Peek (sp, (const uint8_t **)&bufp, rd);
        rt = stream_Read (sp, buf, rd);
        TI ("Received data sz=%d", (int)rt);
        ret = stream_Control (sp, STREAM_GET_POSITION, &pos);
        CHKVLC_RETURN (ret);
        TI ("next pos=%d", (int)pos);
        if (rt > 0) {
            if (i > 0) {
                // check last char
                if (! NUM2CH_ADJECENT(lastch, buf[0])) {
                    TI("content error: lastch=%c, buf[last]=%c", lastch, buf[0]);
                    assert (0);
                }
            }
            lastch = buf [rt - 1];

            i += rt;
        }

        TI("read sz=%d, max=%d", (int)i, (int)pos);
        buf[sizeof (buf) - 1] = 0;
        TI("Content of data:\n%s", buf);
        // delay:
        //sleep (1);
        // get the new size:
        ret = stream_Control (sp, STREAM_GET_SIZE, &pos);
        CHKVLC_RETURN (ret);
        TI("new stream size=%d", (int)pos);
    }

    TI("sleep 50 ...");
    sleep (15);
    stream_module_close (sp);
    stream_Delete (sphttp);
    stream_Delete (sp);
}

void
test_structures0 (void)
{
    std::vector<std::deque<vdash_downsegment_t *> > buffer;

    buffer.resize(2);
    buffer[0].push_back (NULL);
    buffer[0].push_back (NULL);
    std::cout << "size of buffer[0]=" << buffer[0].size() << std::endl;
    std::cout << "size of buffer[1]=" << buffer[1].size() << std::endl;
}

void
test_structures1 (void)
{
    std::vector<std::map<int, vdash_downsegment_t *> > buffer;

    buffer.resize(2);
    buffer[0][1] = NULL;
    buffer[0][2] = NULL;
    buffer[0].erase (1);
    std::cout << "size of buffer[0]=" << buffer[0].size() << std::endl;
    std::cout << "size of buffer[1]=" << buffer[1].size() << std::endl;
}

void
test_trim (void)
{
    std::string a("  \n \r This is the content! \n\r");
    std::cout << "'" << a << "' --> '";
    trim(a);
    std::cout << a << "'" << std::endl;
}
void
test_structures (void)
{
    int idx;
    vdash_downsegment_t *seg;
    std::map<int, vdash_downsegment_t *> maptest;

    idx = 3; seg = new vdash_downsegment_mem_t(); seg->set_id(idx); maptest[idx] = seg;
    idx = 5; seg = new vdash_downsegment_mem_t(); seg->set_id(idx); maptest[idx] = seg;
    idx = 1; seg = new vdash_downsegment_mem_t(); seg->set_id(idx); maptest[idx] = seg;
    idx = 7; seg = new vdash_downsegment_mem_t(); seg->set_id(idx); maptest[idx] = seg;
    idx = 4; seg = new vdash_downsegment_mem_t(); seg->set_id(idx); maptest[idx] = seg;
    for (std::map<int, vdash_downsegment_t *>::iterator it = maptest.begin(); it != maptest.end(); it ++) {
        std::cout << it->first << std::endl;
    }
    std::map<int, vdash_downsegment_t *>::iterator it2 = maptest.find(3);
    for (; it2 != maptest.end(); it2 ++) {
        //std::cout << "find item at pos=" << (it2 - maptest.begin()) << std::endl;
        std::cout << "find item = " << it2->first << std::endl;
    }
}

int
main (void)
{
    test_trim();

    stream_module_init ();

    test_structures();
    test_vlc_dash ();
    return 0;
}
