/**
 * @file    vlc-test.h
 * @brief   VLC test functions
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-10-10
 */
/*****************************************************************************
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifndef _VLC_TESTSTUB_H
#define _VLC_TESTSTUB_H

#define CHKVLC_RETURN(ret0) \
    switch (ret0) { \
    case VLC_SUCCESS: TE ("return success!"); break; \
    case VLC_EGENERIC: TE ("return error generic!"); break; \
    case VLC_ENOMEM: TE ("return error no memory!"); break; \
    }

#define CSTR_MPD_URL "http://localhost/dash/bbb-1080p-dash-sorenson/short20s.mpd"

#define NUM2CH_ASCII_LEN 21 /*between 1 - 26 */
#define NUM2CH_SIZE (10 + NUM2CH_ASCII_LEN * 2)
#define NUM2CH_SIZE_MID (10 + NUM2CH_ASCII_LEN)
//#define NUM2CH(val) ((((val) % 60) < 10)?(((val) % 60) + '0'):((((val) % 60) < 35)?(((val) % 60) - 10 + 'A'):(((val) % 60) - 35 + 'a')))
#define NUM2CH(val) ((((val) % NUM2CH_SIZE) < 10)?(((val) % NUM2CH_SIZE) + '0'):((((val) % NUM2CH_SIZE) < NUM2CH_SIZE_MID)?(((val) % NUM2CH_SIZE) - 10 + 'A'):(((val) % NUM2CH_SIZE) - NUM2CH_SIZE_MID + 'a')))

#define NUM2CH_ADJECENT(val1, val2) (((val1) + 1 == (val2) ) ? 1: (((val1) == '9') && ((val2) == 'A')) || (((val1) == ('A' + NUM2CH_ASCII_LEN - 1)) && ((val2) == 'a')) || (((val1) == ('a' + NUM2CH_ASCII_LEN - 1)) && ((val2) == '0')))

/* 1 -- use string checkpoints for the content in buffer */
#define USE_BUFQUEUE_CHECKPOINTS 1

#endif // _VLC_TESTSTUB_H
