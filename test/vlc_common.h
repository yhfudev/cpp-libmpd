/**
 * @file    vlc_common_vlctest.h
 * @brief   VLC stub functions for DASH module
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-10-10
 */
/*****************************************************************************
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifndef _VLC_TEST_COMMON_H
#define _VLC_TEST_COMMON_H

#include <stdarg.h>
#include <sys/types.h> // ssize_t
#include <stdint.h> // uint8_t
#include <pthread.h>

#ifdef TE
#undef TE
#endif
#ifndef TE
#if 0 // #if LIBDASH_USE_DASH_DOWNLOAD
#  define TE(fmt, ...) msg_Err(NULL, fmt, ##__VA_ARGS__)
#  define TI(fmt, ...) msg_Info(NULL, fmt, ##__VA_ARGS__)
#  define TW(fmt, ...) msg_Dbg(NULL, fmt, ##__VA_ARGS__)
#  define TD(fmt, ...) msg_Dbg(NULL, fmt, ##__VA_ARGS__)
#else
#  ifdef DEBUG
#    define TE(fmt, ...) fprintf(stderr, "[tm=%f][tid=%d][%s()]\t" fmt "\t{%d," __FILE__ "}\n", ((double)mdate() / 1000000), (int)vlc_threadid(), __func__, ##__VA_ARGS__, __LINE__)
#    define TW TE
#    define TI TE
#    define TD TE
#  else
#    define TE(fmt, ...) fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#    define TW TE
#    define TI(fmt, ...)
#    define TD TI
#  endif
#endif
#endif

#ifdef __cplusplus
# define VLC_EXTERN extern "C"
#else
# define VLC_EXTERN
#endif

#ifndef __cplusplus
# include <stdbool.h>
#endif

#define N_(a) a

#define msg_Info(s, fmt, ...) fprintf(stderr, "[%s()]\t" fmt "\t{%d," __FILE__ "}\n", __func__, ##__VA_ARGS__, __LINE__)
#define msg_Err msg_Info
#define msg_Dbg msg_Info

#ifdef __GNUC__
#   define likely(p)   __builtin_expect(!!(p), 1)
#   define unlikely(p) __builtin_expect(!!(p), 0)
#else
#   define likely(p)   (!!(p))
#   define unlikely(p) (!!(p))
#endif

#define VLC_SUCCESS   0
#define VLC_EGENERIC -1
#define VLC_ENOMEM   -2

#include <vlc_stream.h>

#endif // _VLC_TEST_COMMON_H
