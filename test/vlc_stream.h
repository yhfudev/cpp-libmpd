/**
 * @file    vlc_common_vlctest.h
 * @brief   VLC stub functions for DASH module
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-10-10
 */
/*****************************************************************************
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifndef _VLC_TEST_STREAM_H
#define _VLC_TEST_STREAM_H

#include <stdarg.h>
#include <sys/types.h> // ssize_t
#include <stdint.h> // uint8_t

struct _vlc_stream_t;
#define stream_t struct _vlc_stream_t
typedef int (* vlctest_plugin_read_t) (stream_t *p_stream, void *p_ptr, unsigned int i_len);
typedef int (* vlctest_plugin_peek_t) (stream_t *p_stream, const uint8_t ** p_ptr, unsigned int i_len);
typedef int (* vlctest_plugin_control_t) (stream_t *p_stream, int i_query, va_list args);
#undef stream_t

typedef struct _vlc_stream_t {
    struct _vlc_stream_t * p_source;
    char * psz_access;
    char * psz_path;
    void * p_sys;
    vlctest_plugin_read_t pf_read; // function read
    vlctest_plugin_peek_t pf_peek; // function peek
    vlctest_plugin_control_t pf_control; // function control
} stream_t;

stream_t * stream_UrlNew (stream_t *s, const char *url);
void stream_Delete (stream_t *s);
ssize_t stream_Read (stream_t *s, void * buf, size_t size);
ssize_t stream_Peek (stream_t *s, const uint8_t ** buf, size_t size);
ssize_t stream_Size (stream_t *s);
int stream_Seek ( stream_t *s, uint64_t i_pos );
int stream_Control (stream_t *sp, int i_query, ... );

#endif // _VLC_TEST_STREAM_H
