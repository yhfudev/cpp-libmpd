/**
 * @file    vlc_plugin_vlctest.h
 * @brief   VLC stub functions for DASH module
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-10-10
 */
/*****************************************************************************
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifndef _VLC_TEST_PLUGIN_H
#define _VLC_TEST_PLUGIN_H

#define vlc_object_t void

enum _vlc_stream_value_t {
    STREAM_CAN_SEEK = 1,
    STREAM_CAN_FASTSEEK,
    STREAM_CAN_PAUSE,
    STREAM_CAN_CONTROL_PACE,
    STREAM_GET_POSITION,
    STREAM_SET_POSITION,
    STREAM_GET_SIZE,
    STREAM_SET_PAUSE_STATE,
};

typedef int  (* vlc_module_cb_open_t) (vlc_object_t *p_obj);
typedef void (* vlc_module_cb_close_t) (vlc_object_t *p_obj);

typedef struct _vlctest_module_t {
    char *name;
    vlc_module_cb_open_t pf_open;
    vlc_module_cb_close_t pf_close;
} vlctest_module_t;

extern vlctest_module_t g_vlctest_module;
int vlc_module_entry (void);
#define vlc_module_begin() \
    vlctest_module_t g_vlctest_module; \
    int vlc_module_entry () { \
        memset (&g_vlctest_module, 0, sizeof (g_vlctest_module));
#define set_category(a)
#define set_subcategory(a)
#define set_description(a)
#define set_shortname(a) g_vlctest_module.name = a,
#define add_shortcut(a)
#define set_capability(a,b)
#define set_callbacks(open0, close0) g_vlctest_module.pf_open = open0; g_vlctest_module.pf_close = close0;
#define add_integer(a,b,c,d,e)
#define vlc_module_end() return 0; }

// test:
#define stream_module_init() vlc_module_entry()
#define stream_module_open(sp0) g_vlctest_module.pf_open (sp0)
#define stream_module_close(sp0) g_vlctest_module.pf_close (sp0)

#endif // _VLC_TEST_PLUGIN_H
