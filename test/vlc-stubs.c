/**
 * @file    vlc-stubs.c
 * @brief   VLC stubs functions
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-10-10
 */
/*****************************************************************************
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#include <sys/types.h> // ssize_t
#include <sys/time.h> // gettimeofday()
#include <stdint.h> // uint8_t
#include <stdlib.h> // malloc
#include <string.h> // memmove
#include <stdio.h> // printf
#include <errno.h>

#include <assert.h>

#include "vlc-api.h"
#include "vlc-test.h"

#define SCSTR_LEN(str) (sizeof(str) - 1)

static char g_fake_http_contents_soren[] = "<?xml version='1.0' encoding='utf-8'?> \
<MPD xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' \
    xmlns='urn:mpeg:DASH:schema:MPD:2011' \
    xsi:schemaLocation='urn:mpeg:DASH:schema:MPD:2011' \
    mediaPresentationDuration='PT572.625S' \
    minBufferTime='PT2.00S' \
    profiles='urn:mpeg:dash:profiles:isoff-main:2011'> \
  <BaseURL>/</BaseURL> \
  <Period start='PT0.00S' duration='PT572.625S'> \
    <BaseURL></BaseURL> \
    <AdaptationSet mimeType='video/mp4' codecs='avc1.42E01f' subsegmentAlignment='true'>\
      <SegmentTemplate duration='6' startNumber='0' \
          media='$RepresentationID$/seg_$Number%05$.mp4' initialisation='$RepresentationID$/seg_init.mp4' /> \
      <Representation id='video-110Kbps' bandwidth='110000' width='512' height='288'></Representation> \
      <Representation id='video-230Kbps' bandwidth='230000' width='512' height='288'></Representation> \
      <Representation id='video-480Kbps' bandwidth='480000' width='512' height='288'></Representation> \
      <Representation id='video-990Kbps' bandwidth='990000' width='640' height='360'></Representation> \
      <Representation id='video-1800Kbps' bandwidth='1800000' width='852' height='480'></Representation> \
    </AdaptationSet> \
  </Period> \
</MPD>";

static char g_fake_http_contents_soren_short[] = "<?xml version='1.0' encoding='utf-8'?> \
<MPD xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' \
    xmlns='urn:mpeg:DASH:schema:MPD:2011' \
    xsi:schemaLocation='urn:mpeg:DASH:schema:MPD:2011' \
    mediaPresentationDuration='PT10.625S' \
    minBufferTime='PT2.00S' \
    profiles='urn:mpeg:dash:profiles:isoff-main:2011'> \
  <BaseURL>/</BaseURL> \
  <Period start='PT0.00S' duration='PT10.625S'> \
    <BaseURL></BaseURL> \
    <AdaptationSet mimeType='video/mp4' codecs='avc1.42E01f' subsegmentAlignment='true'>\
      <SegmentTemplate duration='6' startNumber='0' \
          media='$RepresentationID$/seg_$Number%05$.mp4' initialisation='$RepresentationID$/seg_init.mp4' /> \
      <Representation id='video-110Kbps' bandwidth='110000' width='512' height='288'></Representation> \
      <Representation id='video-230Kbps' bandwidth='230000' width='512' height='288'></Representation> \
      <Representation id='video-480Kbps' bandwidth='480000' width='512' height='288'></Representation> \
      <Representation id='video-990Kbps' bandwidth='990000' width='640' height='360'></Representation> \
      <Representation id='video-1800Kbps' bandwidth='1800000' width='852' height='480'></Representation> \
    </AdaptationSet> \
  </Period> \
</MPD>";

static char g_fake_http_contents_live[] = "<?xml version='1.0' encoding='UTF-8'?> \
<MPD \
  xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' \
  xmlns='urn:mpeg:DASH:schema:MPD:2011' \
  xsi:schemaLocation='urn:mpeg:DASH:schema:MPD:2011 DASH-MPD.xsd' \
  type='dynamic' \
  minimumUpdatePeriod='PT2S' \
  timeShiftBufferDepth='PT30M' \
  availabilityStartTime='2011-12-25T12:30:00' \
  minBufferTime='PT4S' \
  profiles='urn:mpeg:dash:profile:isoff-live:2011'> \
  <BaseURL>http://cdn1.example.com/</BaseURL> \
  <BaseURL>http://cdn2.example.com/</BaseURL> \
  <Period> \
    <!-- Video --> \
    <AdaptationSet \
      mimeType='video/mp4' \
      codecs='avc1.4D401F' \
      frameRate='30000/1001' \
      segmentAlignment='true' \
      startWithSAP='1'> \
      <BaseURL>video/</BaseURL> \
      <SegmentTemplate timescale='90000' initialization='$Bandwidth$/init.mp4v' media='$Bandwidth$/$Time$.mp4v'> \
        <SegmentTimeline> \
          <S t='0' d='180180' r='432'/> \
        </SegmentTimeline> \
      </SegmentTemplate> \
      <Representation id='v0' width='320' height='240' bandwidth='250000'/> \
      <Representation id='v1' width='640' height='480' bandwidth='500000'/> \
      <Representation id='v2' width='960' height='720' bandwidth='1000000'/> \
    </AdaptationSet> \
    <!-- English Audio --> \
    <AdaptationSet mimeType='audio/mp4' codecs='mp4a.0x40' lang='en' segmentAlignment='0' startWithSAP='1'> \
      <SegmentTemplate timescale='48000' initialization='audio/en/init.mp4a' media='audio/en/$Time$.mp4a'> \
        <SegmentTimeline> \
          <S t='0' d='96000' r='432'/> \
        </SegmentTimeline> \
      </SegmentTemplate> \
      <Representation id='a0' bandwidth='64000' /> \
    </AdaptationSet> \
    <!-- French Audio --> \
    <AdaptationSet mimeType='audio/mp4' codecs='mp4a.0x40' lang='fr' segmentAlignment='0' startWithSAP='1'> \
      <SegmentTemplate timescale='48000' initialization='audio/fr/init.mp4a' media='audio/fr/$Time$.mp4a'> \
        <SegmentTimeline> \
          <S t='0' d='96000' r='432'/> \
        </SegmentTimeline> \
      </SegmentTemplate> \
      <Representation id='a0' bandwidth='64000' /> \
    </AdaptationSet> \
  </Period> \
</MPD>";

#define g_fake_http_contents g_fake_http_contents_soren

typedef struct _vlctest_http_t {
    size_t position;
    int fake_value;
    size_t size;
} vlctest_http_t;

static char g_fake_content_buffer[sizeof (g_fake_http_contents) + 10];

static int
vlctest_http_peek0 (stream_t *p_stream, void *p_ptr, unsigned int i_len)
{
    size_t i;
    vlctest_http_t *p_sys = (vlctest_http_t *) p_stream->p_sys;

    if (p_sys->fake_value) {
        if (0) {
            memset (p_ptr, p_sys->fake_value, i_len);
        } else {
            size_t pos = 0;
            char *p = p_ptr;
#if USE_BUFQUEUE_CHECKPOINTS
            // the check point
            g_fake_content_buffer[0] = NUM2CH(p_sys->position);
            g_fake_content_buffer[1] = 0;
            sprintf (g_fake_content_buffer + 1, "(%d)", (int)(p_sys->position));
            pos = strlen (g_fake_content_buffer);
            if (pos > i_len) {
                pos = 0;
            } else {
                memmove (p_ptr, g_fake_content_buffer, pos);
            }
#endif
            p = (char *)p_ptr + pos;
            i = pos;
            pos += p_sys->position;
            for (; i < i_len; i ++) {
                *p = NUM2CH(pos);
                p ++;
                pos ++;
            }
            //p_ptr[i_len] = 0;
        }
    } else {
        size_t position1 = p_sys->position;
        assert (p_sys->size <= SCSTR_LEN (g_fake_http_contents));
        if (position1 > p_sys->size) {
            i_len = 0;
        } else if (position1 + i_len > SCSTR_LEN (g_fake_http_contents)) {
            i_len = SCSTR_LEN (g_fake_http_contents) - position1;
        }
        if (i_len > 0) {
            memmove (p_ptr, g_fake_http_contents + position1, i_len);
        }
    }
    return i_len;
}

static int
vlctest_http_peek (stream_t *p_stream, const uint8_t ** pp_peek0, unsigned int i_len)
{
    int ret = -1;
    char ** pp_peek = (char **) pp_peek0;
    if (NULL == pp_peek) {
        return VLC_EGENERIC;
    }
    if (i_len < 1) {
        *pp_peek = NULL;
        return 0;
    }
    vlctest_http_t *p_sys = (vlctest_http_t *) p_stream->p_sys;

    if (p_sys->fake_value) {
        if (i_len > sizeof (g_fake_content_buffer)) {
            i_len = sizeof (g_fake_content_buffer);
        }
        ret = vlctest_http_peek0 (p_stream, g_fake_content_buffer, i_len);
        *pp_peek = g_fake_content_buffer;
    } else {
        size_t position1 = p_sys->position;
        assert (p_sys->size <= SCSTR_LEN (g_fake_http_contents));
        if (position1 > p_sys->size) {
            i_len = 0;
        } else if (position1 + i_len > SCSTR_LEN (g_fake_http_contents)) {
            i_len = SCSTR_LEN (g_fake_http_contents) - position1;
        }
        *pp_peek = g_fake_http_contents + position1;
        ret = i_len;
    }

    return ret;
}

static int
vlctest_http_read (stream_t *p_stream, void *p_ptr, unsigned int i_len)
{
    vlctest_http_t * p_sys = (vlctest_http_t *)p_stream->p_sys;
    i_len = vlctest_http_peek0 (p_stream, p_ptr, i_len);
    p_sys->position += i_len;
    return i_len;
}

static int
vlctest_http_control (stream_t *p_stream, int i_query, va_list args)
{
    vlctest_http_t *p_sys = (vlctest_http_t *)p_stream->p_sys;

    switch (i_query) {
        case STREAM_CAN_SEEK:
            /*TODO Support Seek */
#if LIBDASH_USE_DASH_DOWNLOAD
            msg_Dbg( p_stream, "Control get can seek = false\n");
            *(va_arg (args, bool *)) = false;
#else
            msg_Dbg( p_stream, "Control get can seek = true\n");
            *(va_arg (args, bool *)) = true;
#endif
            break;

        case STREAM_CAN_FASTSEEK:
            msg_Dbg( p_stream, "Control get can fast seek = false\n");
            *(va_arg (args, bool *)) = false;
            break;

#if ! LIBDASH_USE_208
        case STREAM_CAN_PAUSE:
        case STREAM_CAN_CONTROL_PACE:
            *(va_arg (args, bool *)) = false; /* TODO */
            break;
#endif
        case STREAM_GET_POSITION:
            *(va_arg (args, uint64_t *)) = p_sys->position;
            break;
        case STREAM_SET_POSITION:
        {
            uint64_t pos = (uint64_t)va_arg(args, uint64_t);
            p_sys->position = pos;
        }
            break;
        case STREAM_GET_SIZE:
        {
            uint64_t*   res = (va_arg (args, uint64_t *));
            *res = p_sys->size;
        }
            break;
        default:
            return VLC_EGENERIC;
    }
    return VLC_SUCCESS;
}

stream_t *
stream_UrlNew (stream_t *s0, const char *url)
{
    stream_t *sp = NULL;

    sp = (stream_t *)malloc (sizeof (*sp));
    if (NULL == sp) {
        return NULL;
    }
    memset (sp, 0, sizeof (*sp));
    sp->p_source = s0;
    if (0 == strncmp (url, "http://", 7)) {
        sp->psz_access = "http";
        sp->psz_path = strdup (url + 7);

        sp->pf_read = vlctest_http_read;
        sp->pf_peek = vlctest_http_peek;
        sp->pf_control = vlctest_http_control;

        // for debug test
        vlctest_http_t * psys = (vlctest_http_t *)malloc (sizeof (*psys));
        sp->p_sys = psys;
        psys->position = 0;
        psys->fake_value = 0;
        psys->size = SCSTR_LEN (g_fake_http_contents);

#if 1
        if (strcmp (CSTR_MPD_URL, url)) {
            psys->fake_value = mdate () % 96 + 32;
            psys->size = (rand () % 500000) / NUM2CH_SIZE * NUM2CH_SIZE;
        }
#endif
    } else if (0 == strncmp (url, "fake://", 7)) {
        sp->psz_access = "http";
        sp->psz_path = strdup (url + 7);
    }
    return sp;
}

void
stream_Delete (stream_t *sp)
{
    if (NULL != sp->psz_path) {
        free (sp->psz_path);
    }
    free (sp);
}

ssize_t
stream_Peek (stream_t *sp, const uint8_t ** buf, size_t size)
{
    assert (NULL != sp->pf_read);
    assert (NULL != sp->pf_peek);
    assert (NULL != sp->pf_control);
    return sp->pf_peek (sp, buf, size);
}

ssize_t
stream_Read (stream_t *sp, void * buf, size_t size)
{
    assert (NULL != sp->pf_read);
    assert (NULL != sp->pf_peek);
    assert (NULL != sp->pf_control);
    return sp->pf_read (sp, buf, size);
}

int
stream_Seek ( stream_t *s, uint64_t i_pos )
{
    return stream_Control( s, STREAM_SET_POSITION, i_pos );
}

int
stream_Control (stream_t *sp, int i_query, ... )
{
    va_list argptr;
    va_start (argptr, i_query);
    int ret;
    assert (NULL != sp->pf_read);
    assert (NULL != sp->pf_peek);
    assert (NULL != sp->pf_control);
    ret = sp->pf_control (sp, i_query, argptr);
    va_end (argptr);
    return ret;
}

ssize_t
stream_Size (stream_t *sp)
{
    int ret;
    uint64_t pos = 0;
    ret = stream_Control (sp, STREAM_GET_SIZE, &pos);
    CHKVLC_RETURN (ret);
    if (VLC_SUCCESS != ret) {
        return 0;
    }
    return pos;
}

char *
FromCharset (const char * encoding, char * peeked, size_t size)
{
    return peeked;
}

mtime_t
mdate (void)
{
    struct timeval tv;
    if (unlikely(gettimeofday (&tv, NULL) != 0))
        abort ();
    return (INT64_C(1000000) * tv.tv_sec) + tv.tv_usec;
}

static struct timespec
mtime_to_ts (mtime_t date)
{
    lldiv_t d = lldiv (date, CLOCK_FREQ);
    struct timespec ts = { d.quot, d.rem * (1000000000 / CLOCK_FREQ) };

    return ts;
}

int
vlc_cond_timedwait (vlc_cond_t *p_condvar, vlc_mutex_t *p_mutex0, mtime_t deadline)
{
    struct timespec ts = mtime_to_ts (deadline);
#if USE_MUTEX_COUNT_DEBUG
    pthread_mutex_t * p_mutex = &(p_mutex0->mut);
    if (! (p_mutex0)->cntlck) { TE ("free bug? owner='fn=%s;ln=%d;func=%s'", (p_mutex0)->file, (p_mutex0)->line, (p_mutex0)->func); }
    assert ((p_mutex0)->cntlck == 1);
    (p_mutex0)->cntlck --;
#else
    pthread_mutex_t * p_mutex = p_mutex0;
#endif // USE_MUTEX_COUNT_DEBUG
    int res = pthread_cond_timedwait (p_condvar, p_mutex, &ts);
    switch (res) {
    case 0:
    case ETIMEDOUT:
#if USE_MUTEX_COUNT_DEBUG
        TE ("timeout limit reached!");
        if ((p_mutex0)->cntlck) { TE ("dead lock? owner='fn=%s;ln=%d'", (p_mutex0)->file, (p_mutex0)->line); }
        assert ((p_mutex0)->cntlck == 0);
        (p_mutex0)->cntlck ++;
        (p_mutex0)->func = __func__;
        (p_mutex0)->file = __FILE__;
        (p_mutex0)->line = __LINE__;
#endif
        break;
    default:
    case EINVAL: // lock is not locked so we can just return
        return res;
    }
    return res;
}

unsigned long
vlc_threadid (void)
{
     union { pthread_t th; unsigned long int i; } v = { };
     v.th = pthread_self ();
     return v.i;
}
