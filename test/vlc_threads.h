/**
 * @file    vlc_threads_vlctest.h
 * @brief   VLC stub functions for DASH module
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-10-10
 */
/*****************************************************************************
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifndef _VLC_TEST_THREADS_H
#define _VLC_TEST_THREADS_H

#include <stdarg.h>
#include <sys/types.h> // ssize_t
#include <stdint.h> // uint8_t
#include <pthread.h>

#define CLOCK_FREQ INT64_C(1000000)
typedef int64_t mtime_t;
mtime_t mdate (void);

#if DEBUG
#define USE_MUTEX_COUNT_DEBUG 1
#endif

#if USE_MUTEX_COUNT_DEBUG
typedef struct _vlc_stub_mutex_t {
    pthread_mutex_t mut;
    size_t cntlck; /*!< the times of locked */
    /* debug info */
    const char * func; /*!< owner's function */
    const char * file; /*!< owner's file */
    int line; /*!< owner's line */
} vlc_mutex_t;

//typedef struct _vlc_stub_cond_t {
//    pthread_cond_t cond;
//    size_t cntlck; /*!< the times of locked */
//} vlc_cond_t;
#define vlc_cond_t         pthread_cond_t

#define vlc_mutex_init(a)    \
    TI("mutex init (" # a ") ..."); \
    memset (a, 0, sizeof(*a)); \
    pthread_mutex_init(&((a)->mut), NULL); \
    TI("END of mutex init (" # a ")")

#define vlc_mutex_destroy(a) \
    TI("mutex destory (" # a ") ..."); \
    if ((a)->cntlck) { \
        TE ("exist thread in critical section? owner='fn=%s;ln=%d;func=%s'", (a)->file, (a)->line, (a)->func); \
    } \
    pthread_mutex_destroy(&((a)->mut)); \
    TI("END of mutex destory (" # a ")")

#define vlc_mutex_lock(a)    \
    pthread_mutex_lock(&((a)->mut)); \
    if ((a)->cntlck) { TE ("dead lock? owner='fn=%s;ln=%d;func=%s'", (a)->file, (a)->line, (a)->func); } \
    assert ((a)->cntlck == 0); \
    (a)->cntlck ++; \
    (a)->file = __FILE__; \
    (a)->line = __LINE__; \
    (a)->func = __func__

#define vlc_mutex_unlock(a)  \
    if (! (a)->cntlck) { \
        TE ("free bug? owner='fn=%s;ln=%d;func=%s'", (a)->file, (a)->line, (a)->func); \
    } \
    assert ((a)->cntlck == 1); \
    (a)->cntlck --; \
    (a)->file = __FILE__; \
    (a)->line = __LINE__; \
    (a)->func = __func__; \
    pthread_mutex_unlock(&((a)->mut))

#define vlc_cond_init(a)     \
    TI("cond init (" # a ") ..."); \
    pthread_cond_init((a), NULL); \
    TI("END of cond init (" # a ")")

#define vlc_cond_destroy(a)  \
    TI("cond destory (" # a ") ..."); \
    pthread_cond_destroy(a); \
    TI("END of cond destory (" # a ")")

#define vlc_cond_signal(a)   \
    TI("cond signal (" # a ") ..."); \
    pthread_cond_signal(a); \
    TI("END of cond signal (" # a ")")

#define vlc_cond_wait(aaa,a)  { \
    int res; \
    TI("cond wait (" # aaa ", " # a ") ..."); \
    if (! (a)->cntlck) { \
        TE ("free bug? owner='fn=%s;ln=%d;func=%s'", (a)->file, (a)->line, (a)->func); \
    } \
    assert ((a)->cntlck == 1); \
    (a)->cntlck --; \
    (a)->file = __FILE__; \
    (a)->line = __LINE__; \
    (a)->func = __func__; \
    res = pthread_cond_wait((aaa),&((a)->mut)); \
    switch (res) { \
    case 0: \
        TE ("timeout limit reached!"); \
        if ((a)->cntlck) { TE ("dead lock? owner='fn=%s;ln=%d'", (a)->file, (a)->line); } \
        assert ((a)->cntlck == 0); \
        (a)->cntlck ++; \
        (a)->func = __func__; \
        (a)->file = __FILE__; \
        (a)->line = __LINE__; \
        break; \
    } \
    TI("END of cond wait (" # aaa ", " # a ")"); \
}

#elif 0 // DEBUG
#define vlc_mutex_t        pthread_mutex_t
#define vlc_cond_t         pthread_cond_t

#define vlc_mutex_init(a)    TI("mutex init (" # a ") ...");    pthread_mutex_init((a), NULL); TI("END of mutex init (" # a ")");
#define vlc_mutex_destroy(a) TI("mutex destory (" # a ") ..."); pthread_mutex_destroy(a);   TI("END of mutex destory (" # a ")")
//#define vlc_mutex_lock(a)    TI("mutex lock (" # a ") ...");    pthread_mutex_lock(a);      TI("END of mutex lock (" # a ")")
//#define vlc_mutex_unlock(a)  TI("mutex unlock (" # a ") ...");  pthread_mutex_unlock(a);    TI("END of mutex unlock (" # a ")")
#define vlc_mutex_lock(a)    pthread_mutex_lock(a)
#define vlc_mutex_unlock(a)  pthread_mutex_unlock(a)

#define vlc_cond_init(a)     TI("cond init (" # a ") ...");     pthread_cond_init((a), NULL); TI("END of cond init (" # a ")")
#define vlc_cond_destroy(a)  TI("cond destory (" # a ") ...");  pthread_cond_destroy(a);    TI("END of cond destory (" # a ")")
#define vlc_cond_signal(a)   TI("cond signal (" # a ") ...");   pthread_cond_signal(a);     TI("END of cond signal (" # a ")")
#define vlc_cond_wait(a,b)   TI("cond wait (" # a ", " # b ") ..."); pthread_cond_wait((a),(b)); TI("END of cond wait (" # a ", " # b ")")

#else
#define vlc_mutex_t        pthread_mutex_t
#define vlc_cond_t         pthread_cond_t

#define vlc_mutex_init(a)    pthread_mutex_init((a), NULL)
#define vlc_mutex_destroy(a) pthread_mutex_destroy(a)
#define vlc_mutex_lock(a)    pthread_mutex_lock(a)
#define vlc_mutex_unlock(a)  pthread_mutex_unlock(a)

#define vlc_cond_init(a)     pthread_cond_init((a), NULL)
#define vlc_cond_destroy(a)  pthread_cond_destroy(a)
#define vlc_cond_signal(a)   pthread_cond_signal(a)
#define vlc_cond_wait(a,b)   pthread_cond_wait((a),(b))

#endif

unsigned long vlc_threadid (void);

//#define vlc_cond_timedwait pthread_cond_timedwait
int vlc_cond_timedwait (vlc_cond_t *p_condvar, vlc_mutex_t *p_mutex, mtime_t deadline);

#define vlc_thread_t      pthread_t
#define vlc_clone(thhd, func, usrdata, priority) pthread_create((thhd), NULL, (func), (usrdata))
#define vlc_join(thhd, retval) pthread_join((thhd), (retval))

#endif // _VLC_TEST_THREADS_H
