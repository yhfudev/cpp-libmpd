/**
 * @file    mpdverifier.cc
 * @brief   MPD verifier
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-05-01
 */
#include <stdio.h>
#include <assert.h>

#include <iostream>
#include <stdlib.h>
#include <getopt.h>

#include "mympdutils.h"
#include "mympd.h"
#include "mympdalg.h"

#define VER_MAJOR 0
#define VER_MINOR 2
#define VER_MOD   0

//#define SLP() usleep(1000000)
#define SLP()

#define NUM_ARRAY(a) (sizeof(a)/sizeof((a)[0]))

int
test_xduration (void)
{
    std::string str;
    str = "-P5Y2M10D";      std::cout << "string '" << str << "' to seconds: " << xsduration_to_seconds (str) << std::endl;
    str = "-PT15H";         std::cout << "string '" << str << "' to seconds: " << xsduration_to_seconds (str) << std::endl;
    str = "P5Y2M10DT15H";   std::cout << "string '" << str << "' to seconds: " << xsduration_to_seconds (str) << std::endl;
    str = "PT15H34M21.11S"; std::cout << "string '" << str << "' to seconds: " << xsduration_to_seconds (str) << std::endl;
    str = "PT572.625S";   std::cout << "string '" << str << "' to seconds: " << xsduration_to_seconds (str) << std::endl;
    str = "PT0.00S";      std::cout << "string '" << str << "' to seconds: " << xsduration_to_seconds (str) << std::endl;
    str = "PT2.00S";      std::cout << "string '" << str << "' to seconds: " << xsduration_to_seconds (str) << std::endl;
    str = "PT0H0M52.01S"; std::cout << "string '" << str << "' to seconds: " << xsduration_to_seconds (str) << std::endl;
    str = "PT0S";         std::cout << "string '" << str << "' to seconds: " << xsduration_to_seconds (str) << std::endl;
    str = "PT1.5S";       std::cout << "string '" << str << "' to seconds: " << xsduration_to_seconds (str) << std::endl;
    str = "PT3256S";      std::cout << "string '" << str << "' to seconds: " << xsduration_to_seconds (str) << std::endl;
    str = "PT1.2S";       std::cout << "string '" << str << "' to seconds: " << xsduration_to_seconds (str) << std::endl;
    str = "PT10.625S";    std::cout << "string '" << str << "' to seconds: " << xsduration_to_seconds (str) << std::endl;

    std::cout << std::endl; str = "2002-05-30T09:30:10.5-06:00"; std::cout << "string '" << str << "'" << std::endl; xsdatetime_to_timet(str);
    std::cout << std::endl; str = "2002-05-30T09:30:10.5+06:00"; std::cout << "string '" << str << "'" << std::endl; xsdatetime_to_timet(str);
    std::cout << std::endl; str = "2002-05-30T09:30:10.5Z"; std::cout << "string '" << str << "'" << std::endl; xsdatetime_to_timet(str);
    std::cout << std::endl; str = "2002-05-30+06:00"; std::cout << "string '" << str << "'" << std::endl; xsdatetime_to_timet(str);
    std::cout << std::endl; str = "2002-05-30-06:00"; std::cout << "string '" << str << "'" << std::endl; xsdatetime_to_timet(str);
    std::cout << std::endl; str = "2002-05-30Z"; std::cout << "string '" << str << "'" << std::endl; xsdatetime_to_timet(str);
    std::cout << std::endl; str = "2002-05-30"; std::cout << "string '" << str << "'" << std::endl; xsdatetime_to_timet(str);
    std::cout << std::endl; str = "09:30:10.5+06:00"; std::cout << "string '" << str << "'" << std::endl; xsdatetime_to_timet(str);
    std::cout << std::endl; str = "09:30:10.5-06:00"; std::cout << "string '" << str << "'" << std::endl; xsdatetime_to_timet(str);
    std::cout << std::endl; str = "09:30:10.5Z"; std::cout << "string '" << str << "'" << std::endl; xsdatetime_to_timet(str);
    std::cout << std::endl; str = "09:30:10.5"; std::cout << "string '" << str << "'" << std::endl; xsdatetime_to_timet(str);

    return 0;
}

int
test_split (void)
{
    int i;
    const char * items_origin[] = {
        // Basic
        "urn:mpeg:mpegB:profile:dash:isoff-basic-on-demand:cm",
        "urn:mpeg:dash:profile:isoff-ondemand:2011",
        "urn:mpeg:dash:profile:isoff-on-demand:2011", // 8.3
        "urn:mpeg:dash:profile:isoff-live:2011", // 8.4
        // isoff-main
        "urn:mpeg:dash:profile:isoff-main:2011", // 8.5
        "urn:mpeg:dash:profiles:isoff-main:2011", // 8.5 typo
        "urn:mpeg:dash:profile:full:2011", // 8.2

        "urn:mpeg:dash:profile:mp2t-main:2011", // 8.6
        "urn:mpeg:dash:profile:mp2t-simple:2011", // 8.7
        "urn:webm:dash:profile:webm-on-demand:2012", //WebM DASH js
    };
    std::vector<std::string> items;
    std::string str(items_origin[0]);
    assert (NUM_ARRAY(items_origin) >= 1);
    for (i = 1; i < NUM_ARRAY(items_origin); i ++) {
        str += ",";
        str += items_origin[i];
    }
    size_t num;
    std::cout << "origin string:" << str << std::endl;
    num = str_split<std::string> (items, str, ", ");
    std::cout << num << "(" << items.size() << ") items:" << std::endl;
    if (num != NUM_ARRAY(items_origin)) {
        std::cerr << "Error: number of content is not equal: original=" << NUM_ARRAY(items_origin) << "; processed=" << num << std::endl;
    }
    for (i = 0; i < items.size(); i ++) {
        std::cout << "items[" << i << "]=" << items[i] << std::endl;
        if (0 != items[i].compare (items_origin[i])) {
            std::cerr << "Error: item at (" << i << ") not equal: original=" << items_origin[i] << "; processed=" << items[i] << std::endl;
        }
    }

    size_t items2_origin[] = {3158, 3929, 59, 3492, 391, 835, 3984};
    std::vector<size_t> items2;
    std::ostringstream s;
    for (i = 0; i < NUM_ARRAY(items2_origin); i ++) {
        s << " " << items2_origin[i];
    }
    std::string str2 = s.str();
    std::cout << "origin string:" << str2 << std::endl;
    num = val_split<size_t, std::string> (items2, str2, ", ");
    std::cout << num << "(" << items2.size() << ") items2:" << std::endl;
    if (num != NUM_ARRAY(items2_origin)) {
        std::cerr << "Error: number of content is not equal: original=" << NUM_ARRAY(items2_origin) << "; processed=" << num << std::endl;
    }
    for (int i = 0; i < items2.size(); i ++) {
        std::cout << "items2[" << i << "]=" << items2[i] << std::endl;
        if (items2_origin[i] != items2[i]) {
            std::cerr << "Error: item at (" << i << ") not equal: original=" << items2_origin[i] << "; processed=" << items2[i] << std::endl;
        }
    }
    return 0;
}

void
test_segtemp_format (void)
{
    std::string str;

    str.clear();
    segtempl_urlformat (str, "$RepresentationID$/seg_$Bandwidth%020$_$Number%05$.mp4", "repid", 15, 353423, 1394);
    std::cout << "SegTemp format string 1: '" << str << "'" << std::endl;
    if (str != "repid/seg_00000000000000353423_00015.mp4") {
        std::cerr << "Error in format 1!" << std::endl;
    }

    str.clear();
    segtempl_urlformat (str, "$$$RepresentationID$/seg_$Bandwidth%020$_$Time%05$.mp4", "repid", 15, 353423, 1394);
    std::cout << "SegTemp format string 2: '" << str << "'" << std::endl;
    if (str != "$repid/seg_00000000000000353423_01394.mp4") {
        std::cerr << "Error in format 2!" << std::endl;
    }

    str.clear();
    segtempl_urlformat (str, "mp4-main-ogop-$RepresentationID$$Number$.m4s", "h264ogop_low", 15, 44876, 1394);
    std::cout << "SegTemp format string 3: '" << str << "'" << std::endl;
    if (str != "mp4-main-ogop-h264ogop_low15.m4s") {
        std::cerr << "Error in format 3!" << std::endl;
    }

    str.clear();
    segtempl_urlformat (str, "$Bandwidth$/$Time$.mp4v", "h264ogop_low", 15, 44876, 1394);
    std::cout << "SegTemp format string 4: '" << str << "'" << std::endl;
    if (str != "44876/1394.mp4v") {
        std::cerr << "Error in format 4!" << std::endl;
    }
}

int
test_mpd_template (const char *fn_mpd)
{
    assert (NULL != fn_mpd);
    xml_reader_lib_t xmlrd (fn_mpd);
    SLP();
    if (! xmlrd.init()) {
        std::cerr << "Error in init libxml" << std::endl;
        return -1;
    }
    SLP();
    std::string name(fn_mpd);
    mpd_t *mpd = mpd_parse_current (&xmlrd, name);
    SLP();
    if (NULL == mpd) {
        std::cerr << "Error in parse MPD" << std::endl;
        return -1;
    }
    SLP();
    if (! mpd->verify()) {
        std::cerr << "Error in verify MPD" << std::endl;
    } else {
        std::cerr << "MPD file is valid!" << std::endl;
    }
    SLP();
    std::cout << mpd->toString() << std::endl;
    SLP();

    // test
    mpd_adaptationlogic_yhfu_t alg(mpd);
    mpd_chunk_t * chunk = NULL;
    std::vector<double> plist = alg.get_period_stime_list ();
    size_t i;
    size_t j;
    size_t k;
    size_t a;
    size_t b;
    for (i = 0; i + 1 < plist.size(); i ++) {
        std::cerr << "period " << plist[i] << std::endl;
        alg.seek (plist[i]);
        //std::vector<size_t> bwlist = alg.get_bandwith_list ("video", "");
        std::vector<std::string> cntyp = alg.get_mimetype_list ();
        for (a = 0; a < cntyp.size(); a ++) {
            std::string cnttype = cntyp[a];
            std::vector<std::string> langs = alg.get_language_list (cnttype);
        for (b = 0; b < langs.size(); b ++) {
            std::string language = langs[b];
            std::cerr << "  TYPE: " << cnttype << ", LANGUAGE: " << language << std::endl;

            alg.seek (plist[i]);

            std::vector<size_t> bwlist = alg.get_bandwith_list (cnttype, language);
            if (bwlist.size() < 1) continue;
            //for (c = 0; c < bwlist.size(); c ++) {}
            j = 0; // bwlist.size() / 2
            k = 0;
            alg.notify_avg_throughput (bwlist[j]);
            std::cerr << "    bandwidth " << bwlist[j] << std::endl;
            while ((chunk = alg.get_next_chunk(cnttype, language)) != NULL) {
                std::cout << "    (" << k << ") URL='" << chunk->get_url() << "', bitrate=" << chunk->get_bitrate();
                if (chunk->has_byterange()) {
                    std::cout << "   byte start=" << chunk->get_byte_start() << " end=" << chunk->get_byte_end();
                }
                if (chunk->is_index()) {
                    std::cout << " (index)";
                }
                std::cout << std::endl;
                if (chunk->get_opt_url().size() > 0) {
                    size_t l;
                    for (l = 0; l < chunk->get_opt_url().size(); l ++) {
                        std::cout << "      opt url(" << l << ") " << chunk->get_opt_url().at(l) << std::endl;
                    }
                    std::cout << std::endl;
                }
                delete chunk;
                k ++;
                if (alg.tell() >= plist[i + 1]) {
                    j ++;
                    if (j < bwlist.size()) {
                        alg.seek (plist[i]);
                        alg.notify_avg_throughput (bwlist.at(j));
                        k = 0;
                        std::cerr << "    bandwidth " << bwlist[j] << std::endl;
                    } else {
                        break;
                    }
                }
            }
        }}
    }
    delete mpd;
    SLP();
    return 0;
}

void
version (void)
{
    fprintf (stderr, "MPD verify tool for DASH specification.\n");
    fprintf (stderr, "Version %d.%d.%d\n", VER_MAJOR, VER_MINOR, VER_MOD);
    fprintf (stderr, "Copyright (c) 2013 Y. Fu<yhfudev@gmail.com>. All rights reserved.\n\n");
}

void
help (char *progname)
{
    fprintf (stderr, "Usage: \n"
        "\t%s [options] <MPD files>\n"
        , basename(progname));
    fprintf (stderr, "\nOptions:\n");
    //fprintf (stderr, "\t--output|-o <output file>\tThe name of file to be saved.\n");
    //fprintf (stderr, "\t--nocompab|-c\tDon't use compability mode.\n");
    fprintf (stderr, "\t--selftest|-S\tSelftest.\n");
    fprintf (stderr, "\t--help|-h\tPrint this message.\n");
    fprintf (stderr, "\t--verbose|-v\tVerbose information.\n");
    fprintf (stderr, "\t<MPD files>\tthe MPD file list\n");
}

void
usage (char *progname)
{
    version ();
    help (progname);
}

int
main (int argc, char *argv[])
{
    size_t i;
    int flag_selftest = 0;
    int flag_nocompab = 0;
    int c;
    char *fn_out = NULL;
    struct option longopts[]  = {
        { "selftest",     0, 0, 'S' },
        //{ "nocompab",     0, 0, 'c' },
        //{ "output",       1, 0, 'o' },

        { "help",         0, 0, 'h' },
        { "verbose",      0, 0, 'v' },
        { 0,              0, 0,  0  },
    };

    while ((c = getopt_long( argc, argv, "Sco:vh", longopts, NULL )) != EOF) {
        switch (c) {
        case 'v':
            break;
        case 'h':
            usage (argv[0]);
            exit (0);
            break;

        case 'S':
            flag_selftest = 1;
            break;
        case 'c':
            flag_nocompab = 1;
            break;
        case 'o':
            fn_out = optarg;
            break;
        default:
            fprintf (stderr, "Unknown parameter: '%c'.\n", c);
            fprintf (stderr, "Use '%s -h' for more information.\n", basename(argv[0]));
            exit (-1);
            break;
        }
    }
    if (flag_selftest) {
        test_xduration ();
        test_split ();
        test_segtemp_format ();
        return 0;
    }
    i = optind;
    for (; i < argc; i ++) {
        test_mpd_template (argv[i]);
    }

    return 0;
}

