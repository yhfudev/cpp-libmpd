/**
 * @file    mympdutils.h
 * @brief   functions for MPD
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-04-16
 */
#ifndef MYMPDUTILS_H
#define MYMPDUTILS_H
#if __cplusplus > 199711L
// c++11
    #define USE_CPP11X 1
#endif // c++11

#include <iostream>
#include <string>
#include <vector>

#include <ctime>

#include <stdio.h>
#define TRACE printf
#define ERROR printf

//#define FIXME() assert(0)
#define FIXME()

#define NUM_TYPE(a) (sizeof(a)/sizeof((a)[0]))

#ifdef __WIN32__ // or whatever
#define PRIiSZ "ld"
#define PRIuSZ "Iu"
#else
#define PRIiSZ "zd"
#define PRIuSZ "zu"
#endif

#if 0
inline void
split2vector (const std::string &sentence, std::vector<std::string> & contains)
{
    std::istringstream iss (sentence);
    contains.erase (contains.begin(), contains.end());
#if 0
    std::copy(std::istream_iterator<std::string>(iss),
             std::istream_iterator<std::string>(),
             std::ostream_iterator<std::string>(std::cout, "\n"));
#endif // 0
    std::copy(std::istream_iterator<std::string>(iss),
             std::istream_iterator<std::string>(),
             std::back_inserter<std::vector<std::string> >(contains));
}

//std::vector<std::string> v = str_split0<std::string>("Hello, there; World", ";,");
//std::vector<std::wstring> v = str_split0<std::wstring>(L"Hello, there; World", L";,");
template<typename T> vector<T>
str_split0 (const T & str, const T & delimiters)
{
    vector<T> v;
    T::size_type start = 0;
    auto pos = str.find_first_of (delimiters, start);
    while (pos != T::npos) {
        if(pos != start) { // ignore empty tokens
            v.emplace_back(str, start, pos - start);
        }
        start = pos + 1;
        pos = str.find_first_of(delimiters, start);
    }
    if(start < str.length()) { // ignore trailing delimiter
        v.emplace_back(str, start, str.length() - start); // add what's left of the string
    }
    return v;
}
#endif

/**
 * @brief split the string by the delimiters
 *
 * @param v : the vector to be returned
 * @param str : the string to be splitted
 * @param delimiters : the delimiters list
 *
 * @return the size of vector
 *
 * split the string by the delimiters
 */
template<typename T> size_t
str_split (std::vector<T> & v, const T & str, const T & delimiters)
{
    size_t c = 0;
    typename T::size_type start = 0;
    typename T::size_type pos = str.find_first_of (delimiters, start);
    while (pos != T::npos) {
        if(pos != start) { // ignore empty tokens
            std::cerr << "substr=" << str.substr(start, pos - start) << std::endl;
            v.push_back(str.substr(start, pos - start));
            c ++;
        }
        start = pos + 1;
        pos = str.find_first_of(delimiters, start);
    }
    if(start < str.length()) { // ignore trailing delimiter
        v.push_back(str.substr(start, str.length() - start)); // add what's left of the string
        c ++;
    }
    return c;
}

/**
 * @brief split the string and convert the values by the delimiters
 *
 * @param v : the vector to be returned
 * @param str : the string to be splitted
 * @param delimiters : the delimiters list
 *
 * @return the size of vector
 *
 * split the string and convert the values by the delimiters
 */
template<typename V, typename T> size_t
val_split (std::vector<V> & v, const T & str, const T & delimiters)
{
    size_t c = 0;
    V val;
    typename T::size_type start = 0;
    typename T::size_type pos = str.find_first_of (delimiters, start);
    while (pos != T::npos) {
        if(pos != start) { // ignore empty tokens
            std::stringstream(str.substr(start, pos - start)) >> val;
            v.push_back (val);
            c ++;
        }
        start = pos + 1;
        pos = str.find_first_of(delimiters, start);
    }
    if(start < str.length()) { // ignore trailing delimiter
        // add what's left of the string
        std::stringstream(str.substr(start, str.length() - start)) >> val;
        v.push_back (val);
        c ++;
    }
    return c;
}

void trim (std::string & str);

time_t xsdatetime_to_timet (const std::string & str);
double xsduration_to_seconds (const std::string & str);
void xsduration_from_seconds (std::string & str, double val);

bool segtempl_urlformat (std::string &ret, const std::string &fmt, const std::string & rid, size_t number, size_t bandwidth, size_t time);
bool segtempl_urlformat (std::string &ret, const char * format1, const char * rid1, size_t number, size_t bandwidth, size_t time);

/**
 * @brief compare the user data at position idx and data_pin
 *
 * @param userdata : User's data
 * @param idx : the user data to be compared with at the position idx
 * @param data_pin : the data to be compared passed by the bsearch's arg
 *
 * @return return the result of "userdata[idx] - *data_pin". 0 on equal, -1 on userdata[idx]<*data_pin, 1 on userdata[idx]>*data_pin
 *
 * compare the user data at position idx and data_pin
 */
typedef int (* pf_bsearch_cb_comp_t)(void *userdata, size_t idx, void * data_pin); /*"userdata[idx] - *data_pin"*/
int pf_bsearch_r (void *userdata, size_t num_data, pf_bsearch_cb_comp_t cb_comp, void *data_pinpoint, size_t *ret_idx);

#endif // MYMPDUTILS_H
