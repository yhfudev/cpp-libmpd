/**
 * @file    myxmlparser.h
 * @brief   General xml parser interface
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-04-16
 */
#ifndef MYXMLPARSER_H
#define MYXMLPARSER_H

#include <vector>
#include <string>
#include <sstream>
#include <iostream>

#include <string.h>
#include <assert.h>

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#ifdef __cplusplus
# define CEXT_BEGIN extern "C" {
# define CEXT_END   }
#else
# define CEXT_BEGIN
# define CEXT_END
#endif

#if defined(LIBXML2) || defined(HAVE_LIBXML_URI_H)
#define USE_LIBXML2 1
//#define USE_VLCAPI  1
#endif


#ifdef MODULE_STRING
#define USE_VLCLIB  1
#endif


#ifdef USE_VLCLIB
#define USE_VLCAPI  1
#endif

#ifdef USE_VLCAPI
// VLC module
#include "vlcfix.h"
#endif

#include "mympdutils.h"

///////////////////////////////////////////////////////////////////////////////
// wrap vlc-xml and libxml2
class xml_reader_base_t {

public:
    typedef enum type_s {
        ERROR = -1,
        NONE=0,
        STARTELEM,
        ENDELEM,
        TEXT,
    } type_t;

    virtual bool init (void) = 0;
    virtual bool clear (void) = 0;
    virtual xml_reader_base_t::type_t NextNode (const char **pval) = 0;
    virtual const char * NextAttr (const char **pval) = 0;
    virtual bool IsEmptyElement (void) = 0;

    // bool printAll (std::string & parent_name);
};

#ifdef USE_VLCLIB
// VLC module
#include <vlc_common.h>
#include <vlc_stream.h>
#include <vlc_xml.h>

#define xml_reader_lib_t xml_reader_vlc_t

class xml_reader_vlc_t : public xml_reader_base_t {

public:
    xml_reader_vlc_t (stream_t *s) : stream(s), vlc_xml(NULL), vlc_reader(NULL) {}
    ~xml_reader_vlc_t () {clear ();}
    virtual bool init (void);
    virtual bool clear (void);

    virtual xml_reader_base_t::type_t NextNode (const char **pval);
    virtual const char * NextAttr (const char **pval);
    virtual bool IsEmptyElement (void);

private:
    stream_t     *stream;
    xml_t        *vlc_xml;
    xml_reader_t *vlc_reader;
};

inline bool
xml_reader_vlc_t::init (void) {
    this->vlc_xml = xml_Create (this->stream);
    if(! this->vlc_xml) {
        return false;
    }
    this->vlc_reader = xml_ReaderCreate (this->vlc_xml, this->stream);
    if(! this->vlc_reader) {
        xml_Delete (this->vlc_xml);
        return false;
    }
    return true;
}

inline bool
xml_reader_vlc_t::clear (void)
{
    if (this->vlc_reader) {
        xml_ReaderDelete (this->vlc_reader);
    }
    this->vlc_xml = NULL;
    if (this->vlc_xml) {
        xml_Delete (this->vlc_xml);
    }
    this->vlc_xml = NULL;
    return true;
}

inline xml_reader_base_t::type_t
xml_reader_vlc_t::NextNode (const char **pval)
{
    switch (xml_ReaderNextNode (this->vlc_reader, pval)) {
    case XML_READER_NONE:
        return xml_reader_base_t::NONE;
    case XML_READER_STARTELEM:
        return xml_reader_base_t::STARTELEM;
    case XML_READER_ENDELEM:
        return xml_reader_base_t::ENDELEM;
    case XML_READER_TEXT:
        return xml_reader_base_t::TEXT;
    default:
        return xml_reader_base_t::ERROR;
    }
    return xml_reader_base_t::ERROR;
}

inline const char *
xml_reader_vlc_t::NextAttr (const char **pval)
{
    return xml_ReaderNextAttr (this->vlc_reader, pval);
}

inline bool
xml_reader_vlc_t::IsEmptyElement (void)
{
    if (xml_ReaderIsEmptyElement (this->vlc_reader)) {
        return true;
    }
    return false;
}

#elif ! defined(USE_LIBXML2)
#error please use xml or vlcxml

#include <stack>
#include <stdexcept>

#define xml_reader_lib_t xml_reader_xerces_t

// Xerces-C
#include <xercesc/dom/DOM.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOMDocumentType.hpp>
#include <xercesc/dom/DOMElement.hpp>
#include <xercesc/dom/DOMImplementation.hpp>
#include <xercesc/dom/DOMImplementationLS.hpp>
#include <xercesc/dom/DOMNode.hpp>
#include <xercesc/dom/DOMNodeIterator.hpp>
#include <xercesc/dom/DOMNodeList.hpp>
#include <xercesc/dom/DOMText.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/XMLUni.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>

XERCES_CPP_NAMESPACE_USE

class xml_reader_xerces_t : public xml_reader_base_t {

public:
    xml_reader_xerces_t (std::string url1) : url(url1), m_ConfigFileParser(NULL), curNode(NULL), curAttrs(NULL), idxAttr(0), tmpNode(NULL), tmpValue(NULL) {}
    ~xml_reader_xerces_t () {clear ();}
    virtual bool init (void);
    virtual bool clear (void);

    virtual xml_reader_base_t::type_t NextNode (const char **pval);
    virtual const char * NextAttr (const char **pval);
    virtual bool IsEmptyElement (void);

private:
    std::string url;
    xercesc::XercesDOMParser *m_ConfigFileParser;

    //DOMDocument* xmlDoc; // no need to free this pointer - owned by the parent parser object
    //DOMElement* elementRoot; // the top-level element: NAme is "MPD".

    std::stack<DOMNode *> stk;
    DOMNode * curNode;
    DOMNamedNodeMap * curAttrs;
    int idxAttr;
    char * tmpNode; // 用于缓冲字符串
    char * tmpValue; // 用于缓冲字符串
};

inline bool
xml_reader_xerces_t::init (void)
{
    try {
        XMLPlatformUtils::Initialize();  // Initialize Xerces infrastructure
    } catch( XMLException& e ) {
        char* message = XMLString::transcode( e.getMessage() );
        std::cerr << "XML toolkit initialization error: " << message << std::endl;
        XMLString::release( &message );
        // throw exception here to return ERROR_XERCES_INIT
    }
    m_ConfigFileParser = new XercesDOMParser;
    if (NULL == m_ConfigFileParser) {
        return false;
    }
    m_ConfigFileParser->setValidationScheme( XercesDOMParser::Val_Never );
    m_ConfigFileParser->setDoNamespaces( false );
    m_ConfigFileParser->setDoSchema( false );
    m_ConfigFileParser->setLoadExternalDTD( false );

    m_ConfigFileParser->parse (this->url.c_str());

    try {
        DOMDocument* xmlDoc = m_ConfigFileParser->getDocument();
        DOMElement* elementRoot = xmlDoc->getDocumentElement();
        if( !elementRoot ) {
            throw(std::runtime_error( "empty XML document" ));
        }
        stk.push (dynamic_cast< xercesc::DOMNode * >(elementRoot));

    } catch( xercesc::XMLException& e ) {
        char* message = xercesc::XMLString::transcode( e.getMessage() );
        std::ostringstream errBuf;
        errBuf << "Error parsing file: " << message << std::flush;
        XMLString::release( &message );
    }

    return true;
}

inline bool
xml_reader_xerces_t::clear (void)
{
    if (NULL != m_ConfigFileParser) {
        delete m_ConfigFileParser;
    }
    m_ConfigFileParser = NULL;
    if (NULL != tmpNode) {
        XMLString::release(&tmpNode);
    }
    tmpNode = NULL;
    if (NULL != tmpValue) {
        XMLString::release(&tmpValue);
    }
    tmpValue = NULL;
    try {
        XMLPlatformUtils::Terminate();  // Terminate after release of memory
    } catch( xercesc::XMLException& e ) {
        char* message = xercesc::XMLString::transcode( e.getMessage() );
        std::cerr << "XML ttolkit teardown error: " << message << std::endl;
        XMLString::release( &message );
    }
    return true;
}

inline char *
xeres_get_value (DOMNode * node, char * &buffer)
{
    if (buffer) {
        XMLString::release(&buffer);
        buffer = NULL;
    }
    const XMLCh * nm = node->getNodeValue();
    if (NULL == nm) {
        return NULL;
    }
    buffer = XMLString::transcode (nm);
    return buffer;
}

inline char *
xeres_get_name (DOMNode * node, char * &buffer)
{
    if (buffer) {
        XMLString::release(&buffer);
        buffer = NULL;
    }
    const XMLCh * nm = node->getNodeName();
    if (NULL == nm) {
        return NULL;
    }
    buffer = XMLString::transcode (nm);
    return buffer;
}

inline xml_reader_base_t::type_t
xml_reader_xerces_t::NextNode (const char **pval)
{
    // 压入栈的节点都访问过
    // 当前节点表示需要访问子节点
    // 当子节点访问完后，弹出的节点需要访问兄弟节点
    // 如果当前子节点为 NULL 表示栈中的节点需要访问（根节点）,如果栈中没有，则需要退出
    // assess
    curAttrs = NULL;
    xml_reader_base_t::type_t ret = xml_reader_base_t::NONE;
    while (xml_reader_base_t::NONE == ret) {
        // get next
        if (NULL == curNode) {
            if (NULL == curNode) {
                if (this->stk.size () > 0) {
                    curNode  = this->stk.top ();
                    this->stk.pop ();
                }
            }
            if (NULL == curNode) {
                std::cerr << "No next node" << std::endl;
                return xml_reader_base_t::NONE;
            }
        } else {
            if (curNode->hasChildNodes ()) {
                stk.push (curNode);
                curNode = curNode->getFirstChild();
            } else {
                curNode = curNode->getNextSibling();
                while ((NULL == curNode) && (this->stk.size () > 0)) {
                    curNode  = this->stk.top ();
                    this->stk.pop ();
                    tmpNode = xeres_get_name (curNode, tmpNode);
                    ret = xml_reader_base_t::ENDELEM;
                    curNode = curNode->getNextSibling();
                    this->stk.push (curNode);
                    curNode = NULL;
                    break;
                }
            }
        }
        if (NULL != curNode) {
            switch (curNode->getNodeType()) {
            case DOMNode::ELEMENT_NODE:
                tmpNode = xeres_get_name (curNode, tmpNode);
                ret = xml_reader_base_t::STARTELEM;
                break;
            case DOMNode::CDATA_SECTION_NODE:
            case DOMNode::TEXT_NODE:
                tmpNode = xeres_get_value (curNode, tmpNode);
                ret = xml_reader_base_t::TEXT;
                break;
            case DOMNode::ENTITY_REFERENCE_NODE:
            case DOMNode::ENTITY_NODE:
            case DOMNode::DOCUMENT_NODE:
            default:
                break;
            }
        }
    }

    if (pval && tmpNode) {
        *pval = (char *)tmpNode;//strdup ((char *)tmpNode);
    }
    return ret;
}

inline const char *
xml_reader_xerces_t::NextAttr (const char **pval)
{
    const XMLCh *name, *value;

    if (NULL == curAttrs) {
        curAttrs = curNode->getAttributes();
        idxAttr = 0;
        if (NULL == curAttrs) {
            return NULL;
        }
    }
    if (idxAttr < curAttrs->getLength()) {
        DOMNode* node = curAttrs->item (idxAttr);
        char * name = NULL;
        name = xeres_get_name (node, name);
        char * value = NULL;
        value = xeres_get_value (node, value);
        *pval = (const char *)value;
        idxAttr ++;
        return (const char *)name;
    }

    return NULL;
}

inline bool
xml_reader_xerces_t::IsEmptyElement (void)
{
    if (curNode->hasChildNodes ()) {
        return false;
    }
    return true;
}

#else
//#error please use vlcxml

#include <libxml/xmlreader.h>

#define xml_reader_lib_t xml_reader_libxml_t

class xml_reader_libxml_t : public xml_reader_base_t {

public:
    ~xml_reader_libxml_t () {clear ();}
    virtual bool init (void);
    virtual bool clear (void);

    virtual xml_reader_base_t::type_t NextNode (const char **pval);
    virtual const char * NextAttr (const char **pval);
    virtual bool IsEmptyElement (void);

#ifndef USE_VLCAPI
    xml_reader_libxml_t (std::string url1) : url(url1), reader(NULL) {}
#else
    xml_reader_libxml_t (std::string url1) : stream(NULL), content_buf(NULL), url(url1), reader(NULL) {}
    xml_reader_libxml_t (stream_t *s) : stream(s), content_buf(NULL), reader(NULL) {
        url = s->psz_access; url += "://"; url += s->psz_path;
    }
private:
    stream_t * stream;
    char * content_buf;
#endif

private:
    std::string url;
    xmlTextReaderPtr reader;
};

inline bool
xml_reader_libxml_t::init (void)
{
    xmlInitParser();
    if (NULL != this->reader) {
        xmlFreeTextReader (this->reader);
    }

    this->reader = NULL;
#ifdef USE_VLCAPI
    if (NULL != this->stream) {
        int64_t size = stream_Size (this->stream);
        if (size > 0) {
            this->content_buf = (char *)realloc (this->content_buf, size + 2);
            if (this->content_buf) {
                int read = stream_Read ( this->stream, this->content_buf, size );
                if (read > 0) {
                    this->reader = xmlReaderForMemory (this->content_buf, read, this->url.c_str(), NULL, 0);
                }
            }
        }
    } else
#else
    this->reader = xmlReaderForFile(this->url.c_str(), NULL, 0);
#endif
    if (NULL == this->reader) {
        //ERROR ("Error in xmlReaderForFile('%s')", this->url.c_str());
        return false;
    }
    return true;
}

inline bool
xml_reader_libxml_t::clear (void)
{
    if (NULL != this->reader) {
        xmlFreeTextReader (this->reader);
    }
    this->reader = NULL;
    xmlCleanupParser();
#ifdef USE_VLCAPI
    if (NULL != content_buf) {
        free (content_buf);
        content_buf = NULL;
    }
#endif
    return true;
}

inline xml_reader_base_t::type_t
xml_reader_libxml_t::NextNode (const char **pval)
{
    const xmlChar * node = NULL;
    xml_reader_base_t::type_t ret;
    while (1) {
        switch (xmlTextReaderRead(this->reader)) {
        case 0:
            return xml_reader_base_t::NONE;
        case -1:
            return xml_reader_base_t::ERROR;
        }
        int type = xmlTextReaderNodeType(this->reader);
        if (-1 == type) {
            return xml_reader_base_t::ERROR;
        }
        ret = xml_reader_base_t::NONE;
        switch (type) {
        case XML_READER_TYPE_ELEMENT:
            node = xmlTextReaderConstName (this->reader);
            ret = xml_reader_base_t::STARTELEM;
            break;
        case XML_READER_TYPE_END_ELEMENT:
            node = xmlTextReaderConstName (this->reader);
            ret = xml_reader_base_t::ENDELEM;
            break;

        case XML_READER_TYPE_CDATA:
        case XML_READER_TYPE_TEXT:
            node = xmlTextReaderConstValue (this->reader);
            ret = xml_reader_base_t::TEXT;
            break;
        }
        if (xml_reader_base_t::NONE == ret) {
            continue;
        } else {
            break;
        }
    }
    assert (NULL != node);

    if (pval) {
        *pval = (char *)node;//strdup ((char *)node);
    }
    return ret;
}

inline const char *
xml_reader_libxml_t::NextAttr (const char **pval)
{
    const xmlChar *name, *value;

    if( xmlTextReaderMoveToNextAttribute( this->reader ) != 1
     || (name = xmlTextReaderConstName( this->reader )) == NULL
     || (value = xmlTextReaderConstValue( this->reader )) == NULL )
        return NULL;

    *pval = (const char *)value;
    return (const char *)name;
}

inline bool
xml_reader_libxml_t::IsEmptyElement (void)
{
    if (xmlTextReaderIsEmptyElement (this->reader)) {
        return true;
    }
    return false;
}

#endif // 0

///////////////////////////////////////////////////////////////////////////////
//extern bool xml_parser_skip_current_node (xml_reader_base_t * xmlrd, int rank = 1);

#define MYXML_UNSED(a) ((a)=(a))

class xml_parser_base_t {

public:
    bool parse_current (xml_reader_base_t * xmlrd);

protected:
    virtual bool on_text (const char *text) {MYXML_UNSED(text); return false;};
    virtual bool on_attribute (const char *attrName, const char *attrValue) {MYXML_UNSED(attrName); MYXML_UNSED(attrValue); return false;};
    virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name) {MYXML_UNSED(xmlrd); MYXML_UNSED(node_name); return false;};
};

class xml_parser_stdstring_t : public xml_parser_base_t, public std::string {

protected:
    inline virtual bool on_text (const char *text);
};

inline bool
xml_parser_stdstring_t::on_text (const char *text)
{
    this->reserve (strlen (text));
    this->clear ();
    this->append (text);
    return true;
}

#endif // MYXMLPARSER_H
