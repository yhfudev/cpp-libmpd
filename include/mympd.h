/**
 * @file    mympd.h
 * @brief   parse and present the the DASH MPD file data structures mapped directly from the description of the
 *          ISO/IEC 23009-1:2012 Dynamic adaptive streaming over HTTP (DASH) -- Part 1: Media presentation description and segment formats
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-04-16
 */
#ifndef MYMPD_H
#define MYMPD_H

#include <string.h>
#include <assert.h>

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <iterator>

#include "myxmlparser.h"

#ifndef TI
#if 0 // #if LIBDASH_USE_DASH_DOWNLOAD
#  define TE(fmt, ...) msg_Err(NULL, fmt, ##__VA_ARGS__)
#  define TI(fmt, ...) msg_Info(NULL, fmt, ##__VA_ARGS__)
#  define TW(fmt, ...) msg_Dbg(NULL, fmt, ##__VA_ARGS__)
#  define TD(fmt, ...) msg_Dbg(NULL, fmt, ##__VA_ARGS__)
#else
#  ifdef DEBUG
#    define TE(fmt, ...) fprintf(stderr, "[%s()]\t" fmt "\t{%d," __FILE__ "}\n", __func__, ##__VA_ARGS__, __LINE__)
#    define TW TE
#    define TI TE
#    define TD TE
#  else
#    define TE(fmt, ...) fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#    define TW TE
#    define TI(fmt, ...)
#    define TD TI
#  endif
#endif
#endif

#if USE_VLCLIB
extern stream_t *g_dbg_stream;
#define DBG_SET_STREAM(s) g_dbg_stream = (s)

#undef TI
#undef TE
#undef TW
#undef TD
#  define TE(fmt, ...) msg_Err(g_dbg_stream, fmt, ##__VA_ARGS__)
#  define TI(fmt, ...) msg_Info(g_dbg_stream, fmt, ##__VA_ARGS__)
#  define TW(fmt, ...) msg_Dbg(g_dbg_stream, fmt, ##__VA_ARGS__)
#  define TD(fmt, ...) msg_Dbg(g_dbg_stream, fmt, ##__VA_ARGS__)
#else
#define DBG_SET_STREAM(s) (0)
#endif

// if compatibility with old draft?
#define USE_COMPATIBILITY 1

#define FIX_SORENSON_SQUEEZE 1

///////////////////////////////////////////////////////////////////////////////
/* @brief the base class for all of the MPD classes,
 *        The function toString() is used for debug, verify() is for checking the correctness of the MPD file.
 */
class mpd_base_t : public xml_parser_base_t {

public:
    virtual ~mpd_base_t() {}
    virtual std::string toString (void) = 0;       /*!< convert all of the values back to MPD xml format. */
    virtual bool verify (void) {return true;}      /*!< verify if the values got from MPD file are all resonable. */
    virtual bool post_parser (void) {return true;} /*!< do some optimizations after reading config file */
};

// URLType, 5.3.9.2.2
class mpd_url_t : public mpd_base_t {

protected:
    virtual bool on_attribute (const char *attrName, const char *attrValue);
    //virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    mpd_url_t (const char *t) : title(t) {}
    virtual ~mpd_url_t () {}

    // base func:
    virtual std::string toString (void);

    const char * get_title () const {return this->title;}
    void set_title (const char * p) {this->title = p;}
//    void set_title (const std::string& u) {this->title = u;}

    const std::string& get_surl () const {return this->sourceURL;}
    void set_surl (const char * p) {this->sourceURL = p;}
    void set_surl (const std::string& u) {this->sourceURL = u;}

    //const size_t get_range () const {return this->range;}
    //void set_range (const std::string& u) {std::stringstream(u) >> this->range;}
    const std::string& get_range () const {return this->range;}
    void set_range (const char * p) {this->range = p;}
    void set_range (const std::string& u) {this->range = u;}

private:
    const char * title; // Initialization/RepresentationIndex
    // attributes
    // O
    std::string sourceURL;
    //size_t range;
    std::string range;
};

// DescriptorType
class mpd_descriptor_t : public mpd_base_t {

protected:
    virtual bool on_attribute (const char *attrName, const char *attrValue);
    //virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    mpd_descriptor_t (const char * t) : title (t) {}

    // base func:
    virtual bool verify (void);
    virtual std::string toString (void);

    const char * get_title () const {return this->title;}

    const std::string& get_schid () const {return this->schemeIdUri;}
    void set_schid (const char * p) {this->schemeIdUri = p;}
    void set_schid (const std::string& u) {this->schemeIdUri = u;}

    const std::string& get_value () const {return this->value;}
    void set_value (const char * p) {this->value = p;}
    void set_value (const std::string& u) {this->value = u;}

private:
    const char * title;

    // M
    std::string schemeIdUri; // URN or URL

    // O
    std::string value;
};

// SegmentBase
class mpd_segmentbase_t : public mpd_base_t {

protected:
    virtual bool on_attribute (const char *attrName, const char *attrValue);
    virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    mpd_segmentbase_t ();
    ~mpd_segmentbase_t ();

    // base func:
    virtual bool verify (void);
    virtual std::string toString (void);

    std::string toStringAttr (void);
    std::string toStringContent (void);

    size_t get_timescale () const {return this->timescale;}
    void set_timescale (const std::string& u) {std::stringstream(u) >> this->timescale;}

    size_t get_prestmoffset () const {return this->presentationTimeOffset;}
    void set_prestmoffset (const std::string& u) {std::stringstream(u) >> this->presentationTimeOffset;}

    bool get_idxrangeexact () const {return this->indexRangeExact;}
    void set_idxrangeexact (const std::string& u) {if (! u.compare("true")) {this->indexRangeExact = true;} else {this->indexRangeExact = false;}}

    const std::string& get_idxrange () const {return this->indexRange;}
    void set_idxrange (const char * p) {this->indexRange = p;}
    void set_idxrange (const std::string& u) {this->indexRange = u;}

    mpd_url_t * get_initurl (void) { return this->Initialization; }
    void set_initurl (mpd_url_t *purl) {if (NULL != this->Initialization) { delete this->Initialization; } this->Initialization = purl; }
    mpd_url_t * get_repidxurl (void) { return this->RepresentationIndex; }
    void set_repidxurl (mpd_url_t *purl) {if (NULL != this->RepresentationIndex) { delete this->RepresentationIndex; } this->RepresentationIndex = purl; }

private:
    // attributes
    // O
    size_t timescale; // default 1,  the timescale in units per seconds to be used for the derivation of different real-time duration values in the Segment Information.
    size_t presentationTimeOffset;
    std::string indexRange;
    bool indexRangeExact; // OD: false/true

    // sequences:
    mpd_url_t *Initialization;
    mpd_url_t *RepresentationIndex;
};

class mpd_segmenttimeline_s_t : public mpd_base_t {

protected:
    virtual bool on_attribute (const char *attrName, const char *attrValue);
    //virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    mpd_segmenttimeline_s_t () : r(0), t(-1), d(-1) {}

    // base func:
    virtual bool verify (void);
    virtual std::string toString (void);

    ssize_t get_t () const {return (const ssize_t)this->t;}
    void set_t (const std::string & u) {std::stringstream(u) >> this->t;}

    size_t get_r () const {return this->r;}
    void set_r (const std::string & u) {std::stringstream(u) >> this->r;}

    ssize_t get_d () const {return this->d;}
    void set_d (const std::string & u) {std::stringstream(u) >> this->d;}

private:
    // attributes
    // O
    size_t r;  // OD, 0. the repeat count of the number of following contiguous Segments with the same duration expressed by the value of @d
    ssize_t t; // the MPD start time, in @timescale units
    // M
    ssize_t d; // the Segment duration, in units of the value of the @timescale.
};

// SegmentTimeline
class mpd_segmenttimeline_t : public mpd_base_t {

protected:
    //virtual bool on_attribute (const char *attrName, const char *attrValue);
    virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    // base func:
    virtual bool verify (void);
    virtual std::string toString (void);

    std::vector<mpd_segmenttimeline_s_t *> & get_s (void) { return this->S; }
    void add_s (mpd_segmenttimeline_s_t *pb) {S.push_back(pb);}

private:
    std::vector<mpd_segmenttimeline_s_t *> S;
};

// MultipleSegmentBaseInformation
class mpd_segmentmultibase_t : public mpd_segmentbase_t {

protected:
    virtual bool on_attribute (const char *attrName, const char *attrValue);
    virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    mpd_segmentmultibase_t ();
    ~mpd_segmentmultibase_t ();

    // base func:
    virtual bool verify (void);
    virtual std::string toString (void);

    std::string toStringAttr (void);
    std::string toStringContent (void);

    double inference_duration (size_t idx = 0); /*<= get the real duration */

    ssize_t get_duration () const {return this->duration;}

    void set_duration (const std::string& u)
    {
#if USE_COMPATIBILITY
        if (0 == u.find("P")) {
            this->duration = xsduration_to_seconds (u);
        } else
#endif
        {
            std::stringstream(u) >> this->duration;
        }
    }

    size_t get_snum () const {return this->startNumber;}
    void set_snum (const std::string& u) {std::stringstream(u) >> this->startNumber;}

    mpd_segmenttimeline_t * get_segtmln () {return this->SegmentTimeline;}
    void set_segtmln (mpd_segmenttimeline_t *p) {this->SegmentTimeline = p;}

    mpd_url_t * get_bitswitch () {return this->BitstreamSwitching;}
    void set_bitswitch (mpd_url_t *p) {this->BitstreamSwitching = p;}

    // utils:
    size_t get_timeline (size_t idx);

private:
    // attributes
    // O
    ssize_t duration;   // -1, no-exist
    ssize_t startNumber; // -1, no-exist

    // sequences:
    mpd_segmenttimeline_t *SegmentTimeline;
    mpd_url_t *BitstreamSwitching;
};

// The elements AdaptationSet, Representation and SubRepresentation have assigned common attributes and elements.
// CommonAttributesElements, RepresentationBaseType, 5.3.7
class mpd_representation_base_t : public mpd_base_t {

protected:
    virtual bool on_attribute (const char *attrName, const char *attrValue);
    virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    ~mpd_representation_base_t ();

    // base func:
    virtual bool verify (void);
    virtual std::string toString (void);

    std::string toStringAttr (void);
    std::string toStringContent (void);

    const std::string& get_mimetype () const {return this->mimeType;}
    void set_mimetype (const char * p) {this->mimeType = p;}
    void set_mimetype (const std::string& u) {this->mimeType = u;}

    const std::string& get_codecs () const {return this->codecs;}
    void set_codecs (const char * p) {this->codecs = p;}
    void set_codecs (const std::string& u) {this->codecs = u;}

    size_t get_width () const {return this->width;}
    void set_width (const std::string& u) {std::stringstream(u) >> this->width;}

    size_t get_height () const {return this->height;}
    void set_height (const std::string& u) {std::stringstream(u) >> this->height;}

    size_t get_startsap () const {return this->startWithSAP;}
    void set_startsap (const std::string& u) {std::stringstream(u) >> this->startWithSAP;}

    std::vector<mpd_descriptor_t *> & get_framepck (void) { return this->FramePacking; }
    void add_framepck (mpd_descriptor_t *pb) {FramePacking.push_back(pb);}

    std::vector<mpd_descriptor_t *> & get_audconf (void) { return this->AudioChannelConfiguration; }
    void add_audconf (mpd_descriptor_t *pb) {AudioChannelConfiguration.push_back(pb);}

    std::vector<mpd_descriptor_t *> & get_cntprotect (void) { return this->ContentProtection; }
    void add_cntprotect (mpd_descriptor_t *pb) {ContentProtection.push_back(pb);}

    const std::vector<std::string> & get_profiles () const {return this->profiles;}
    void set_profiles (const char * p) { str_split<std::string> (this->profiles, p, ","); }
    void set_profiles (const std::string& s) { str_split<std::string> (this->profiles, s, ","); }

    const std::vector<std::string> & get_segprofiles () const {return this->segmentProfiles;}
    void set_segprofiles (const char * p) { str_split<std::string> (this->segmentProfiles, p, ","); }
    void set_segprofiles (const std::string& s) { str_split<std::string> (this->segmentProfiles, s, ","); }

    const std::vector<std::string> & get_audsamp () const {return this->audioSamplingRate;}
    void set_audsamp (const char * p) { str_split<std::string> (this->audioSamplingRate, p, " "); }
    void set_audsamp (const std::string& s) { str_split<std::string> (this->audioSamplingRate, s, " "); }

    const std::string& get_fmrate () const {return this->frameRate;}
    void set_fmrate (const char * p) {this->frameRate = p;}
    void set_fmrate (const std::string& u) {this->frameRate = u;}

    double get_sapperiodmax () const {return this->maximumSAPPeriod;}
    void set_sapperiodmax (const std::string& u) {std::stringstream(u) >> this->maximumSAPPeriod;}

    double get_plratemax () const {return this->maxPlayoutRate;}
    void set_plratemax (const std::string& u) {std::stringstream(u) >> this->maxPlayoutRate;}

    bool get_codedep () const {return this->codingDependency;}
    void set_codedep (const std::string& u) {if (! u.compare("true")) {this->codingDependency = true;} else {this->codingDependency = false;}}

    const std::string& get_scantype () const {return this->scanType;}
    void set_scantype (const char * p) {this->scanType = p;}
    void set_scantype (const std::string& u) {this->scanType = u;}

    const std::string& get_sar () const {return this->sar;}
    void set_sar (const char * p) {this->sar = p;}
    void set_sar (const std::string& u) {this->sar = u;}

private:
    // attributes:
    // M
    std::string mimeType;
    std::string codecs;

    // O
    size_t width; // if not exit, 0
    size_t height;

    std::vector<std::string> profiles; // a comma-separated list of profile identifiers.
    std::string sar;
    std::string frameRate;
    std::vector<std::string> audioSamplingRate; // Either a single decimal integer value specifying the sampling rate or a whitespace separated pair of decimal integer values specifying the minimum and maximum sampling rate of the audio media component type.
    std::vector<std::string> segmentProfiles; // a comma-separated list of the individual Segment profile identifiers.
    double maximumSAPPeriod;
    size_t startWithSAP;
    double maxPlayoutRate;
    bool codingDependency;
    std::string scanType;

    // sequence:
    std::vector<mpd_descriptor_t *> FramePacking;
    std::vector<mpd_descriptor_t *> AudioChannelConfiguration;
    std::vector<mpd_descriptor_t *> ContentProtection;
};

// SubRepresentation
class mpd_subrepres_t : public mpd_representation_base_t {

protected:
    virtual bool on_attribute (const char *attrName, const char *attrValue);
    virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    mpd_subrepres_t ();

    // base func:
    virtual bool verify (void);
    virtual std::string toString (void);

    size_t get_level () const {return this->level;}
    void set_level (const std::string& u) {std::stringstream(u) >> this->level;}

    size_t get_bandwidth () const {return this->bandwidth;}
    void set_bandwidth (const std::string& u) {std::stringstream(u) >> this->bandwidth;}

    const std::vector<size_t> & get_deplevel () const {return this->dependencyLevel;}
    void set_deplevel (const char * p) { val_split<size_t, std::string> (this->dependencyLevel, p, " "); }
    void set_deplevel (const std::string& s) { val_split<size_t, std::string> (this->dependencyLevel, s, " "); }

    const std::vector<size_t> & get_cntcomp () const {return this->contentComponent;}
    void set_cntcomp (const char * p) { val_split<size_t, std::string> (this->contentComponent, p, " "); }
    void set_cntcomp (const std::string& s) { val_split<size_t, std::string> (this->contentComponent, s, " "); }

private:
    // attributes:
    // O
    ssize_t level;
    ssize_t bandwidth;
    //std::vector<size_t> dependencyLevel;
    std::vector<size_t> dependencyLevel; //UIntVectorType, a whitespace-separated list of @level values.
    std::vector<size_t> contentComponent; //StringVectorType, a whitespace-separated list of values of ContentComponent@id values.
};

class mpd_baseurl_t : public mpd_base_t {

protected:
    virtual bool on_attribute (const char *attrName, const char *attrValue);
    //virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    // base func:
    virtual bool on_text (const char *text);
    virtual std::string toString (void);

    const std::string& get_url () const {return this->url;}
    void set_url (const char * p) {this->url = p; chk_valid_url();}
    void set_url (const std::string & u) {this->url = u; chk_valid_url();}

    const std::string& get_svcloc () const {return this->serviceLocation;}
    void set_svcloc (const char * p) {this->serviceLocation = p;}
    void set_svcloc (const std::string& u) {this->serviceLocation = u;}

    const std::string& get_brange () const {return this->byteRange;}
    void set_brange (const char * p) {this->byteRange = p;}
    void set_brange (const std::string& u) {this->byteRange = u;}

private:
#ifdef WIN32
#define PATH_SEP '\\'
#else
#define PATH_SEP '/'
#endif
    void chk_valid_url (void) {
        trim (this->url);
        if (this->url.size() > 0) {
            if (PATH_SEP != this->url.at(this->url.size() - 1)) {
                this->url += PATH_SEP;
            }
        }
    }
    std::string url;
    // attributes:
    // O
    std::string serviceLocation;
    std::string byteRange;
};

// SegmentURL
class mpd_segmenturl_t : public mpd_base_t {

protected:
    virtual bool on_attribute (const char *attrName, const char *attrValue);
    //virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    // base func:
    virtual bool verify (void);
    virtual std::string toString (void);

    const std::string& get_media () const {return this->media;}
    void set_media (const char * p) {this->media = p;}
    void set_media (const std::string& u) {this->media = u;}

    const std::string& get_index () const {return this->index;}
    void set_index (const char * p) {this->index = p;}
    void set_index (const std::string& u) {this->index = u;}

    const std::string& get_mediarange () const {return this->mediaRange;}
    void set_mediarange (const char * p) {this->mediaRange = p;}
    void set_mediarange (const std::string& u) {this->mediaRange = u;}

    const std::string& get_indexrange () const {return this->indexRange;}
    void set_indexrange (const char * p) {this->indexRange = p;}
    void set_indexrange (const std::string& u) {this->indexRange = u;}

private:
    // attributes:
    // O
    std::string media;
    std::string mediaRange;
    std::string index;
    std::string indexRange;
};

// SegmentList
class mpd_segmentlist_t : public mpd_segmentmultibase_t {

protected:
    virtual bool on_attribute (const char *attrName, const char *attrValue);
    virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    ~mpd_segmentlist_t ();

    // base func:
    virtual bool verify (void);
    virtual std::string toString (void);

    const std::string& get_xhref () const {return this->xhref;}
    void set_xhref (const char * p) {this->xhref = p;}
    void set_xhref (const std::string& u) {this->xhref = u;}

    const std::string& get_xactuate () const {return this->xactuate;}
    void set_xactuate (const char * p) {this->xactuate = p;}
    void set_xactuate (const std::string& u) {this->xactuate = u;}

    std::vector<mpd_segmenturl_t *> & get_segurl (void) { return this->SegmentURL; }
    void add_segurl (mpd_segmenturl_t *pb) {SegmentURL.push_back(pb);}

private:
    // attributes:
    // O
    std::string xhref;
    std::string xactuate; // OD, onRequest/ "onLoad"

    std::vector<mpd_segmenturl_t *> SegmentURL;
};

// SegmentTemplate
class mpd_segmenttemplate_t : public mpd_segmentmultibase_t {

protected:
    virtual bool on_attribute (const char *attrName, const char *attrValue);
    //virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    // base func:
    virtual std::string toString (void);

    const std::string & get_media () const {return this->media;}
    void set_media (const char * p) {this->media = p;}
    void set_media (const std::string& u) {this->media = u;}

    const std::string& get_initialisation () const {return this->initialization;}
    void set_initialisation (const char * p) {this->initialization = p;}
    void set_initialisation (const std::string& u) {this->initialization = u;}

    const std::string& get_index () const {return this->index;}
    void set_index (const char * p) {this->index = p;}
    void set_index (const std::string& u) {this->index = u;}

    const std::string& get_bitswitch () const {return this->bitstreamSwitching;}
    void set_bitswitch (const char * p) {this->bitstreamSwitching = p;}
    void set_bitswitch (const std::string& u) {this->bitstreamSwitching = u;}

private:
    // attributes:
    // O
    std::string media;
    std::string index;
    std::string initialization;
    std::string bitstreamSwitching;
};

// the items (BaseURL/SegmentBase/SegmentList/SegmentTemplate/) used in Period/AdaptationSet/Representation
// this class is not defined in DASH
class mpd_segmentcommon_t {

protected:
    bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    mpd_segmentcommon_t () : SegmentTemplate(NULL), SegmentBase(NULL), SegmentList(NULL) {}
    ~mpd_segmentcommon_t();

    bool verify (void);
    std::string toString (void);
    std::string toStringContent (void);

    mpd_segmenttemplate_t * get_segtemplate () {return this->SegmentTemplate;}
    void set_segtemplate (mpd_segmenttemplate_t *pb) {this->SegmentTemplate = pb;}

    std::vector<mpd_baseurl_t *> & get_baseurl (void) { return this->BaseURL; }
    void add_baseurl (mpd_baseurl_t *pb) { this->BaseURL.push_back(pb); }

    mpd_segmentbase_t * get_segbase () {return this->SegmentBase;}
    void set_segbase (mpd_segmentbase_t *pb) {this->SegmentBase = pb;}

    mpd_segmentlist_t * get_seglist () {return this->SegmentList;}
    void set_seglist (mpd_segmentlist_t *pb) {this->SegmentList = pb;}

private:
    // sequence
    std::vector<mpd_baseurl_t *> BaseURL;
    mpd_segmenttemplate_t * SegmentTemplate;
    mpd_segmentbase_t * SegmentBase;
    mpd_segmentlist_t * SegmentList;
};

//RepresentationType
class mpd_representation_t : public mpd_representation_base_t, public mpd_segmentcommon_t  {

protected:
    virtual bool on_attribute (const char *attrName, const char *attrValue);
    virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    mpd_representation_t ();
    ~mpd_representation_t ();

    // base func:
    virtual bool verify (void);
    virtual std::string toString (void);

    size_t get_bandwidth () const {return this->bandwidth;}
    void set_bandwidth (const std::string& u) {std::stringstream(u) >> this->bandwidth;}
    void set_bandwidth (const size_t val) {this->bandwidth = val;}

    size_t get_qranking () const {return this->qualityRanking;}
    void set_qranking (const std::string& u) {std::stringstream(u) >> this->qualityRanking;}

    const std::string & get_id () const {return this->id;}
    void set_id (const char * p) {this->id = p;}
    void set_id (const std::string& u) {this->id = u;}

    const std::vector<std::string> & get_depid () const {return this->dependencyId;}
    void set_depid (const char * p) { str_split<std::string> (this->dependencyId, p, " "); }
    void set_depid (const std::string & s) { str_split<std::string> (this->dependencyId, s, " "); }

    const std::vector<std::string> & get_structid () const {return this->mediaStreamStructureId;}
    void set_structid (const char * p) { str_split<std::string> (this->mediaStreamStructureId, p, " "); }
    void set_structid (const std::string & s) { str_split<std::string> (this->mediaStreamStructureId, s, " "); }

    std::vector<mpd_subrepres_t *> & get_subrepres (void) { return this->SubRepresentation; }
    void add_subrepres (mpd_subrepres_t *pb) {SubRepresentation.push_back(pb);}

private:
    // attributes:
    // M
    std::string id;
    size_t bandwidth;
    // O
    size_t qualityRanking;
    std::vector<std::string> dependencyId; //StringVectorType, a whitespace-separated list of values of @id attributes.
    std::vector<std::string> mediaStreamStructureId; //StringVectorType, a whitespace-separated list of media stream structure identifier values

    // sequences:
    std::vector<mpd_subrepres_t *> SubRepresentation;
    // the following are moved to mpd_segmentcommon_t
//    std::vector<mpd_baseurl_t *> BaseURL;
//    mpd_segmenttemplate_t * SegmentTemplate;
//    mpd_segmentbase_t * SegmentBase;
//    mpd_segmentlist_t * SegmentList;
};

// ContentComponent
class mpd_contentcomponent_t : public mpd_base_t {

protected:
    virtual bool on_attribute (const char *attrName, const char *attrValue);
    virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    mpd_contentcomponent_t ();
    ~mpd_contentcomponent_t ();

    // base func:
    virtual bool verify (void);
    virtual std::string toString (void);

    size_t get_id () const {return this->id;}
    void set_id (const std::string& u) {std::stringstream(u) >> this->id;}

    const std::string& get_cnttype () const {return this->contentType;}
    void set_cnttype (const char * p) {this->contentType = p;}
    void set_cnttype (const std::string& u) {this->contentType = u;}

    const std::string& get_lang () const {return this->lang;}
    void set_lang (const char * p) {this->lang = p;}
    void set_lang (const std::string& u) {this->lang = u;}

    const std::string& get_par () const {return this->par;}
    void set_par (const char * p) {this->par = p;}
    void set_par (const std::string& u) {this->par = u;}


    std::vector<mpd_descriptor_t *> & get_access (void) { return this->Accessibility; }
    void add_access (mpd_descriptor_t *pb) {this->Accessibility.push_back(pb);}

    std::vector<mpd_descriptor_t *> & get_role (void) { return this->Role; }
    void add_role (mpd_descriptor_t *pb) {this->Role.push_back(pb);}

    std::vector<mpd_descriptor_t *> & get_rating (void) { return this->Rating; }
    void add_rating (mpd_descriptor_t *pb) {this->Rating.push_back(pb);}

    std::vector<mpd_descriptor_t *> & get_viewpoint (void) { return this->Viewpoint; }
    void add_viewpoint (mpd_descriptor_t *pb) {this->Viewpoint.push_back(pb);}

private:
    // attributes:
    // O
    size_t id;
    std::string lang;
    std::string contentType;
    std::string par;

    // sequences:
    std::vector<mpd_descriptor_t *> Accessibility;
    std::vector<mpd_descriptor_t *> Role;
    std::vector<mpd_descriptor_t *> Rating;
    std::vector<mpd_descriptor_t *> Viewpoint;
};

class mpd_adaptationset_t : public mpd_representation_base_t, public mpd_segmentcommon_t {

protected:
    virtual bool on_attribute (const char *attrName, const char *attrValue);
    virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    mpd_adaptationset_t ();
    ~mpd_adaptationset_t ();

    // base func:
    virtual bool verify (void);
    virtual std::string toString (void);

    size_t get_id () const {return this->id;}
    void set_id (const std::string& u) {std::stringstream(u) >> this->id;}

    size_t get_group () const {return this->group;}
    void set_group (const std::string& u) {std::stringstream(u) >> this->group;}

    size_t get_subsegsap () const {return this->subsegmentStartsWithSAP;}
    void set_subsegsap (const std::string& u) {std::stringstream(u) >> this->subsegmentStartsWithSAP;}

    bool get_bitswitch () const {return this->bitstreamSwitching;}
    void set_bitswitch (const std::string& u) {if (! u.compare("true")) {this->bitstreamSwitching = true;} else {this->bitstreamSwitching = false;}}

    bool get_subsegalign () const {return this->subsegmentAlignment;}
    void set_subsegalign (const std::string& u) {if (! u.compare("true")) {this->subsegmentAlignment = true;} else {this->subsegmentAlignment = false;}}

    bool get_segalign () const {return this->segmentAlignment;}
    void set_segalign (const std::string& u) {if (! u.compare("true")) {this->segmentAlignment = true;} else {this->segmentAlignment = false;}}

    const std::string& get_lang () const {return this->lang;}
    void set_lang (const char * p) {this->lang = p;}
    void set_lang (const std::string& u) {this->lang = u;}

    const std::string& get_par () const {return this->par;}
    void set_par (const char * p) {this->par = p;}
    void set_par (const std::string& u) {this->par = u;}

    const std::string& get_xhref () const {return this->xhref;}
    void set_xhref (const char * p) {this->xhref = p;}
    void set_xhref (const std::string& u) {this->xhref = u;}

    const std::string& get_xactuate () const {return this->xactuate;}
    void set_xactuate (const char * p) {this->xactuate = p;}
    void set_xactuate (const std::string& u) {this->xactuate = u;}

    const std::string& get_cnttype () const {return this->contentType;}
    void set_cnttype (const char * p) {this->contentType = p;}
    void set_cnttype (const std::string& u) {this->contentType = u;}

    const std::string& get_fmratemin () const {return this->minFrameRate;}
    void set_fmratemin (const char * p) {this->minFrameRate = p;}
    void set_fmratemin (const std::string& u) {this->minFrameRate = u;}

    const std::string& get_fmratemax () const {return this->maxFrameRate;}
    void set_fmratemax (const char * p) {this->maxFrameRate = p;}
    void set_fmratemax (const std::string& u) {this->maxFrameRate = u;}

    size_t get_bwmin () const {return this->minBandwidth;}
    void set_bwmin (const std::string& u) {std::stringstream(u) >> this->minBandwidth;}

    size_t get_bwmax () const {return this->maxBandwidth;}
    void set_bwmax (const std::string& u) {std::stringstream(u) >> this->maxBandwidth;}

    size_t get_widthmin () const {return this->minWidth;}
    void set_widthmin (const std::string& u) {std::stringstream(u) >> this->minWidth;}

    size_t get_widthmax () const {return this->maxWidth;}
    void set_widthmax (const std::string& u) {std::stringstream(u) >> this->maxWidth;}

    size_t get_heightmin () const {return this->minHeight;}
    void set_heightmin (const std::string& u) {std::stringstream(u) >> this->minHeight;}

    size_t get_heightmax () const {return this->maxHeight;}
    void set_heightmax (const std::string& u) {std::stringstream(u) >> this->maxHeight;}

    std::vector<mpd_representation_t *> & get_represent (void) { return this->Representation; }
    void add_represent (mpd_representation_t *pb) {this->Representation.push_back(pb);}

    std::vector<mpd_contentcomponent_t *> & get_cntcomponent (void) { return this->ContentComponent; }
    void add_cntcomponent (mpd_contentcomponent_t *pb) {this->ContentComponent.push_back(pb);}

    std::vector<mpd_descriptor_t *> & get_access (void) { return this->Accessibility; }
    void add_access (mpd_descriptor_t *pb) {this->Accessibility.push_back(pb);}

    std::vector<mpd_descriptor_t *> & get_role (void) { return this->Role; }
    void add_role (mpd_descriptor_t *pb) {this->Role.push_back(pb);}

    std::vector<mpd_descriptor_t *> & get_rating (void) { return this->Rating; }
    void add_rating (mpd_descriptor_t *pb) {this->Rating.push_back(pb);}

    std::vector<mpd_descriptor_t *> & get_viewpoint (void) { return this->Viewpoint; }
    void add_viewpoint (mpd_descriptor_t *pb) {this->Viewpoint.push_back(pb);}

    bool is_match0 (std::string & type, std::string & lang);
    bool is_match_type (std::string & type);
    bool is_match_lang (std::string & lang);

    virtual bool post_parser (void); /*!< sort Representation by bandwidth */

    // utils:
    mpd_representation_t * get_match_represent (size_t bandwidth);

private:
    // attributes:
    // O
    ssize_t id;
    bool subsegmentAlignment; // OD, false/true
    size_t subsegmentStartsWithSAP; // OD, 0
    std::string lang;
    bool bitstreamSwitching;

    std::string xhref;
    std::string xactuate; // OD, onRequest/ "onLoad"
    ssize_t group;
    std::string contentType;
    std::string par;
    size_t minBandwidth;
    size_t maxBandwidth;
    size_t minWidth;
    size_t maxWidth;
    size_t minHeight;
    size_t maxHeight;
    std::string minFrameRate;
    std::string maxFrameRate;
    bool segmentAlignment; // OD, false/true

    // sequences:
    std::vector<mpd_representation_t *> Representation; // TODO: sort it by bandwidth
    std::vector<mpd_contentcomponent_t *> ContentComponent;

    std::vector<mpd_descriptor_t *> Accessibility;
    std::vector<mpd_descriptor_t *> Role;
    std::vector<mpd_descriptor_t *> Rating;
    std::vector<mpd_descriptor_t *> Viewpoint;

    // the following are moved to mpd_segmentcommon_t
//    std::vector<mpd_baseurl_t *> BaseURL;
//    mpd_segmenttemplate_t * SegmentTemplate;
//    mpd_segmentbase_t * SegmentBase;
//    mpd_segmentlist_t * SegmentList;
};

// SubsetType, 5.3.8
class mpd_subset_t : public mpd_base_t {

protected:
    virtual bool on_attribute (const char *attrName, const char *attrValue);
    //virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    // base func:
    virtual bool verify (void);
    virtual std::string toString (void);

    const std::vector<std::string> & get_contains () const {return this->contains;}
    void set_contains (const char * p) { str_split<std::string> (this->contains, p, " "); }
    void set_contains (const std::string & s) { str_split<std::string> (this->contains, s, " "); }

private:
    // attributes
    // M
    std::vector<std::string> contains; //UIntVectorType, a white-space separated list of the @id values of the contained Adaptation Sets.
};

class mpd_period_t : public mpd_base_t, public mpd_segmentcommon_t {

protected:
    virtual bool on_attribute (const char *attrName, const char *attrValue);
    virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    mpd_period_t ();
    ~mpd_period_t ();

    // base func:
    virtual std::string toString (void);
    virtual bool verify (void);

    const std::string& get_id () const {return this->id;}
    void set_id (const char * p) {this->id = p;}
    void set_id (const std::string& id) {this->id = id;}

    const std::string& get_start () const {return this->start;}
    double get_start_value () const {if (this->start.size () < 1) {return 0.0;} return xsduration_to_seconds (this->start); }
    void set_start (double val) {this->start.clear(); xsduration_from_seconds(this->start, val);}
    void set_start (const char * p) {this->start = p;}
    void set_start (const std::string& u) {this->start = u;}

    const std::string& get_duration () const {return this->duration;}
    double get_duration_value () const {if (this->duration.size () < 1) {return 0.0;} return xsduration_to_seconds (this->duration); }
    void set_duration (double val) {this->duration.clear(); xsduration_from_seconds(this->duration, val);}
    void set_duration (const char * p) {this->duration = p;}
    void set_duration (const std::string& u) {this->duration = u;}

    const std::string& get_xhref () const {return this->xhref;}
    void set_xhref (const char * p) {this->xhref = p;}
    void set_xhref (const std::string& u) {this->xhref = u;}

    const std::string& get_xactuate () const {return this->xactuate;}
    void set_xactuate (const char * p) {this->xactuate = p;}
    void set_xactuate (const std::string& u) {this->xactuate = u;}

    bool get_bitswitch () const {return this->bitstreamSwitching;}
    void set_bitswitch (const std::string& u) {if (! u.compare("true")) {this->bitstreamSwitching = true;} else {this->bitstreamSwitching = false;}}

    std::vector<mpd_adaptationset_t *> & get_adaptset (void) { return this->adaptationSets; }
    void add_adaptset (mpd_adaptationset_t *pb) {this->adaptationSets.push_back(pb);}

    std::vector<mpd_subset_t *> & get_subset (void) { return this->Subset; }
    void add_subset (mpd_subset_t *pb) {this->Subset.push_back(pb);}

    virtual bool post_parser (void);

    // utils:
    size_t get_next_match_adapset_pos (size_t idx_start, std::string & type, std::string & lang);
    std::vector<std::string> get_mimetype_list (void);
    std::vector<std::string> get_language_list (std::string & type);
    std::vector<size_t> get_bandwith_list (std::string & type, std::string & lang);

private:
    // attributes:
    // O
    std::string id;
    std::string start;    // double
    std::string duration; // double

    bool bitstreamSwitching; // OD, false/true
    std::string xhref;
    std::string xactuate; // OD, onRequest/ "onLoad"

    // sequences:
    std::vector<mpd_adaptationset_t *> adaptationSets;
    std::vector<mpd_subset_t *> Subset;

    // the following are moved to mpd_segmentcommon_t
//    std::vector<mpd_baseurl_t *> BaseURL;
//    mpd_segmenttemplate_t *SegmentTemplate;
//    mpd_segmentbase_t *SegmentBase;
//    mpd_segmentlist_t *SegmentList;
};

// ProgramInformationType
class mpd_proginfo_t : public mpd_base_t {

protected:
    virtual bool on_attribute (const char *attrName, const char *attrValue);
    virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    // base func:
    virtual std::string toString (void);

    const std::string& get_lang () const {return this->lang;}
    void set_lang (const char * p) {this->lang = p;}
    void set_lang (const std::string& u) {this->lang = u;}

    const std::string& get_moreinfourl () const {return this->moreInformationURL;}
    void set_moreinfourl (const char * p) {this->moreInformationURL = p;}
    void set_moreinfourl (const std::string& u) {this->moreInformationURL = u;}

    const std::string& get_title () const {return this->Title;}
    void set_title (const char * p) {this->Title = p;}
    void set_title (const std::string& u) {this->Title = u;}

    const std::string& get_source () const {return this->Source;}
    void set_source (const char * p) {this->Source = p;}
    void set_source (const std::string& u) {this->Source = u;}

    const std::string& get_cyright () const {return this->Copyright;}
    void set_cyright (const char * p) {this->Copyright = p;}
    void set_cyright (const std::string& u) {this->Copyright = u;}

private:
    // attributes:
    // O
    std::string lang;
    std::string moreInformationURL;

    // sequences:
    std::string Title;
    std::string Source;
    std::string Copyright;
};

// RangeType
class mpd_range_t : public mpd_base_t {

protected:
    virtual bool on_attribute (const char *attrName, const char *attrValue);
    //virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    // base func:
    virtual std::string toString (void);

    const std::string& get_starttime () const {return this->starttime;}
    void set_starttime (const char * p) {this->starttime = p;}
    void set_starttime (const std::string& u) {this->starttime = u;}

    const std::string& get_duration () const {return this->duration;}
    void set_duration (const char * p) {this->duration = p;}
    void set_duration (const std::string& u) {this->duration = u;}

private:
    // attributes:
    // O
    std::string starttime;
    std::string duration;
};

// MetricsType
class mpd_metrics_t : public mpd_base_t {

protected:
    virtual bool on_attribute (const char *attrName, const char *attrValue);
    virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    ~mpd_metrics_t ();

    // base func:
    virtual bool verify (void);
    virtual std::string toString (void);

    const std::string& get_metrics () const {return this->metrics;}
    void set_metrics (const char * p) {this->metrics = p;}
    void set_metrics (const std::string& u) {this->metrics = u;}

    std::vector<mpd_descriptor_t *> & get_report (void) { return this->Reporting; }
    void add_report (mpd_descriptor_t *pb) {Reporting.push_back(pb);}

    std::vector<mpd_range_t *> & get_range (void) { return this->Range; }
    void add_range (mpd_range_t *pb) {Range.push_back(pb);}

private:
    // attributes:
    // M
    std::string metrics;

    // sequences:
    std::vector<mpd_descriptor_t *> Reporting; // 1..N
    std::vector<mpd_range_t *> Range; // 0..N
};

class mpd_t : public mpd_base_t {

protected:
    virtual bool on_attribute (const char *attrName, const char *attrValue);
    //virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);
    virtual bool parse_content (xml_reader_base_t * xmlrd, const char *node_name);

public:
    typedef enum presentation_s {
        PERROR = -1, PSTATIC, PDYNAMIC,
    } presentation_t;
    mpd_t (std::string &default_url);
    ~mpd_t ();

    // base func:
    virtual std::string toString (void);
    virtual bool verify (void);

    const std::string & get_id () const;
    void set_id (const char * p) {this->id = p;}
    void set_id (const std::string& id) {this->id = id;}

    const std::vector<std::string> & get_profiles () const {return this->profiles;}
    void set_profiles (const char * p) { str_split<std::string> (this->profiles, p, ","); }
    void set_profiles (const std::string& s) { str_split<std::string> (this->profiles, s, ","); }

    const std::string& get_min_buf_time () const {return this->minBufferTime;}
    double get_min_buf_time_value () const {if (this->minBufferTime.size () < 1) {return 0.0;} return xsduration_to_seconds (this->minBufferTime); }
    void set_min_buf_time (const std::string& pf) {this->minBufferTime = pf;}

    // "2011-12-25T12:30:00"
    const std::string& get_avstarttime () const {return this->availabilityStartTime;}
    //double get_avstarttime_value () const {if (this->availabilityStartTime.size () < 1) {return 0.0;} return xsduration_to_seconds (this->availabilityStartTime); }
    void set_avstarttime (const char * p) {this->availabilityStartTime = p;}
    void set_avstarttime (const std::string& pf) {this->availabilityStartTime = pf;}

    const std::string& get_pres_duration () const {return this->mediaPresentationDuration;}
    double get_pres_duration_value () const {if (this->mediaPresentationDuration.size () < 1) {return -1;} return xsduration_to_seconds (this->mediaPresentationDuration); }
    void set_pres_duration (const std::string& pf) {this->mediaPresentationDuration = pf;}

    const char * get_type () const {return presentation_type_to_string(this->type);}
    void set_type (const std::string& pf) {this->type = presentation_cstr_to_type(pf.c_str());}
    void set_type (const char *p) {this->type = presentation_cstr_to_type(p);}

    std::vector<mpd_baseurl_t *> & get_baseurl (void) { return this->BaseURL; }
    void add_baseurl (mpd_baseurl_t *pb) {this->BaseURL.push_back(pb);}

    std::vector<mpd_period_t *> & get_periods (void) { return this->Periods; }
    void add_period (mpd_period_t *pb) {this->Periods.push_back(pb);}
    size_t get_periods_num (void) {return this->Periods.size();} /*!< get the number of Period */
    double get_period_starttime (size_t idx);
    mpd_period_t * get_period_from_stime (double stime);

    std::vector<mpd_proginfo_t *> & get_proginfo (void) { return this->ProgramInformation; }
    void add_proginfo (mpd_proginfo_t *pb) {this->ProgramInformation.push_back(pb);}

    std::vector<std::string> & get_location (void) { return this->Location; }
    void add_location (std::string &b) {this->Location.push_back(b);}

    std::vector<mpd_metrics_t *> & get_metric (void) { return this->Metrics; }
    void add_metric (mpd_metrics_t *pb) {this->Metrics.push_back(pb);}

    const std::string& get_xxsi () const {return this->xmlns_xsi;}
    void set_xxsi (const char * p) {this->xmlns_xsi = p;}
    void set_xxsi (const std::string& pf) {this->xmlns_xsi = pf;}

    const std::string& get_xmlns () const {return this->xmlns;}
    void set_xmlns (const char * p) {this->xmlns = p;}
    void set_xmlns (const std::string& pf) {this->xmlns = pf;}

    const std::string& get_xxsischloc () const {return this->xsi_schemaLocation;}
    void set_xxsischloc (const char * p) {this->xsi_schemaLocation = p;}
    void set_xxsischloc (const std::string& pf) {this->xsi_schemaLocation = pf;}

    const std::string& get_avendtime () const {return this->availabilityEndTime;}
    void set_avendtime (const char * p) {this->availabilityEndTime = p;}
    void set_avendtime (const std::string& pf) {this->availabilityEndTime = pf;}

    const std::string& get_minupdperiod () const {return this->minimumUpdatePeriod;}
    void set_minupdperiod (const char * p) {this->minimumUpdatePeriod = p;}
    void set_minupdperiod (const std::string& pf) {this->minimumUpdatePeriod = pf;}

    const std::string& get_bufdeptmsft () const {return this->timeShiftBufferDepth;}
    void set_bufdeptmsft (const char * p) {this->timeShiftBufferDepth = p;}
    void set_bufdeptmsft (const std::string& pf) {this->timeShiftBufferDepth = pf;}

    const std::string& get_sugpresentdelay () const {return this->suggestedPresentationDelay;}
    void set_sugpresentdelay (const char * p) {this->suggestedPresentationDelay = p;}
    void set_sugpresentdelay (const std::string& pf) {this->suggestedPresentationDelay = pf;}

    const std::string& get_maxsegduration () const {return this->maxSegmentDuration;}
    void set_maxsegduration (const char * p) {this->maxSegmentDuration = p;}
    void set_maxsegduration (const std::string& pf) {this->maxSegmentDuration = pf;}

    const std::string& get_maxsubsegduration () const {return this->maxSubsegmentDuration;}
    void set_maxsubsegduration (const char * p) {this->maxSubsegmentDuration = p;}
    void set_maxsubsegduration (const std::string& pf) {this->maxSubsegmentDuration = pf;}

    void set_live (bool is_live = true) {flg_live = is_live;}
    bool is_live (void) {return flg_live;}

    const std::string& get_default_baseurl () const {return this->url_default;}

    virtual bool post_parser (void);

    // utils
    bool get_period_adapset_rep (std::string & type, std::string & lang, double start_time, size_t avg_throughput, mpd_period_t ** pperiod, mpd_adaptationset_t ** padap, mpd_representation_t ** prep);
    double get_segment_duration (std::string & type, std::string & lang, double start_time, size_t avg_throughput, size_t idx);
    double get_segment_duration (const std::basic_string<char>& type, std::string & lang, double start_time, size_t avg_throughput, size_t idx) { std::string t(type); return get_segment_duration (t, lang, start_time, avg_throughput, idx); }

    std::vector<double> get_period_stime_list (void);

private:
    std::string url_default;
    std::string xmlns_xsi;
    std::string xmlns;
    std::string xsi_schemaLocation;
    // attributes:
    // O
    std::string id;
    mpd_t::presentation_t type; // OD static/dynamic
    std::string availabilityEndTime;
    std::string minimumUpdatePeriod;
    std::string timeShiftBufferDepth;
    std::string suggestedPresentationDelay;
    std::string maxSegmentDuration;
    std::string maxSubsegmentDuration;
    // M
    std::vector<std::string> profiles; // a comma-separated list of profile identifiers
    std::string availabilityStartTime; // CM,
    std::string mediaPresentationDuration; // CM,
    std::string minBufferTime;

    // sequence
    std::vector<mpd_period_t *> Periods; // TODO: sort the period? set start, duration if not exist.
    std::vector<mpd_baseurl_t *> BaseURL;

    std::vector<mpd_proginfo_t *> ProgramInformation;
    std::vector<std::string> Location;
    std::vector<mpd_metrics_t *> Metrics;
    // other flags
    bool flg_live;

    static const char * presentation_type_to_string (mpd_t::presentation_t t) {
        switch (t) {
        case PDYNAMIC:
            return "dynamic";
        default:
            break;
        }
        return "static";
    }
    static mpd_t::presentation_t presentation_cstr_to_type (const char *name) {
        if (! strcmp (name, "dynamic")) {
            return mpd_t::PDYNAMIC;
        } else if (! strcmp (name, "static")) {
            return mpd_t::PSTATIC;
        }
        return mpd_t::PERROR;
    }

    bool check_set_period (void);
};

mpd_t * mpd_parse_current (xml_reader_base_t * xmlrd, std::string &default_url);

double get_segment_duration (size_t idx, mpd_period_t * period, mpd_adaptationset_t * adap, mpd_representation_t * rep);

#endif // MYMPD_H
