/**
 * @file    mympdalg.h
 * @brief   DASH adaptitive algorithms
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-04-16
 */
#ifndef MYMPDALG_H
#define MYMPDALG_H

#include <map>

#include "mympd.h"
#include "mychunk.h"

// the adaptation logic abstract class;
// the next request url will base on current time, bitrate, render ratio, user actions(play, rewind, pause etc)
class mpd_adaptationlogic_t {

public:
    mpd_adaptationlogic_t (mpd_t * m) : avg_throughput(0), mpd(m), width(1920), height(1080), cur_time(0.0) {
        mpd = m;
        init ();
    }

    double tell () {return this->get_cur_time();} /*!< return current position */
    bool seek (double pos_sec); /*!< seek to a new position */
    bool seek (size_t id); /*!< seek to the postion of segment id, the id is unique for all periods(init segment, later Periods are start at previous Period end) */

    size_t get_avg_throughput (void) { return this->avg_throughput; }
    virtual void notify_avg_throughput (size_t bw) {this->avg_throughput = bw;} /*!< set the current throughput of the link */
    virtual void notify_buf_level (double sec_max, double sec_cur, double sec_request) = 0; /*!< set the current level of the buffer, maximum of buffer, current buffer size, the requested size */

    void set_render_ratio (std::string & codec, double ratio); /*!< what's the performance of the cpu */
    void set_vedio_size (size_t w, size_t h) {width = w; height = h;} /*!< the width and height of preferred display area */

    virtual bool is_need_request (void) = 0; /*!<  if the user need to call get_next_chunk according to current status (buffer level/throughput) */
    mpd_chunk_t * get_next_chunk (const char * type, const char *lang) {std::string t0 = type; std::string l0 = lang; return get_next_chunk(t0,l0);}
    mpd_chunk_t * get_next_chunk (const std::basic_string<char>& type, std::string & lang) {std::string t1 = type; return get_next_chunk(t1,lang);}
    mpd_chunk_t * get_next_chunk (std::string & type, std::string & lang);

    //mpd_chunk_t * get_chunk (std::string & type, std::string & lang, size_t idx);

    size_t get_current_bitrate (const char * type, const char *lang) {std::string tt0 = type; std::string ll0 = lang; return get_current_bitrate(tt0,ll0);}
    size_t get_current_bitrate (const std::basic_string<char>& type, std::string & lang) {std::string tt1 = type; return get_current_bitrate(tt1,lang);}
    size_t get_current_bitrate (std::string & type, std::string & lang);
//protected:
    std::vector<size_t> get_bandwith_list (const char * type, const char * lang) {std::string ttt = type; std::string lll = lang; return get_bandwith_list(ttt,lll);}
    std::vector<size_t> get_bandwith_list (std::string & type, std::string & lang);
    std::vector<std::string> get_mimetype_list (void);
    std::vector<std::string> get_language_list (std::string & type);

    std::vector<double> & get_period_stime_list (void) { return this->period_stime_list; }
    size_t get_total_number_segments (void); /*!< the total # of segments in this content */
    double get_segment_duration (size_t id); /*!< we need to get the segment duration according to the converted index (init segments are occupied ids) */

protected:
    virtual bool is_gt_upper (size_t bandwidth_current, size_t bandwidth_upper1, size_t bandwidth_upper2) const = 0; /*!< bandwidth_current > bandwidth_upper ? */
    virtual bool is_lt_lower (size_t bandwidth_current, size_t bandwidth_lower1, size_t bandwidth_lower2) const = 0; /*!< bandwidth_current < bandwidth_upper ? */

    double get_cur_time (void) {return this->cur_time;}
    void set_cur_time (double v) { this->cur_time = v; }
    mpd_chunk_t * peek_next_chunk (std::string & type, std::string & lang, size_t * pidx, double * pstart_time);
    bool get_period_adapset_rep (std::string & type, std::string & lang, mpd_period_t ** pperiod, mpd_adaptationset_t ** padap, mpd_representation_t ** prep);

    size_t avg_throughput;
private:
    mpd_t * mpd; /*!< the content of the MPD config file */

    size_t width;
    size_t height;
    std::map<std::string, double> ratios; /*!< map cpu ability for processing the codecs, such as "avc1.4D401F" */

    // the segment info:
    // map type "video"/"audio"/"text" to current position
    //std::map<std::string, size_t> cur_pos; /*!< the current position in current period, for seek()*/
    double cur_time; /*!< the current time, over all period */
    std::map<std::string, size_t> cur_idx; /*!< the current index of the current adapationset */

    // convert the abstract time to abstract id
    std::vector<double> period_stime_list;
    void init (void);
    size_t time2id_abstract (double seconds); /*!< get the abstract id at the second */
    std::vector<size_t> id_list; /*!< the start segment id of at global range, the init segments are involved. */
    std::vector<double> segduration_list; /*!< the segment duration in each peroid */

    ssize_t search_id_list_position (size_t id); /*!< get the position in the id_list from id value */
};

class mpd_adaptationlogic_yhfu_t : public mpd_adaptationlogic_t {

public:
    mpd_adaptationlogic_yhfu_t(mpd_t *mpd) : mpd_adaptationlogic_t(mpd), ratio_threshold_increase(0.5), ratio_threshold_reduction(0.5), last_aavg(0.0), buffer_sec_max(0), buffer_sec_cur(0) {}
    virtual void notify_avg_throughput (size_t bw);
    virtual void notify_buf_level (double sec_max, double sec_cur, double sec_requested);
    virtual bool is_need_request (void);

protected:
    virtual bool is_gt_upper (size_t bandwidth_current, size_t bandwidth_upper1, size_t bandwidth_upper2) const; /*!< bandwidth_current > bandwidth_upper ? */
    virtual bool is_lt_lower (size_t bandwidth_current, size_t bandwidth_lower1, size_t bandwidth_lower2) const; /*!< bandwidth_current < bandwidth_upper ? */

private:
    double ratio_threshold_increase;  /*!< 0% ~ 100% of the (bitrate[i+1] - bitrate[i]) */
    double ratio_threshold_reduction; /*!< 0% ~ 100% of the (bitrate[i] - bitrate[i-1]) which current bit-rate lower than it will drop to [i-1] bitrate requests. */
    double last_aavg;

    double buffer_sec_max; /*!< the max seconds of the buffer */
    double buffer_sec_cur; /*!< the current seconds in the buffer */
    double buffer_sec_requested; /*!< the requested segments not in the buffer */
};

#endif // MYMPDALG_H
