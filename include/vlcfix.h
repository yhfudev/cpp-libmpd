/**
 * @file    vlcfix.h
 * @brief   VLC test functions
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-10-29
 */
/*****************************************************************************
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifndef _VLC_FIXINC_H
#define _VLC_FIXINC_H

#ifdef __cplusplus
# define CEXT_BEGIN extern "C" {
# define CEXT_END   }
#else
# define CEXT_BEGIN
# define CEXT_END
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#ifdef __cplusplus
extern "C"{
#  define __STDC_CONSTANT_MACROS
#  ifdef _STDINT_H
#    undef _STDINT_H
#  endif
#  include <stdint.h>
}
#endif
#ifndef INT64_C
#  define INT64_C(c) (c ## LL)
//#  define INT64_C(a) ((int64_t)(a))
#endif
#ifndef UINT64_C
#  define UINT64_C(c) (c ## ULL)
//#  define UINT64_C(a) ((uint64_t)(a))
#endif

/* because of using CEXT_BEGIN, we move the vlc_arrays.h here */
#ifndef unlikely
/* Branch prediction */
#ifdef __GNUC__
#   define likely(p)   __builtin_expect(!!(p), 1)
#   define unlikely(p) __builtin_expect(!!(p), 0)
#else
#   define likely(p)   (!!(p))
#   define unlikely(p) (!!(p))
#endif
#endif

#ifndef __cplusplus
# include <stdbool.h>
#endif

#include <vlc_arrays.h>

CEXT_BEGIN
#include <vlc_common.h>
#include <vlc_plugin.h>

#include <vlc_threads.h>
#include <vlc_charset.h> //FromCharset()
#include <vlc_stream.h>
#include <vlc_demux.h>
CEXT_END

#endif /*_VLC_FIXINC_H */
