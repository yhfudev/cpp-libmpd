/**
 * @file    mychunk.h
 * @brief   DASH chunk
 * @author  Yunhui Fu (yhfudev@gmail.com)
 * @version 1.0
 * @date    2013-10-16
 */
#ifndef MYMPDCHUNK_H
#define MYMPDCHUNK_H

#include <sys/types.h> // ssize_t

#include <string>
#include <vector>
#include <map>

#include "mympd.h"
#include "myxmlparser.h"

/* @brief   mpd_chunk_t is to manage the chunks of the meida stream.
 */
#if LIBDASH_USE_DASH_DOWNLOAD

#include "http/Chunk.h"
#include "mpd/IMPDManager.h"

class mpd_chunk_t : public dash::http::Chunk {

public:
    bool has_byterange (void) {return useByteRange();}
    void set_byte_start (size_t startByte) {setUseByteRange(true); setStartByte(startByte);}
    void set_byte_end (size_t endByte) {setEndByte(endByte);}
    void set_bitrate (size_t val) {setBitrate (val);}
    void set_url (const std::string &url) {setUrl(url);}
    void add_opt_url (const std::string &url) {addOptionalUrl(url);}

    size_t get_byte_start (void) { return getStartByte(); }
    size_t get_byte_end (void) { return getEndByte(); }
    size_t get_bitrate (void) { return getBitrate(); }
    std::string & get_url (void) { return const_cast<std::string &>(getUrl()); }
    std::vector<std::string> & get_opt_url (void) { return this->getOptionalUrls(); }

    bool is_media (void) {return (MEDIA == this->type); }
    bool is_index (void) {return (INDEX == this->type); }
    void set_media (bool t = true) { if (t) {this->type = MEDIA;} else {this->type = UNKNOWN;}; }
    void set_index (bool t = true) { if (t) {this->type = INDEX;} else {this->type = UNKNOWN;}; }

    // the api for received package:
    void set_id (int id0) { id = id0; } /*!< the id of the segment. If there exist queues for both video and audio, the user should use two buffer lines for each of the media type */
    int get_id (void) {return id;}
    void set_category1 (int cat0) { this->category1 = cat0; }
    int get_category (void) { return this->category1; }

    bool operator == (mpd_chunk_t & other);

private:
    typedef enum _mpd_chunk_type_s {
        MEDIA,
        INDEX,
        UNKNOWN,
    } mpd_chunk_type_t;
    mpd_chunk_type_t type;
    int id;
    int category1;
};

#else // LIBDASH_USE_DASH_DOWNLOAD
class mpd_chunk_t {

public:
    mpd_chunk_t () : type(UNKNOWN), flg_byterange(false), bitrate(0), byte_start(0), byte_end(0), id(-1), category1(-1) {}
    bool has_byterange (void) {return flg_byterange;}

    void set_byte_start (size_t startByte) {flg_byterange = true; byte_start = startByte;}
    void set_byte_end (size_t endByte) {flg_byterange = true; byte_end = endByte;}
    void set_bitrate (size_t val) {bitrate = val;} /*!< for computing the real time of the chunk for block_t */
    void set_url (const std::string &url0) {url = url0;} /*!< set the main download url */
    void add_opt_url (const std::string &url0) {url_opt.push_back(url0);} /*!< add the optional download url */

    size_t get_byte_start (void) { return byte_start; }
    size_t get_byte_end (void) { return byte_end; }
    size_t get_bitrate (void) { return bitrate; }
    std::string & get_url (void) { return url; }
    std::vector<std::string> & get_opt_url (void) { return url_opt; }

    bool is_media (void) {return (MEDIA == this->type); }
    bool is_index (void) {return (INDEX == this->type); }
    void set_media (bool t = true) { if (t) {this->type = MEDIA;} else {this->type = UNKNOWN;}; }
    void set_index (bool t = true) { if (t) {this->type = INDEX;} else {this->type = UNKNOWN;}; }

    // the api for received package:
    void set_id (int id0) { id = id0; } /*!< the id of the segment. If there exist queues for both video and audio, the user should use two buffer lines for each of the media type */
    int get_id (void) {return id;}
    //void set_category (int cat0) { this->category = cat0; }
    void set_category1 (int cat0) { this->category1 = cat0; }
    int get_category (void) { return this->category1; }

    bool operator == (mpd_chunk_t & other);

private:
    typedef enum _mpd_chunk_type_s {
        MEDIA,
        INDEX,
        UNKNOWN,
    } mpd_chunk_type_t;
    mpd_chunk_type_t type;

    bool flg_byterange;
    size_t bitrate;
    size_t byte_start;
    size_t byte_end;
    std::string url;
    std::vector<std::string> url_opt;

    int id; /*!< chunk id, 0-init, 1-N - segments */
    int category1; /*!< video/audio id, created by the upper layer */
};
#endif // LIBDASH_USE_DASH_DOWNLOAD


inline bool
mpd_chunk_t::operator == (mpd_chunk_t & otherchk)
{
    if ((this->get_category() == otherchk.get_category()) && (this->get_id() == otherchk.get_id())) {
        return true;
    }
    return false;
}

extern void dump_mpdchunk_info (mpd_chunk_t *chk);

//extern mpd_chunk_t * get_chunk (mpd_t *pmpd, std::string & type, std::string & lang, double start_time, size_t avg_throughput, size_t idx);
mpd_chunk_t * get_chunk (mpd_t *pmpd, std::string & type, std::string & lang, double start_time, size_t avg_throughput, size_t idx);

#endif // MYMPDCHUNK_H
